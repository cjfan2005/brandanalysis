package com.trendplus.audit.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.trendplus.audit.bean.Audit;
import com.trendplus.audit.dao.AuditDAO;
import com.trendplus.user.bean.Users;
import com.trendplus.util.StringUtils;

public class AuditDAOImpl implements AuditDAO {
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void createAudit(Session session, Audit audit) {
		session.save(audit);
	}
	
	public List<Map<String, Object>> getAllAudit(String order, String sort) {
		
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			//String sql = "select * from Audit ";
			sql += " SELECT ";
			sql += " * ";
			sql += " FROM ";
			sql += " audit ";
			sql += " WHERE ";
			sql += " 1 = 1 ";
			
			if(StringUtils.isNotBlank(order) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + order;
			}else{
				sql += " ORDER BY ";
				sql += " AuditTime DESC ";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	public List<Map<String, Object>> getAuditCondition(String userId, String auditType, String startDate, String endDate, String order, String sort) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			//String sql = "select * from Audit ";
			sql += " SELECT ";
			sql += " UserId, ";
			sql += " AuditTime, ";
			sql += " ProgramName, ";
			sql += " AuditType, ";
			sql += " SqlLog ";
			sql += " FROM ";
			sql += " audit ";
			sql += " WHERE ";
			sql += " 1 = 1 ";
			
			if(StringUtils.isNotBlank(auditType))
				sql += " AND auditType = '" + auditType + "' ";
			
			if(StringUtils.isNotBlank(userId))
				sql += " AND UserId = '" + userId + "' ";
			
			if(StringUtils.isNotBlank(startDate))
				sql += " AND AuditTime > '" + startDate + "' ";
			
			if(StringUtils.isNotBlank(endDate))
				sql += " AND AuditTime < '" + endDate + "' ";
			
			if(StringUtils.isNotBlank(order) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + order;
			}else{
				sql += " ORDER BY ";
				sql += " AuditTime DESC ";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
}
