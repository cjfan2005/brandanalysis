package com.trendplus.crawler.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.trendplus.crawler.bean.*;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.trendplus.crawler.dao.CrawlerDAO;
import com.trendplus.util.PageParameter;
import com.trendplus.util.Util;

public class CrawlerAction {
	private String companyId, companyName, companyName2;
	private String topic, topicSerial, act, topicCategory, siteSerial, keywordSerial;
	private int categorySerial, subCategory;
	private String urlName, url;
	private String keyword, keyword_0, keyword_1, keyword_2, keyword_3, keyword_4, keyword_5;
	private char enable;
	private Map allCompanyJsonStr, allCompanyJsonStr2, allTopicJSONStr, siteJSONStr, keywordJSONStr;
	

	
	@Autowired
	private CrawlerDAO crawlerDAO;
	
	// jqgrid
	private int total = 0; // 總頁數
	private int records = 0; // 資料總筆數
	private int pageSize = 0;
	private int currentPageNo = 0;
	private String sort;
	private String orderBy;
	private List<Map<String, Object>> rows;
	
	/**
	 * CRUD the Topic and Site Tables.
	 * For ADMIN.
	 */
	public String initCrawlerSettings() {
		return "success";
	}
	
	public String addNewQueryCondition() {
		//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
		
		if(topicSerial==null || topicSerial.length()==0) {
			return "input";
		}
		
		//query_site bean
		List<QuerySite> qsList = new ArrayList<QuerySite>();
		QuerySite qs = new QuerySite();
		
		qs.setUrlName(urlName);
		qs.setUrl(url);
		qs.setEnable(enable);
		qs.setUpdateDate(new Date());
		qs.setEditor(loggedInUser);
		qs.setTopicSerial(topicSerial);
		qsList.add(qs);
		
		
		//query_keyword bean
		List<QueryKeyword> qkList = new ArrayList<QueryKeyword>();
		String[] kwArray = new String[]{keyword_0, keyword_1, keyword_2, keyword_3, keyword_4, keyword_5};
		for(int i=0; i<kwArray.length; i++) {
			QueryKeyword qk = new QueryKeyword();
			
			//空的keyword跳過
			if(!kwArray[i].isEmpty()) {
				qk.setKeyword(kwArray[i]);
				qk.setEnable(enable);
				qk.setUpdateDate(new Date());
				qk.setEditor(loggedInUser);
				qk.setTopicSerial(topicSerial);
				qkList.add(qk);
			}
		}
		
		String result = crawlerDAO.insertQueryCondition(qsList, qkList);
		if(!result.equals("success")) {
			return "input";
		}
		
		return "success";
	}
	
	public String initMaintainCrawlerParam(){
		return "success";
	}
	
	
	@SuppressWarnings("unchecked")
	public String getSiteJSON() {
		//if topicSerial is empty, do not query! return success directly.
		if(topicSerial==null || topicSerial.trim().length()==0) {
			return "success";
		}
		
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		List<Map<String, Object>> objList = crawlerDAO.getSiteJSON(topicSerial, sort, orderBy);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
		

		List<Map<String, Object>> sList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> sItem;
		siteJSONStr = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			sItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			sItem.put("siteSerial", obj[0].toString());
			sItem.put("topicSerial", obj[1].toString());
			sItem.put("urlName", obj[2]);
			sItem.put("url", obj[3]);
			sItem.put("enable", obj[4]);
			sItem.put("updateDate", obj[5]);
			sItem.put("editor", obj[6]);
			sItem.put("topic", obj[7]);
			sList.add(sItem);
		}
		
		rows = sList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		siteJSONStr = new HashMap<String, Object>();
		siteJSONStr.put("pageSize", pageSize);
		siteJSONStr.put("total", total);
		siteJSONStr.put("count", count);
		siteJSONStr.put("currentPageNo", currentPageNo);
		siteJSONStr.put("rows", rows);
		
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getKeywordJSON() {
		//if topicSerial is empty, do not query! return success directly.
		if(topicSerial==null || topicSerial.trim().length()==0) {
			return "success";
		}
		
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		List<Map<String, Object>> objList = crawlerDAO.getKeywordJSON(topicSerial, sort, orderBy);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
		

		List<Map<String, Object>> kList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> kItem;
		keywordJSONStr = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			kItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			
			kItem.put("keywordSerial", obj[0].toString());
			kItem.put("topicSerial", obj[1].toString());
			kItem.put("keyword", obj[2]);
			kItem.put("enable", obj[3]);
			kItem.put("updateDate", obj[4]);
			kItem.put("editor", obj[5]);
			kItem.put("topic", obj[6]);
			kList.add(kItem);
		}
		
		rows = kList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		keywordJSONStr = new HashMap<String, Object>();
		keywordJSONStr.put("pageSize", pageSize);
		keywordJSONStr.put("total", total);
		keywordJSONStr.put("count", count);
		keywordJSONStr.put("currentPageNo", currentPageNo);
		keywordJSONStr.put("rows", rows);
		
		return "success";
	}
	
	public String initTopicSettings() {
		return "success";
	}
	
	/**
	 * JSON TYPE! for Dropdown function.
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getCompany() {
		
		List<Map<String, Object>> objList = crawlerDAO.getAllCompany(null, null);
		
		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> tList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> tItem;
		allCompanyJsonStr2 = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			tItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			
			tItem.put("companyId", obj[0].toString());
			tItem.put("companyName", obj[1]);
			tItem.put("companyName2", obj[2]);
			tItem.put("editor", obj[3]);
			tItem.put("updateDate", obj[4]);
			tList.add(tItem);
		}
		
		rows = tList;	

		allCompanyJsonStr2 = new HashMap<String, Object>();
		
		allCompanyJsonStr2.put("rows", rows);
		
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getAllCompany() {
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		List<Map<String, Object>> objList = crawlerDAO.getAllCompany(sort, orderBy);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
		

		List<Map<String, Object>> tList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> tItem;
		allCompanyJsonStr = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			tItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			
			tItem.put("companyId", obj[0].toString());
			tItem.put("companyName", obj[1]);
			tItem.put("companyName2", obj[2]);
			tItem.put("editor", obj[3]);
			tItem.put("updateDate", obj[4]);
			tList.add(tItem);
		}
		
		rows = tList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		allCompanyJsonStr = new HashMap<String, Object>();
		allCompanyJsonStr.put("pageSize", pageSize);
		allCompanyJsonStr.put("total", total);
		allCompanyJsonStr.put("count", count);
		allCompanyJsonStr.put("currentPageNo", currentPageNo);
		allCompanyJsonStr.put("rows", rows);
		
		return "success";
	}
	
	public String modifyCompany() {
		//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
		
		Company cBean = new Company();
		
		if(act.endsWith("add")) {
            UUID uuid = UUID.randomUUID();
	    	
	    	cBean.setCompanyId(uuid.toString().replaceAll("-", ""));
	    	cBean.setCompanyName(companyName);
	    	cBean.setCompanyName2(companyName2);
	    	cBean.setEditor(loggedInUser);
	    	cBean.setUpdateDate(new Date());
			try {
				crawlerDAO.modifyCompany(cBean, "add");
    			return "success";
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "success";
			
		} else if (act.equals("modify")) {
			cBean.setCompanyId(companyId);
	    	cBean.setCompanyName(companyName);
	    	cBean.setCompanyName2(companyName2);
	    	cBean.setEditor(loggedInUser);
	    	cBean.setUpdateDate(new Date());
			crawlerDAO.modifyCompany(cBean, "modify");
			
		} else if (act.equals("delete")) {
			cBean.setCompanyId(companyId);
			crawlerDAO.modifyCompany(cBean, "delete");
		}
		
		return "success";
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public String getAllTopicJSON() {
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		List<Map<String, Object>> objList = crawlerDAO.getAllTopic(categorySerial, topic, sort, orderBy);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
		

		List<Map<String, Object>> tList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> tItem;
		allTopicJSONStr = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			tItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			
			tItem.put("topicSerial", obj[0].toString());
			tItem.put("topic", obj[1]);
			tItem.put("updateDate", obj[2]);
			tItem.put("enable", obj[3]);
			tItem.put("editor", obj[4]);
			tItem.put("topicCategory", obj[5]);//category name
			//tItem.put("subCategory", obj[6]);//sub-category name
			tItem.put("subCategory", obj[5] +" - "+ obj[6]);
			tItem.put("companyName", obj[7]);
			tItem.put("companyName2", obj[8]);
			tList.add(tItem);
		}
		
		rows = tList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		allTopicJSONStr = new HashMap<String, Object>();
		allTopicJSONStr.put("pageSize", pageSize);
		allTopicJSONStr.put("total", total);
		allTopicJSONStr.put("count", count);
		allTopicJSONStr.put("currentPageNo", currentPageNo);
		allTopicJSONStr.put("rows", rows);
		
		return "success";
	}
	
	
	
	public String modifyQueryTopic() {
		//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
		
		
		QueryTopic qtBean = new QueryTopic();
		
		if(act.endsWith("add")) {
            UUID uuid = UUID.randomUUID();
	    	
	    	qtBean.setTopicSerial(uuid.toString().replaceAll("-", ""));
			qtBean.setTopic(topic);
			qtBean.setEnable(enable);
			qtBean.setUpdateDate(new Date());
			qtBean.setEditor(loggedInUser);
			qtBean.setTopicCategory(Integer.parseInt(topicCategory));
			qtBean.setSubCategory(subCategory);
			qtBean.setCompanyId(companyName);
			try {
				crawlerDAO.modifyQueryTopic(qtBean, "add");
    			return "success";
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "success";
			
		} else if (act.equals("modify")) {
			qtBean.setTopicSerial(topicSerial);
			qtBean.setTopic(topic);
			qtBean.setEnable(enable);
			qtBean.setUpdateDate(new Date());
			qtBean.setEditor(loggedInUser);
			qtBean.setTopicCategory(Integer.parseInt(topicCategory));
			qtBean.setSubCategory(subCategory);
			qtBean.setCompanyId(companyName);
			crawlerDAO.modifyQueryTopic(qtBean, "modify");
			
		} else if (act.equals("delete")) {
			qtBean.setTopicSerial(topicSerial);
			crawlerDAO.modifyQueryTopic(qtBean, "delete");
		}
		
		return "success";
	}

	public String modifyQuerySite() {
		//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
		
		QuerySite qsBean = new QuerySite();
		
		if(act.endsWith("add")) {
			qsBean.setTopicSerial(topicSerial);
			qsBean.setUrlName(urlName);
			qsBean.setUrl(url);
			qsBean.setEnable(enable);
			qsBean.setUpdateDate(new Date());
			qsBean.setEditor(loggedInUser);
			try {
				crawlerDAO.modifyQuerySite(qsBean, "add");
    			return "success";
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "success";
			
		} else if (act.equals("modify")) {
			qsBean.setSiteSerial(Long.parseLong(siteSerial));
			qsBean.setTopicSerial(topicSerial);
			qsBean.setUrlName(urlName);
			qsBean.setUrl(url);
			qsBean.setEnable(enable);
			qsBean.setUpdateDate(new Date());
			qsBean.setEditor(loggedInUser);
			crawlerDAO.modifyQuerySite(qsBean, "modify");
			
		} else if (act.equals("delete")) {
			qsBean.setSiteSerial(Long.parseLong(siteSerial));
			crawlerDAO.modifyQuerySite(qsBean, "delete");
		}
		
		return "success";
	}
	
	public String modifyQueryKeyword() {
		
		//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
		
		QueryKeyword qkBean = new QueryKeyword();
		
		if(act.endsWith("add")) {
			qkBean.setTopicSerial(topicSerial);
			qkBean.setKeyword(keyword);
			qkBean.setEnable(enable);
			qkBean.setUpdateDate(new Date());
			qkBean.setEditor(loggedInUser);
			try {
				crawlerDAO.modifyQueryKeyword(qkBean, "add");
    			return "success";
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "success";
			
		} else if (act.equals("modify")) {
			qkBean.setKeywordSerial(Long.parseLong(keywordSerial));
			qkBean.setTopicSerial(topicSerial);
			qkBean.setKeyword(keyword);
			qkBean.setEnable(enable);
			qkBean.setUpdateDate(new Date());
			qkBean.setEditor(loggedInUser);
			crawlerDAO.modifyQueryKeyword(qkBean, "modify");
			
		} else if (act.equals("delete")) {
			qkBean.setKeywordSerial(Long.parseLong(keywordSerial));
			crawlerDAO.modifyQueryKeyword(qkBean, "delete");
		}
		
		return "success";
	}
	
	//getter and setter
	
	public String getTopic() {
		return topic;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyName2() {
		return companyName2;
	}

	public void setCompanyName2(String companyName2) {
		this.companyName2 = companyName2;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getTopicSerial() {
		return topicSerial;
	}

	public void setTopicSerial(String topicSerial) {
		this.topicSerial = topicSerial;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public List<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public Map getAllTopicJSONStr() {
		return allTopicJSONStr;
	}

	public void setAllTopicJSONStr(Map allTopicJSONStr) {
		this.allTopicJSONStr = allTopicJSONStr;
	}

	public char getEnable() {
		return enable;
	}

	public void setEnable(char enable) {
		this.enable = enable;
	}

	public String getAct() {
		return act;
	}

	public void setAct(String act) {
		this.act = act;
	}

	public Map getSiteJSONStr() {
		return siteJSONStr;
	}

	public void setSiteJSONStr(Map siteJSONStr) {
		this.siteJSONStr = siteJSONStr;
	}

	public Map getKeywordJSONStr() {
		return keywordJSONStr;
	}

	public void setKeywordJSONStr(Map keywordJSONStr) {
		this.keywordJSONStr = keywordJSONStr;
	}

	public String getTopicCategory() {
		return topicCategory;
	}

	public void setTopicCategory(String topicCategory) {
		this.topicCategory = topicCategory;
	}

	public int getCategorySerial() {
		return categorySerial;
	}

	public void setCategorySerial(int categorySerial) {
		this.categorySerial = categorySerial;
	}

	public int getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(int subCategory) {
		this.subCategory = subCategory;
	}

	public String getUrlName() {
		return urlName;
	}

	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getKeyword_0() {
		return keyword_0;
	}

	public void setKeyword_0(String keyword_0) {
		this.keyword_0 = keyword_0;
	}

	public String getKeyword_1() {
		return keyword_1;
	}

	public void setKeyword_1(String keyword_1) {
		this.keyword_1 = keyword_1;
	}

	public String getKeyword_2() {
		return keyword_2;
	}

	public void setKeyword_2(String keyword_2) {
		this.keyword_2 = keyword_2;
	}

	public String getKeyword_3() {
		return keyword_3;
	}

	public void setKeyword_3(String keyword_3) {
		this.keyword_3 = keyword_3;
	}

	public String getKeyword_4() {
		return keyword_4;
	}

	public void setKeyword_4(String keyword_4) {
		this.keyword_4 = keyword_4;
	}

	public String getKeyword_5() {
		return keyword_5;
	}

	public void setKeyword_5(String keyword_5) {
		this.keyword_5 = keyword_5;
	}

	public String getSiteSerial() {
		return siteSerial;
	}

	public void setSiteSerial(String siteSerial) {
		this.siteSerial = siteSerial;
	}

	public String getKeywordSerial() {
		return keywordSerial;
	}

	public void setKeywordSerial(String keywordSerial) {
		this.keywordSerial = keywordSerial;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Map getAllCompanyJsonStr() {
		return allCompanyJsonStr;
	}

	public void setAllCompanyJsonStr(Map allCompanyJsonStr) {
		this.allCompanyJsonStr = allCompanyJsonStr;
	}

	public Map getAllCompanyJsonStr2() {
		return allCompanyJsonStr2;
	}

	public void setAllCompanyJsonStr2(Map allCompanyJsonStr2) {
		this.allCompanyJsonStr2 = allCompanyJsonStr2;
	}	
	
}
