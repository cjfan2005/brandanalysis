package com.trendplus.crawler.action;

import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

import com.trendplus.util.Util;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.FilePipeline;
import us.codecraft.webmagic.processor.PageProcessor;

/**
 * @author code4crafter@gmail.com <br>
 */
public class TEST implements PageProcessor {

    public static final String URL_LIST = "http://blog\\.sina\\.com\\.cn/s/articlelist_1487828712_0_\\d+\\.html";

    public static final String URL_POST = "http://blog\\.sina\\.com\\.cn/s/blog_\\w+\\.html";

    private Site site = Site
            .me()
            .setDomain("blog.sina.com.cn")
            .setSleepTime(3000)
            .setUserAgent(
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31");

    @Override
    public void process(Page page) {
        //列表页
        if (page.getUrl().regex(URL_LIST).match()) {
            page.addTargetRequests(page.getHtml().xpath("//div[@class=\"articleList\"]").links().regex(URL_POST).all());
            page.addTargetRequests(page.getHtml().links().regex(URL_LIST).all());
            //文章页
        } else {
            //page.putField("title", page.getHtml().xpath("//div[@class='articalTitle']/h2"));
            //page.putField("title", page.getHtml().xpath("//div[@class='articalTitle']/h2").toString());
            String a = page.getHtml().xpath("//div[@class='articalTitle']/h2").toString();
            System.out.println("a=>>>>" + Util.html2text(a));
            //page.putField("content", page.getHtml().xpath("//div[@id='articlebody']//div[@class='articalContent']").toString());
            String b = page.getHtml().xpath("//div[@id='articlebody']//div[@class='articalContent']").toString();
            System.out.println("b=>>>>>" + Util.html2text(b));
            page.putField("date", page.getHtml().xpath("//div[@id='articlebody']//span[@class='time SG_txtc']").regex("\\((.*)\\)"));
            String c = page.getHtml().xpath("//div[@id='articlebody']//span[@class='time SG_txtc']").regex("\\((.*)\\)").toString();
            System.out.println("c=>>>>>" + Util.html2text(c));
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
    	Date sDate= null;
    	String startDate = "2016/01/01";
    	sDate = Util.getDateFromStr(startDate+" 00:00:00");
    	System.out.println("Sdate=" + sDate);
    	
//        Spider.create(new TEST()).addUrl("http://blog.sina.com.cn/s/articlelist_1487828712_0_1.html").
//        			//addPipeline(new FilePipeline("/Users/cjfan/Desktop/webmagic")).
//        			thread(1)
//                .run();
    }
}
