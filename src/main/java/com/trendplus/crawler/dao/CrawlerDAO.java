package com.trendplus.crawler.dao;

import java.util.List;
import java.util.Map;

import com.trendplus.crawler.bean.Company;
import com.trendplus.crawler.bean.QueryKeyword;
import com.trendplus.crawler.bean.QuerySite;
import com.trendplus.crawler.bean.QueryTopic;

public interface CrawlerDAO {
	public List<Map<String, Object>> getAllCompany(String sort, String orderBy);
	
	public String modifyCompany(Company cBean, String act);
	
	public List<Map<String, Object>> getAllTopic(int categorySerial, String topic, String sort, String orderBy);

	public String modifyQueryTopic(QueryTopic qtBean, String act);
	
	public List<Map<String, Object>> getSiteJSON(String topicSerial, String sort, String orderBy);
	
	public List<Map<String, Object>> getKeywordJSON(String topicSerial, String sort, String orderBy);
	
	public String insertQueryCondition(List<QuerySite> qsList, List<QueryKeyword> qkList);
	
	public String modifyQuerySite(QuerySite qsBean, String act);
	
	public String modifyQueryKeyword(QueryKeyword qkBean, String act);
	
	
}
