package com.trendplus.crawler.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.trendplus.crawler.bean.Company;
import com.trendplus.crawler.bean.QueryKeyword;
import com.trendplus.crawler.bean.QuerySite;
import com.trendplus.crawler.bean.QueryTopic;
import com.trendplus.util.StringUtils;

public class CrawlerDAOImpl implements CrawlerDAO {
	private static final Logger logger = Logger.getLogger(CrawlerDAOImpl.class);
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/**
	 * Get All Company
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getAllCompany(String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT c.companyId, c.companyName, c.companyName2, c.editor, DATE_FORMAT(c.UpdateDate,'%Y-%m-%d %H:%I:%S') as UpdateDate ";
			sql += "FROM company AS c ";
            sql += "WHERE 1=1 ";
			
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + orderBy +";";
			} else {
				sql += " ORDER BY ";
				sql += " c.updateDate DESC;";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	public String modifyCompany(Company cBean, String act) {
		Session session = null;
		
		try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			
			
			if(act.equals("add")) {
				session.save(cBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("modify")) {
				//String dateStr = Util.convertDate2Str(new Date());
				session.update(cBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("delete")) {
				String companyId = cBean.getCompanyId();
				
				session.createSQLQuery("Delete from company WHERE companyId='" + companyId + "';").executeUpdate();
				t.commit();
			}
			
			
			
		} catch (HibernateException e) {
			logger.log(Level.ERROR, "modify Company=>" + e.getMessage());
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	} 
		}
		
		return "success";
	}
	
	/**
	 * Get All Topic Data
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getAllTopic(int categorySerial, String topic, String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT qt.TopicSerial, qt.Topic, DATE_FORMAT(qt.UpdateDate,'%Y-%m-%d %H:%I:%S') as UpdateDate, ";
			sql += "qt.Enable, qt.Editor, tc.Name, ts.SubName, c.companyName, c.companyName2 ";
			sql += "FROM query_topic AS qt ";
			sql += "LEFT JOIN company AS c ON c.companyId=qt.companyId ";
			sql += "LEFT JOIN topic_category AS tc ON tc.CategorySerial=qt.TopicCategory ";
			sql += "LEFT JOIN topic_subcategory AS ts ON ts.SubcategorySerial=qt.SubCategory ";
            sql += "WHERE 1=1 AND TopicSerial!='0'  AND TopicCategory!='0' ";
			
            if(categorySerial!=0)
				sql += " AND TopicCategory ='"+categorySerial+"'";
            
			if(StringUtils.isNotBlank(topic))
				sql += " AND Topic like '%" + topic + "%' ";
			
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + orderBy +";";
			} else {
				sql += " ORDER BY ";
				sql += " UpdateDate DESC;";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}

	
	
	public String modifyQueryTopic(QueryTopic qtBean, String act) {
		Session session = null;
		
		try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			
			
			if(act.equals("add")) {
				session.save(qtBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("modify")) {
				//String dateStr = Util.convertDate2Str(new Date());
				session.update(qtBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("delete")) {
				String topicSerial = qtBean.getTopicSerial();
				
				session.createSQLQuery("Delete from in_out_record WHERE TopicSerial='" + topicSerial + "';").executeUpdate();
				session.createSQLQuery("Delete from query_crawler WHERE TopicSerial='" + topicSerial + "';").executeUpdate();
				session.createSQLQuery("Delete from query_keyword WHERE TopicSerial='" + topicSerial + "';").executeUpdate();
				session.createSQLQuery("Delete from query_site WHERE TopicSerial='" + topicSerial + "';").executeUpdate();
				session.createSQLQuery("Delete from query_topic WHERE TopicSerial='" + topicSerial + "';").executeUpdate();
				session.createSQLQuery("Delete from sa_article WHERE TopicSerial='" + topicSerial + "';").executeUpdate();
				session.createSQLQuery("Delete from sa_domain_knowhow_topic WHERE TopicSerial='" + topicSerial + "';").executeUpdate();
				session.createSQLQuery("Delete from sa_keyword_frequency WHERE TopicSerial='" + topicSerial + "';").executeUpdate();
				session.createSQLQuery("Delete from sa_period WHERE TopicSerial='" + topicSerial + "';").executeUpdate();
				session.createSQLQuery("Delete from sa_phrase WHERE TopicSerial='" + topicSerial + "';").executeUpdate();
				session.createSQLQuery("Delete from sa_removed_keyword_map WHERE TopicSerial='" + topicSerial + "';").executeUpdate();
				session.createSQLQuery("Delete from sa_sentence WHERE TopicSerial='" + topicSerial + "';").executeUpdate();
				t.commit();
			}
			
			
			
		} catch (HibernateException e) {
			logger.log(Level.ERROR, "modifyQueryTopic=>" + e.getMessage());
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	} 
		}
		
		return "success";
	}
	
	//使用者新增欲做爬蟲的URL與Keywords
	public String insertQueryCondition(List<QuerySite> qsList, List<QueryKeyword> qkList) {
		Session sessionQs = null; 
		Session sessionQk = null;
		Transaction tQs = null; 
		Transaction tQk = null;
		
		try {
			sessionQs = sessionFactory.openSession();
			sessionQk = sessionFactory.openSession();
			tQs= sessionQs.beginTransaction();
			tQk= sessionQk.beginTransaction();
			
			QuerySite qs = null;
			
			QueryKeyword qk = null;
			
			for(int i=0; i<qsList.size(); i++) {
				qs = new QuerySite();
				qs.setUrlName(qsList.get(i).getUrlName());
				qs.setUrl(qsList.get(i).getUrl());
				qs.setEnable(qsList.get(i).getEnable());
				qs.setUpdateDate(qsList.get(i).getUpdateDate());
				qs.setEditor(qsList.get(i).getEditor());
				qs.setTopicSerial(qsList.get(i).getTopicSerial());
				sessionQs.save(qs);
			}
			
			for(int j=0; j<qkList.size(); j++) {
				qk = new QueryKeyword();
				qk.setKeyword(qkList.get(j).getKeyword());
				qk.setEnable(qkList.get(j).getEnable());
				qk.setUpdateDate(qkList.get(j).getUpdateDate());
				qk.setEditor(qkList.get(j).getEditor());
				qk.setTopicSerial(qkList.get(j).getTopicSerial());
				sessionQk.save(qk);
				
			}
			tQs.commit();
			tQk.commit();
			
			
		} catch (HibernateException e) {
			logger.log(Level.ERROR, "insertQueryCondition=>" + e.getMessage());
			tQs.rollback();
			tQk.rollback();
			
		} finally {
			if(sessionQs.isOpen())
				sessionQs.close();
			if(sessionQk.isOpen())
				sessionQk.close();
		}
		return "success";
	}
	
	
    public List<Map<String, Object>> getSiteJSON(String topicSerial, String sort, String orderBy) {
    	List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT qs.SiteSerial, qs.TopicSerial, qs.UrlName, qs.Url, qs.Enable, DATE_FORMAT(qs.UpdateDate,'%Y-%m-%d %H:%I:%S') as UpdateDate, qs.Editor, ";
			sql += "qt.Topic ";
			sql += "FROM query_site AS qs ";
			sql += "LEFT JOIN query_topic AS qt ON qt.TopicSerial=qs.TopicSerial ";
            sql += "WHERE 1=1 ";
			
            if(StringUtils.isNotBlank(topicSerial))
            	sql += "AND qs.TopicSerial ='"+topicSerial+"'";
			
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += "qs."+sort + " " + orderBy +";";
			} else {
				sql += " ORDER BY ";
				sql += " qs.SiteSerial ASC;";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
    }
	
    public List<Map<String, Object>> getKeywordJSON(String topicSerial, String sort, String orderBy) {
    	List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT qk.KeywordSerial, qk.TopicSerial, qk.Keyword, qk.Enable, DATE_FORMAT(qk.UpdateDate,'%Y-%m-%d %H:%I:%S') as UpdateDate, qk.Editor, ";
			sql += "qt.Topic ";
			sql += " FROM query_keyword AS qk ";
			sql += " LEFT JOIN query_topic AS qt ON qt.TopicSerial=qk.TopicSerial ";
            sql += "WHERE 1=1 ";
			
            if(StringUtils.isNotBlank(topicSerial))
            	sql += "AND qk.topicSerial ="+topicSerial+"";
			
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += "qk."+sort + " " + orderBy +";";
			} else {
				sql += " ORDER BY ";
				sql += " UpdateDate DESC;";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
    }
    
    public String modifyQuerySite(QuerySite qsBean, String act) {
    	Session session = null;
		
		try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			String strSql = "";
			
			if(act.equals("add")) {
				session.save(qsBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("modify")) {
				//String dateStr = Util.convertDate2Str(new Date());
				session.update(qsBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("delete")) {
				strSql = "Delete from query_site WHERE SiteSerial='" + qsBean.getSiteSerial() + "'";
			}
			
			session.createSQLQuery(strSql).executeUpdate();
			t.commit();
			
		} catch (HibernateException e) {
			logger.log(Level.ERROR, "modifyQuerySite=>" + e.getMessage());
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	} 
		}
    	return "success";
    }
	
	public String modifyQueryKeyword(QueryKeyword qkBean, String act) {
		Session session = null;
		
		try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			String strSql = "";
			
			if(act.equals("add")) {
				session.save(qkBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("modify")) {
				//String dateStr = Util.convertDate2Str(new Date());
				session.update(qkBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("delete")) {
				strSql = "Delete from query_keyword WHERE KeywordSerial='" + qkBean.getKeywordSerial() + "'";
			}
			
			session.createSQLQuery(strSql).executeUpdate();
			t.commit();
			
		} catch (HibernateException e) {
			logger.log(Level.ERROR, "modifyQueryKeyword=>" + e.getMessage());
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	} 
		}
    	return "success";
	}
	
}
