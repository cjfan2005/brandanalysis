package com.trendplus.general.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.trendplus.general.dao.GeneralDAO;

public class GeneralAction {

private String categorySerial, name, secondName, subCategorySerial, topicSerial;
	
private Map topicCategoryJsonMap, subCategoryJsonMap, topicJSONStr, periodJSONStr;

	
	@Autowired
	private GeneralDAO gDAO;

	@SuppressWarnings("unchecked")
	public String getPeriodJSON() {
		List<Map<String, Object>> objList = gDAO.getPeriodJSON(topicSerial);
		
		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> tcList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> tItem;
		periodJSONStr = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			tItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			
			tItem.put("periodId", obj[0].toString());
			tItem.put("periodName", obj[1]);
			tItem.put("duringTime", obj[2]+" ~ "+obj[3]);
			tcList.add(tItem);
		}
		
		periodJSONStr.put("periodJSONStr", tcList);
		
		return "success";
	}
	
	public String getTopicCategory() {
		List<Map<String, Object>> objList = gDAO.getTopicCategory();
		
		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> tcList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> tItem;
		topicCategoryJsonMap = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			tItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			
			tItem.put("categorySerial", obj[0].toString());
			tItem.put("name", obj[1]);
			tItem.put("secondName", obj[2]);
			tcList.add(tItem);
		}
		
		topicCategoryJsonMap.put("topicCategory", tcList);
		
		return "success";
	}
	
	//get subcategory
	public String getSubcategory() {
		
		List<Map<String, Object>> objList = gDAO.getTopicSubCategory(categorySerial);
		
		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> tcList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> tItem;
		subCategoryJsonMap = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			tItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			
			tItem.put("subCategorySerial", obj[0].toString());
			tItem.put("categorySerial", obj[1].toString());
			tItem.put("subName", obj[4] +" - "+ obj[2]);
			tItem.put("subName2", obj[3]);
			tcList.add(tItem);
		}
		
		subCategoryJsonMap.put("subCategory", tcList);
		
		return "success";
	}
	
	//without left join
	public String getSubcategory2() {
		
		List<Map<String, Object>> objList = gDAO.getTopicSubCategory2(categorySerial);
		
		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> tcList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> tItem;
		subCategoryJsonMap = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			tItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			
			tItem.put("subCategorySerial", obj[0].toString());
			tItem.put("categorySerial", obj[1].toString());
			tItem.put("subName", obj[2]);
			tItem.put("subName2", obj[3]);
			tcList.add(tItem);
		}
		
		subCategoryJsonMap.put("subCategory", tcList);
		
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getTopicJSON() {
		
		
		List<Map<String, Object>> objList = gDAO.getTopic(categorySerial, subCategorySerial);
		
		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> tList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> tItem;
		topicJSONStr = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			tItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			
			tItem.put("topicSerial", obj[0].toString());
			tItem.put("topic", obj[1]);
			tItem.put("updateDate", obj[2]);
			tList.add(tItem);
		}
		
		topicJSONStr.put("topicJSONStr", tList);
		
		return "success";
	}
	
	
	//getter and setter.
	public String getCategorySerial() {
		return categorySerial;
	}

	public void setCategorySerial(String categorySerial) {
		this.categorySerial = categorySerial;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public Map getTopicCategoryJsonMap() {
		return topicCategoryJsonMap;
	}

	public void setTopicCategoryJsonMap(Map topicCategoryJsonMap) {
		this.topicCategoryJsonMap = topicCategoryJsonMap;
	}

	public Map getSubCategoryJsonMap() {
		return subCategoryJsonMap;
	}

	public void setSubCategoryJsonMap(Map subCategoryJsonMap) {
		this.subCategoryJsonMap = subCategoryJsonMap;
	}

	public Map getTopicJSONStr() {
		return topicJSONStr;
	}

	public void setTopicJSONStr(Map topicJSONStr) {
		this.topicJSONStr = topicJSONStr;
	}

	public String getSubCategorySerial() {
		return subCategorySerial;
	}

	public void setSubCategorySerial(String subCategorySerial) {
		this.subCategorySerial = subCategorySerial;
	}

	public String getTopicSerial() {
		return topicSerial;
	}

	public void setTopicSerial(String topicSerial) {
		this.topicSerial = topicSerial;
	}

	public Map getPeriodJSONStr() {
		return periodJSONStr;
	}

	public void setPeriodJSONStr(Map periodJSONStr) {
		this.periodJSONStr = periodJSONStr;
	}
	
}
