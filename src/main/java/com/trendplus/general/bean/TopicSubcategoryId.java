package com.trendplus.general.bean;
// Generated Nov 17, 2016 10:42:16 AM by Hibernate Tools 4.3.4.Final

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * TopicSubcategoryId generated by hbm2java
 */
@Embeddable
public class TopicSubcategoryId implements java.io.Serializable {

	private int subcategorySerial;
	private int categorySerial;

	public TopicSubcategoryId() {
	}

	public TopicSubcategoryId(int subcategorySerial, int categorySerial) {
		this.subcategorySerial = subcategorySerial;
		this.categorySerial = categorySerial;
	}

	@Column(name = "SubcategorySerial", unique = true, nullable = false)
	public int getSubcategorySerial() {
		return this.subcategorySerial;
	}

	public void setSubcategorySerial(int subcategorySerial) {
		this.subcategorySerial = subcategorySerial;
	}

	@Column(name = "CategorySerial", nullable = false)
	public int getCategorySerial() {
		return this.categorySerial;
	}

	public void setCategorySerial(int categorySerial) {
		this.categorySerial = categorySerial;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TopicSubcategoryId))
			return false;
		TopicSubcategoryId castOther = (TopicSubcategoryId) other;

		return (this.getSubcategorySerial() == castOther.getSubcategorySerial())
				&& (this.getCategorySerial() == castOther.getCategorySerial());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getSubcategorySerial();
		result = 37 * result + this.getCategorySerial();
		return result;
	}

}
