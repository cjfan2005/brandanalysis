package com.trendplus.general.dao;

import java.util.List;
import java.util.Map;

public interface GeneralDAO {

	public List<Map<String, Object>> getTopicCategory();
	public List<Map<String, Object>> getTopicSubCategory(String categorySerial);
	public List<Map<String, Object>> getTopicSubCategory2(String categorySerial);
	public List<Map<String, Object>> getTopic(String topicCat, String subCat);
	public List<Map<String, Object>> getPeriodJSON(String topicSerial);
}
