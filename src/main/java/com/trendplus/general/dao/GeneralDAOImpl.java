package com.trendplus.general.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.trendplus.util.StringUtils;

public class GeneralDAOImpl implements GeneralDAO {
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getTopicCategory() {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT CategorySerial, Name, SecondName ";
			sql += "FROM topic_category ";
			sql += "Order by CategorySerial ASC;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	public List<Map<String, Object>> getTopicSubCategory(String categorySerial) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT sc.SubcategorySerial, sc.CategorySerial, sc.SubName, sc.SubName2, ";
			sql += "tc.Name ";
			sql += "FROM topic_subcategory as sc ";
			sql += "LEFT JOIN topic_category AS tc ON tc.CategorySerial=sc.CategorySerial ";
			
			sql += "WHERE 1=1 ";
			
			if(StringUtils.isNotBlank(categorySerial))
				sql += " AND sc.CategorySerial ="+categorySerial+"  ";
			
			
			sql += "Order by sc.SubcategorySerial ASC;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	//without Left join.
	public List<Map<String, Object>> getTopicSubCategory2(String categorySerial) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT sc.SubcategorySerial, sc.CategorySerial, sc.SubName, sc.SubName2 ";
			sql += "FROM topic_subcategory as sc ";
			sql += "WHERE 1=1 ";
			
			if(StringUtils.isNotBlank(categorySerial))
				sql += " AND (sc.CategorySerial=0 || sc.CategorySerial ="+categorySerial+")  ";
			
			
			sql += "Order by sc.SubcategorySerial ASC;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	/**
	 * Get Topic Data
	 * only get Enable=Y items.
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getTopic(String topicCat, String subCat) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT TopicSerial, Topic, DATE_FORMAT(UpdateDate,'%Y-%m-%d %H:%I:%S') as UpdateDate ";
			sql += "FROM query_topic ";
			sql += "WHERE Enable='Y' ";
			
			if(StringUtils.isNotBlank(topicCat))
				sql += " AND (TopicCategory=0 || TopicCategory ="+topicCat+")  ";
			
			if(StringUtils.isNotBlank(subCat))
				sql += " AND (SubCategory=0 || SubCategory ="+subCat+")  ";
			
			sql += "Order by TopicSerial ASC";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	/**
	 * Get Topic Data
	 * only get Enable=Y items.
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getPeriodJSON(String topicSerial) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT PeriodId, PeriodName, ";
			sql += "DATE_FORMAT(StartDate,'%Y-%m-%d') as StartDate, ";
			sql += "DATE_FORMAT(EndDate,'%Y-%m-%d') as EndDate ";
			sql += "FROM sa_period ";
			sql += "WHERE ";
			sql += "TopicSerial='"+topicSerial+"' OR TopicSerial='0'";
			sql += "Order by StartDate ASC;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
}
