package com.trendplus.quartz.dao;

public interface QuartzDAO {
	
	public String removeSentenceByDay();
	
	public String removeKeywordByDay();
}
