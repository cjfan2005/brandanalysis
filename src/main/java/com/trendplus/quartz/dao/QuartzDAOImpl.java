package com.trendplus.quartz.dao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Level;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class QuartzDAOImpl implements QuartzDAO{
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public String removeSentenceByDay() {
		String checkDate;
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, -5);
		checkDate = new SimpleDateFormat("yyyy/MM/dd").format(c.getTime());

		checkDate = checkDate.replace("/", "-")+ " 23:59:59";
		//System.out.println("checkDate:"+checkDate);
		
		Session session = null;
		String sql = "";
		
		sql += "DELETE FROM sa_sentence WHERE ";
		sql += " InsertDate <= '"+ checkDate + "' AND ";
		sql += " (CkipTermFiltered IS NULL OR CkipTermFiltered='') AND ";
		sql += " (IntentionNTUSD IS NULL OR IntentionNTUSD='') AND ";
		sql += " (HowNetScore IS NULL OR HowNetScore='');";
		
		
		try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.createSQLQuery(sql).executeUpdate();
			t.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		
		return "success";
	}
	
	public String removeKeywordByDay() {
		String checkDate;
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, -5);
		checkDate = new SimpleDateFormat("yyyy/MM/dd").format(c.getTime());

		checkDate = checkDate.replace("/", "-")+ " 23:59:59";
		//System.out.println("checkDate:"+checkDate);
		
		Session session = null;
		String sql = "";
		
		sql += "DELETE FROM sa_keyword_frequency WHERE ";
		sql += " InsertDate <= '"+ checkDate + "' AND ";
		sql += " (DomainKnowhowId IS NULL OR DomainKnowhowId ='');";	
		
		try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.createSQLQuery(sql).executeUpdate();
			t.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		
		return "success";
	}
}
