package com.trendplus.quartz.job;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.trendplus.quartz.dao.QuartzDAO;

/**
 * 
 * @author kent
 * Execute at 03:00AM every day .
 */
public class ScheduleRemoveKeyword implements Job {
	
	private static final Logger logger = Logger.getLogger(ScheduleRemoveKeyword.class);

	public void execute(JobExecutionContext context) throws JobExecutionException {
//		logger.log(Level.INFO, "SCHEDULE: "
//				+ "<ScheduleFetchReadyJob> activated at: <"+ context.getFireTime().toString() +">"
//				+ ", next scheduled firing time: <"+ context.getNextFireTime().toString() + ">");

		QuartzDAO qzDAO = null;

		boolean isRun = true;
//		boolean isRun = false;
		
		try {
			qzDAO = (QuartzDAO) context.getScheduler().getContext().get("quartzDAO");
		}catch (Exception e){
			logger.log(Level.ERROR, "Could not load required services, stopping task...");
			isRun = false;
		}
		
		if (isRun) {
			String algorithmResult = qzDAO.removeKeywordByDay();
			algorithmResult = qzDAO.removeSentenceByDay();
			if(algorithmResult.equals("success")) {
				logger.log(Level.INFO, "Completed Remove old database data.");
			} else {
				logger.log(Level.ERROR, "Failed Remove old database data.");
			}
		}
	}
}
