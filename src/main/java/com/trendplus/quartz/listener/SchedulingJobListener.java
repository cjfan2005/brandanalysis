package com.trendplus.quartz.listener;

import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.trendplus.quartz.job.ScheduleRemoveKeyword;
import com.trendplus.quartz.dao.QuartzDAO;

/**
 * Application Lifecycle Listener implementation class SchedulingJobListener
 *
 */
public class SchedulingJobListener implements ServletContextListener {

	public static WebApplicationContext wa;
	private QuartzDAO qzDAO;
	
    /**
     * Default constructor. 
     */
    public SchedulingJobListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0)  { 
    	
		wa = WebApplicationContextUtils.getRequiredWebApplicationContext(arg0.getServletContext());

		qzDAO = wa.getBean(QuartzDAO.class);
		
		JobDetail job = JobBuilder.newJob(ScheduleRemoveKeyword.class)
				.withIdentity("SchedulingPrerunJob", "defaultGroup").build();

			try {
				//temp, could be moved to a property file
				Properties p = new Properties();
				p.setProperty("org.quartz.threadPool.threadCount", "2");

				Trigger trigger = TriggerBuilder
				  .newTrigger()
				  .withIdentity("defaultSchedulingTrigger", "defaultGroup")
				  .withSchedule(CronScheduleBuilder.cronSchedule("0 00 03 * * ?")) //03:00AM
				  //.withSchedule(CronScheduleBuilder.cronSchedule("0 0/1 * * * ?"))//每分鐘執行
				  .build();

				Scheduler scheduler = new StdSchedulerFactory(p).getScheduler();
				
				scheduler.getContext().put("quartzDAO", qzDAO);
				
				scheduler.start();
				scheduler.scheduleJob(job, trigger);

			} catch (SchedulerException e) {
				e.printStackTrace();
			}
		    	
    }
	
}
