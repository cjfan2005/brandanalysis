package com.trendplus.semantic.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.trendplus.general.bean.InOutRecord;
import com.trendplus.semantic.bean.*;
import com.trendplus.semantic.dao.SemanticDAO;
import com.trendplus.util.PageParameter;
import com.trendplus.util.SemanticUtil;
import com.trendplus.util.Util;

public class SemanticAction {
	private String articleId, topicSerial, title, topic,typeOfArticle,content;
	private String act, keywordId, keyword, articleIdGroup, delKwCondition;
	private String domainKnowhowId, domainKnowhowName;
	private String periodId, periodName, startDate, endDate;
	private int categorySerial, subCategorySerial;
	private String phraseId, phrase, partOfSpeech;
	private String removedId, removeItem;
	private String inOutId;
	private int inQuantity, outQuantity;
	
	
	private Map result, result2, result3, result4, domainKnowhowJson, s_dkh, ss_dkh;
	
	@Autowired
	SemanticDAO sDAO;
	
	// jqgrid
	private int total = 0; // 總頁數
	private int records = 0; // 資料總筆數
	private int pageSize = 0;
	private int currentPageNo = 0;
	private String sort;
	private String orderBy;
	private List<Map<String, Object>> rows;
	
	public String deleteArticleById() {
		if(articleId.isEmpty() || articleId==null)
			return "input";
		
		String res = sDAO.deleteArticleById(articleId);
		return res;
	}
	
	public String initGetPeriod() {
		
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getPeriod() {
		
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		if(topicSerial==null || topicSerial.isEmpty())
			return null;
		
		List<Map<String, Object>> objList = sDAO.getPeriod(topicSerial, sort, orderBy);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> pList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> pItem;
		result = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			pItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			pItem.put("periodId", obj[0].toString());
			pItem.put("topicSerial", obj[1]);
			pItem.put("periodName", obj[2]);
			pItem.put("startDate", obj[3]);
			pItem.put("endDate", obj[4]);
			pItem.put("editor", obj[5]);
			pItem.put("topic", obj[6]);
			pList.add(pItem);
		}
		
		rows = pList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);
		
		return "success";
	}
	
	public String modifyPeriod() {
		//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
		
		SaPeriod pBean = new SaPeriod();
		Date sDate=null, eDate=null;
		
		if(act.endsWith("add")) {
			sDate = Util.getDateFromStr(startDate.replace("-", "/") + " 00:00:00");
			eDate = Util.getDateFromStr(endDate.replace("-", "/") + " 23:59:59");
			
			UUID uuid = UUID.randomUUID();
	    	
	    	pBean.setPeriodId(uuid.toString().replaceAll("-", ""));
	    	pBean.setTopicSerial(topicSerial);
	    	pBean.setPeriodName(periodName);
	    	pBean.setStartDate(sDate);
	    	pBean.setEndDate(eDate);
			pBean.setEditor(loggedInUser);
			
			try {
				sDAO.modifyPeriod(pBean, "add");
    			return "success";
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "success";
			
		} else if (act.equals("modify")) {
			sDate = Util.getDateFromStr(startDate.replace("-", "/")+" 00:00:00");
			eDate = Util.getDateFromStr(endDate.replace("-", "/")+" 23:59:59");
			
			pBean.setPeriodId(periodId);
	    	pBean.setTopicSerial(topicSerial);
	    	pBean.setPeriodName(periodName);
	    	pBean.setStartDate(sDate);
	    	pBean.setEndDate(eDate);
			pBean.setEditor(loggedInUser);
			
			sDAO.modifyPeriod(pBean, "modify");
			
		} else if (act.equals("delete")) {
			pBean.setPeriodId(periodId);
			sDAO.modifyPeriod(pBean, "delete");
		}
		
		return "success";
	}
	
	//InOutRecord
	@SuppressWarnings("unchecked")
	public String getInOutRecordById() {
		
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		if(topicSerial==null || topicSerial.isEmpty())
			return null;
		
		List<Map<String, Object>> objList = sDAO.getInOutRecord(topicSerial, sort, orderBy);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> pList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> pItem;
		result3 = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			pItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			pItem.put("inOutId", obj[0].toString());
			pItem.put("periodId", obj[1]);
			pItem.put("topic", obj[2]);
			pItem.put("inQuantity", obj[3]);
			pItem.put("outQuantity", obj[4]);
			pItem.put("editor", obj[5]);
			pItem.put("updateDate", obj[6]);
			pItem.put("periodName", obj[7]);
			pItem.put("startDate", obj[8]);
			pItem.put("endDate", obj[9]);
			pItem.put("topicSerial", obj[10]);
			pList.add(pItem);
		}
		
		rows = pList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result3 = new HashMap<String, Object>();
		result3.put("pageSize", pageSize);
		result3.put("total", total);
		result3.put("count", count);
		result3.put("currentPageNo", currentPageNo);
		result3.put("rows", rows);
		
		return "success";
	}
	
	public String modifyInOutRecord() {
		//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
		
		InOutRecord iBean = new InOutRecord();
		
		if(act.endsWith("add")) {
			
			UUID uuid = UUID.randomUUID();
	    	
			iBean.setInOutId(uuid.toString().replaceAll("-", ""));
			iBean.setTopicSerial(topicSerial);
			iBean.setPeriodId(periodName);
			iBean.setInQuantity(inQuantity);
			iBean.setOutQuantity(outQuantity);
			iBean.setEditor(loggedInUser);
			iBean.setUpdateDate(new Date());
			
			try {
				sDAO.modifyInOutRecord(iBean, "add");
    			return "success";
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "success";
			
		} else if (act.equals("modify")) {

			iBean.setInOutId(inOutId);
			iBean.setTopicSerial(topicSerial);
			iBean.setPeriodId(periodName);
			iBean.setInQuantity(inQuantity);
			iBean.setOutQuantity(outQuantity);
			iBean.setEditor(loggedInUser);
			iBean.setUpdateDate(new Date());
			
			sDAO.modifyInOutRecord(iBean, "modify");
			
		} else if (act.equals("delete")) {
			iBean.setInOutId(inOutId);
			sDAO.modifyInOutRecord(iBean, "delete");
		}
		
		return "success";
	}
	
	public String initAddArticle() {
		return "success";
	}
	
	public String addArticle() {
		
		//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
		
		List<SaArticle> aList = new ArrayList<SaArticle>();
		SaArticle a = new SaArticle();
		
		a.setCategorySerial(categorySerial);
		a.setSubcategorySerial(subCategorySerial);
		a.setTopicSerial(topicSerial);
		a.setPeriodId(periodId);//PeriodId
		a.setTitle(title);
		
		//去除標點符號並斷行
		content = content.replaceAll("(?i)[^a-zA-Z0-9\u4E00-\u9FA5]", " ").trim();
		a.setContent(content);
		a.setEditor(loggedInUser);
		a.setStatus(1);
		aList.add(a);
		
		String result = sDAO.insertArticle(aList);
		if(!result.equals("success")) {
			return "input";
		}
		
		return "success";
	}
		
	public String initGetArticle() {
		
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getArticle() {
		
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		if(topicSerial==null || topicSerial.isEmpty())
			return null;
		
		List<Map<String, Object>> objList = sDAO.getArticle(topicSerial, periodId, sort, orderBy);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> aList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> aItem;
		result = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			aItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			aItem.put("articleId", obj[0].toString());
			aItem.put("periodId", obj[1]);
			aItem.put("title", obj[2]);
			aItem.put("content", obj[3]);
			aItem.put("editor", obj[4]);
			aItem.put("insertDate", obj[5]);
			aItem.put("updateDate", obj[6]);
			aItem.put("topic", obj[7]);
			aItem.put("status", obj[8]);
			aItem.put("period", obj[9]+" ["+obj[10]+"~"+obj[11]+"]");
			aItem.put("periodName", obj[9]);
			aItem.put("startDate", obj[10]);
			aItem.put("endDate", obj[11]);
			
			aList.add(aItem);
		}
		
		rows = aList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);
		
		return "success";
	}
	
    public String initSentenceDetail() {
		
		return "success";
	}
	
    @SuppressWarnings("unchecked")
	public String sentenceDetail() {
    	
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		if(articleId.isEmpty() || articleId.trim().length()==0)
			return null;
		
		List<Map<String, Object>> objList = sDAO.getSentenceById(articleId);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> sList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> sItem;
		result = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			sItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			sItem.put("sentenceId", obj[0].toString());
			sItem.put("articleId", obj[1].toString());
			sItem.put("topic", obj[2].toString());
			sItem.put("content", obj[3]);
			sItem.put("ckipTerm", obj[4]);
			sItem.put("ckipTermFiltered", obj[5]);
			sItem.put("intentionNTUSD", obj[6]);
			sItem.put("howNetScore", obj[7]);
			sList.add(sItem);
		}
		
		rows = sList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);
		
		return "success";
	}
    
    public String tfDfCalculator(){
    	/**
    	 * Check Articles in which Status.
    	 * 若有LOCKED(status=3)的項目,
    	 * counter>0 表示TF/DF執行中,不可再次執行。
    	 */
    	int counter = sDAO.checkArticleList(topicSerial, periodId);
    	
    	if( counter > 0 ) {
    		return "input";
    	} else {
    		sDAO.DFTF_CallCenter(topicSerial, periodId);
    	}
    	return "success";
    }
    
    public String initGetTfDf() {
    	return "success";
    }
    
    @SuppressWarnings("unchecked")
	public String getTfDf() {
    	if(topicSerial==null ||topicSerial.isEmpty() )
			return null;
    	
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
        List<Map<String, Object>> objList = sDAO.getTfDfById(topicSerial, periodId, sort, orderBy);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> kList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> kItem;
		result = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			kItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			kItem.put("keywordId", obj[0].toString());
			kItem.put("articleIdGroup", obj[1].toString());
			kItem.put("topicSerial", obj[2].toString());
			kItem.put("keyword", obj[3]);
			kItem.put("domainKnowhowName", obj[4]);//domain-knowhow-name
			kItem.put("termFrequency", obj[5]);
			kItem.put("docFrequency", obj[6]);
			kItem.put("sentimentScore", obj[7]);
			kItem.put("editor", obj[8]);
			kItem.put("updateDate", obj[9]);
			kItem.put("periodId", obj[10]);
			kItem.put("period", obj[11] + " [" +obj[12]+" ~ " + obj[13]+"]");
			
			kList.add(kItem);
		}
		
		rows = kList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);
		
    	return "success";
    }
    
    public String initCkipKeywordDetail() {
    	return "success";
    }
    
    /**
     * 尋找所有特定keyword在sa_sentence出現的句子
     * @return
     */
    @SuppressWarnings("unchecked")
	public String ckipKeywordDetail() {
    	
    	if(articleIdGroup==null ||articleIdGroup.isEmpty() ||
    			keyword==null || keyword.isEmpty()	)
			return null;
    	
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		/**
		 * 處理ArticleId. ex: 18,21,23轉化成'18','21','23'
		 * 因為articleId為varchar(32)格式
		 */
		StringBuffer sb = new StringBuffer();
		String[] aSplit = articleIdGroup.split(",");
		for(String tmpId : aSplit) {
			sb.append("'"+tmpId+"',");
		}
		articleIdGroup = sb.toString().substring(0, sb.length()-1);
		
        List<Map<String, Object>> objList = sDAO.getCkipKeywordDetail(keyword, periodId, articleIdGroup, sort, orderBy);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> kList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> kItem;
		result = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			kItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			kItem.put("sentenceId", obj[0].toString());
			kItem.put("articleId", obj[1].toString());
			kItem.put("topicSerial", obj[2].toString());
			kItem.put("content", obj[3]);
			kItem.put("ckipTerm", obj[4]);
			kItem.put("ckipTermFiltered", obj[5]);
			kItem.put("intentionNTUSD", obj[6]);//domain-knowhow-name
			kItem.put("howNetScore", obj[7]);
			kItem.put("insertDate", obj[8]);
			kItem.put("updateDate", obj[9]);
			kItem.put("periodId", obj[10]);
			kList.add(kItem);
		}
		
		rows = kList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);
		
    	return "success";
    }
    
    /**
     * 取得欲計算情緒分數的所有句子(Sa_Sentence)
     * return all keyword sentence List
     */
    public List<Map<String,Object>> getKeywordToCalHowNetNtusd() {
    	
    	if(articleIdGroup==null ||articleIdGroup.isEmpty() ||
    			keyword==null || keyword.isEmpty()	)
			return null;
    	
    	/**
		 * 處理ArticleId. ex: 18,21,23轉化成'18','21','23'
		 * 因為articleId為varchar(32)格式
		 */
		StringBuffer sb = new StringBuffer();
		String[] aSplit = articleIdGroup.split(",");
		for(String tmpId : aSplit) {
			sb.append("'"+tmpId+"',");
		}
		articleIdGroup = sb.toString().substring(0, sb.length()-1);
    	
    	List<Map<String, Object>> objList = sDAO.getCkipKeywordDetail(keyword, periodId, articleIdGroup, sort, orderBy);
    	
    	List<Map<String, Object>> kList = new ArrayList<Map<String, Object>>();
    	Iterator<?> its = objList.iterator();
		
		Map<String, Object> kItem;
		result = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			kItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			kItem.put("sentenceId", obj[0].toString());
			kItem.put("articleId", obj[1].toString());
			kItem.put("topicSerial", obj[2].toString());
			kItem.put("content", obj[3]);
			kItem.put("ckipTerm", obj[4]);
			kItem.put("ckipTermFiltered", obj[5]);
			kItem.put("intentionNTUSD", obj[6]);//domain-knowhow-name
			kItem.put("howNetScore", obj[7]);
			kItem.put("insertDate", obj[8]);
			kItem.put("updateDate", obj[9]);
			kItem.put("periodId", obj[10]);
			kList.add(kItem);
		}
    	return kList;
    }
    /**
     * keyword在sa_sentence出現的句子
     * 計算句子howNet與ntusd
     * @return
     */
    public String calHowNetNTUSD() {
    	
    	List<Map<String, Object>> kList = getKeywordToCalHowNetNtusd();
    	List<SaHowNetMap> howNetList= SemanticUtil.parseHowNetMapExcel();
    	List<SaNtusdMap> ntusdList= SemanticUtil.parseNTUSDMapExcel();
    	SaSentence sBean = null;
    	Date cd = new Date();
    	List<SaSentence> sList = new ArrayList<SaSentence>();
    	String content="";
	    double value=0, intentionNTUSD=0, howNetScore=0;
    	for(int i=0; i<kList.size(); i++) {
    		
    		content = kList.get(i).get("content").toString();
    		
    		//HowNet程度詞
    		for(int j=0; j<howNetList.size(); j++) {
    			String tnHN = howNetList.get(j).getTraditionalName(), 
    				   snHN=howNetList.get(j).getSimplifiedName();
        		double valHN = howNetList.get(j).getValue();
        		
        		if(content.contains(tnHN)) {
        			value = valHN;
        			howNetScore = valHN;
        		} else if(content.contains(snHN)) {
        			value = valHN;
        			howNetScore = valHN;
        		} else {
        			value = 0;
        		}
    		}
    		
    		//NTUSD
    		for(int k=0; k<ntusdList.size(); k++) {
    			String tnSD = ntusdList.get(k).getTraditionalName(), 
     				   snSD = ntusdList.get(k).getSimplifiedName();
         		double valSD = ntusdList.get(k).getValue();
         		
    			if(content.contains(tnSD)) {
    				value = value*valSD;
    				
    				if(valSD==1) 
    					intentionNTUSD=1;
    				else
    					intentionNTUSD=-1;
    			}else if(content.contains(snSD)) {
    				value = value*valSD;
    				
    				if(valSD==1) 
    					intentionNTUSD=1;
    				else
    					intentionNTUSD=-1;
    				
    			} else {
    				value = 0;
    			}
    		}
    		
    		//arrange to Sa_sentence Bean
    		sBean = new SaSentence();
    	    sBean.setSentenceId(kList.get(i).get("sentenceId").toString());
    	    sBean.setArticleId(kList.get(i).get("articleId").toString());
    	    sBean.setTopicSerial(kList.get(i).get("topicSerial").toString());
    	    sBean.setContent(kList.get(i).get("content").toString());
    	    sBean.setCkipTerm(kList.get(i).get("ckipTerm").toString());
    	    sBean.setCkipTermFiltered(kList.get(i).get("ckipTermFiltered").toString());
    	    sBean.setIntentionNtusd(intentionNTUSD);
    	    sBean.setHowNetScore(howNetScore);
    	    if(kList.get(i).get("insertDate")!=null)
    	    	sBean.setInsertDate(Util.getDateFromStr(kList.get(i).get("insertDate").toString().replaceAll("-", "/")));
    	    else
    	    	sBean.setInsertDate(cd);
    	    
    	    sBean.setUpdateDate(cd);
    	    
    	    if(kList.get(i).get("periodId")!=null)
    	    	sBean.setPeriodId(kList.get(i).get("periodId").toString());
    	    else 
    	    	sBean.setPeriodId("");
    	    	
    	    sList.add(sBean);
    	}
    	sDAO.insertHowNetNTUSD(sList,keywordId);
    	
    	//所計算的topic, topicSearial回傳到頁面上,
    	//主要是給iniGetTfDf()跳轉頁面時使用！
    	if(!topic.isEmpty() && !topicSerial.isEmpty()) {
    		String[] splitTopicStr = topic.split(",");
    		String[] splitTopicSerialStr = topicSerial.split(",");
    		
    		topic = splitTopicStr[1].trim();
    		topicSerial = splitTopicSerialStr[1].trim();
    	}
    	return "success";
    }
    
    /**
	 * 刪除不必要的keyword in SaKeywordFreq.
	 */
    public String deleteKeywordById() {
    	
    	if(delKwCondition==null || delKwCondition.isEmpty())
    		return "input";
    	
    	//jqgrid頁面傳來delKwCondition = kId + "/"+ kw + "/" + ts;
    	String[] splitStr = delKwCondition.split("/");
    	keywordId = splitStr[0];
    	keyword = splitStr[1];
    	topicSerial = splitStr[2];
    	String result = sDAO.deleteKeywordById(keywordId, keyword, topicSerial);
    	
    	return result;
    }
    
    
    /**
     * 列出domain knowhow topic 項目
     * @return
     */
    @SuppressWarnings("unchecked")
	public String getDomainKnowhowById() {
    	if(topicSerial==null || topicSerial.isEmpty() )
    		return "success";
    	
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
        List<Map<String, Object>> objList = sDAO.getDomainKnowhowById(topicSerial, sort, orderBy);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> dList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> dItem;
		result2 = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			dItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			dItem.put("domainKnowhowId", obj[0].toString());
			dItem.put("topicSerial", obj[1].toString());
			dItem.put("topic", obj[2].toString());//from query_topic table.
			dItem.put("domainKnowhowName", obj[3].toString());//domain-knowhow-name
			dItem.put("editor", obj[4]);
			dItem.put("updateDate", obj[5]);
			dList.add(dItem);
		}
		
		rows = dList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result2 = new HashMap<String, Object>();
		result2.put("pageSize", pageSize);
		result2.put("total", total);
		result2.put("count", count);
		result2.put("currentPageNo", currentPageNo);
		result2.put("rows", rows);
		
    	return "success";
    }
    
    public String modifyDomainKnowhowTopic() {
    	
    	//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
		Date currentDate = new Date();
		SaDomainKnowhowTopic dBean = new SaDomainKnowhowTopic();
		
		if(act.endsWith("add")) {
			UUID uuid = UUID.randomUUID();
	    	
	    	dBean.setDomainKnowhowId(uuid.toString().replaceAll("-", ""));
	    	dBean.setTopicSerial(topicSerial);
	    	dBean.setName(domainKnowhowName);
			dBean.setEditor(loggedInUser);
			dBean.setUpdateDate(currentDate);
			
			try {
				sDAO.modifyDomainKnowhowTopic(dBean, "add");
    			return "success";
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "success";
			
		} else if (act.equals("modify")) {
			dBean.setDomainKnowhowId(domainKnowhowId);
	    	dBean.setTopicSerial(topicSerial);
	    	dBean.setName(domainKnowhowName);
			dBean.setEditor(loggedInUser);
			dBean.setUpdateDate(currentDate);
			sDAO.modifyDomainKnowhowTopic(dBean, "modify");
			
		} else if (act.equals("delete")) {
			dBean.setDomainKnowhowId(domainKnowhowId);
			sDAO.modifyDomainKnowhowTopic(dBean, "delete");
		}
		
		return "success";
    }
    
    @SuppressWarnings("unchecked")
	public String getDomainKnowhowByIdJson() {
    	if(topicSerial.isEmpty() || topicSerial==null)
    		return "success";
    	
        List<Map<String, Object>> objList = sDAO.getDomainKnowhowByIdJson(topicSerial);
		
		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> dList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> dItem;
		domainKnowhowJson = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			dItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			
			dItem.put("domainKnowhowId", obj[0].toString());
			dItem.put("topicSerial", obj[1]);
			dItem.put("name", obj[2]);
			dList.add(dItem);
		}
		
		domainKnowhowJson.put("domainKnowhowJson", dList);
		
    	return "success";
    }
    
    public String updateKeywordFrequencyDomainKnowhow() {
    	//這裡的domainKnowhowName會從頁面jsgrid帶入domainKnowhowId,
    	//因為要與jqgrid名稱相同, 所以name=id解決
    	if(keywordId.isEmpty() || keywordId==null ||
    			domainKnowhowName.isEmpty() || domainKnowhowName==null)
    		return "success";
    	
    	sDAO.updateKeywordFrequencyDomainKnowhow(keywordId, domainKnowhowName);
    	return "success";
    }
    
    /**
     * 依據TopicSerial, periodid
     * 抓取議題詞統計資料主要page.
     * @return
     */
    @SuppressWarnings("unchecked")
	public String getStatisticDomainKnowhowById() {
    	if(topicSerial.isEmpty() || topicSerial==null ||
    			periodId.isEmpty() || periodId ==null )
    		return "success";
    	
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
        List<Map<String, Object>> objList = sDAO.getStatisticDomainKnowhowById(topicSerial, periodId);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> kList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> kItem;
		s_dkh = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			kItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			kItem.put("domainKnowhowId", obj[0]);
			kItem.put("domainKnowhowName", obj[1]);
			kItem.put("counter", obj[2]);
			kItem.put("totalScore", obj[3]);
				
			kItem.put("positiveScore", obj[4]);
			kItem.put("negativeScore", obj[5]);
			
			kItem.put("topicSerial", obj[6]);
			kItem.put("periodId", obj[7]);
			
			kList.add(kItem);
		}
		
		rows = kList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		s_dkh = new HashMap<String, Object>();
		s_dkh.put("pageSize", pageSize);
		s_dkh.put("total", total);
		s_dkh.put("count", count);
		s_dkh.put("currentPageNo", currentPageNo);
		s_dkh.put("rows", rows);
		
    	return "success";
    }
    
    /**
     * 依據TopicSerial, 
     * 抓取議題詞統計資料主要page.
     * @return
     */
    @SuppressWarnings("unchecked")
	public String getAllStatisticDomainKnowhowById() {
    	if(topicSerial.isEmpty() || topicSerial==null)
    		return "success";
    	
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
        List<Map<String, Object>> objList = sDAO.getAllStatisticDomainKnowhowById(topicSerial);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> kList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> kItem;
		s_dkh = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			kItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			kItem.put("domainKnowhowId", obj[0]);
			kItem.put("domainKnowhowName", obj[1]);
			kItem.put("counter", obj[2]);
			kItem.put("totalScore", obj[3]);
				
			kItem.put("positiveScore", obj[4]);
			kItem.put("negativeScore", obj[5]);
			
			kItem.put("topicSerial", obj[6]);
			
			kList.add(kItem);
		}
		
		rows = kList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		s_dkh = new HashMap<String, Object>();
		s_dkh.put("pageSize", pageSize);
		s_dkh.put("total", total);
		s_dkh.put("count", count);
		s_dkh.put("currentPageNo", currentPageNo);
		s_dkh.put("rows", rows);

    	return "success";
    }
    
    /**
     * subGrid for getStatisticDomainKnowhowById() method.
     * @return
     */
    @SuppressWarnings("unchecked")
	public String getKeywordsFormStatistic() {
    	if(topicSerial.isEmpty() || topicSerial==null ||
    			periodId.isEmpty() || periodId ==null ||
    			domainKnowhowId.isEmpty() || domainKnowhowId == null)
    		return "success";
    	
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
        List<Map<String, Object>> objList = sDAO.getKeywordsFromStatistic(topicSerial, domainKnowhowId, periodId);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> kList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> kItem;
		ss_dkh = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			kItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			//kItem.put("keywordId", obj[0]);
			kItem.put("keyword", obj[0]);
			kItem.put("termFrequency", obj[1]);
			kItem.put("docFrequency", obj[2]);
			//kItem.put("positiveNTUSD", obj[4]);
			//kItem.put("negativeNTUSD", obj[5]);
			
			kList.add(kItem);
		}
		
		rows = kList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		ss_dkh = new HashMap<String, Object>();
		ss_dkh.put("pageSize", pageSize);
		ss_dkh.put("total", total);
		ss_dkh.put("count", count);
		ss_dkh.put("currentPageNo", currentPageNo);
		ss_dkh.put("rows", rows);
		
    	return "success";
    }
    
    /**
     * subGrid for getAllStatisticDomainKnowhowById() method.
     * @return
     */
    @SuppressWarnings("unchecked")
	public String getKeywordsFormAllStatistic() {
    	if(topicSerial.isEmpty() || topicSerial==null ||
    			domainKnowhowId.isEmpty() || domainKnowhowId == null)
    		return "success";
    	
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
        List<Map<String, Object>> objList = sDAO.getKeywordsFromAllStatistic(topicSerial, domainKnowhowId);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> kList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> kItem;
		ss_dkh = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			kItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			kItem.put("keyword", obj[0]);
			kItem.put("termFrequency", obj[1]);
			kItem.put("docFrequency", obj[2]);
			
			kList.add(kItem);
		}
		
		rows = kList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		ss_dkh = new HashMap<String, Object>();
		ss_dkh.put("pageSize", pageSize);
		ss_dkh.put("total", total);
		ss_dkh.put("count", count);
		ss_dkh.put("currentPageNo", currentPageNo);
		ss_dkh.put("rows", rows);
		
    	return "success";
    }
    
    
    /**
     * 取得長詞片語注入jqGrid
     * @return
     */
    @SuppressWarnings("unchecked")
	public String getPhraseById() {
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		if(topicSerial==null || topicSerial.isEmpty())
			return null;
		
		List<Map<String, Object>> objList = sDAO.getPhrase(topicSerial, sort, orderBy);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> pList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> pItem;
		result2 = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			pItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			pItem.put("phraseId", obj[0].toString());
			pItem.put("topicSerial", obj[1]);
			pItem.put("phrase", obj[2]);
			pItem.put("partOfSpeech", obj[3]);
			pItem.put("updateDate", obj[4]);
			pItem.put("editor", obj[5]);
			pItem.put("topic", obj[6]);
			pList.add(pItem);
		}
		
		rows = pList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result2 = new HashMap<String, Object>();
		result2.put("pageSize", pageSize);
		result2.put("total", total);
		result2.put("count", count);
		result2.put("currentPageNo", currentPageNo);
		result2.put("rows", rows);
		
		return "success";
    }
    
    /*
     * 長詞CRUD
     */
    public String modifyPhrase() {
    	//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
		
		SaPhrase pBean = new SaPhrase();
		Date updateDate=new Date();
		
		if(act.endsWith("add")) {
			
			UUID uuid = UUID.randomUUID();
	    	
	    	pBean.setPhraseId(uuid.toString().replaceAll("-", ""));
	    	pBean.setTopicSerial(topicSerial);
	    	pBean.setPhrase(phrase);
	    	pBean.setPartOfSpeech(partOfSpeech);
	    	pBean.setUpdateDate(updateDate);
			pBean.setEditor(loggedInUser);
			
			try {
				sDAO.modifyPhrase(pBean, "add");
    			return "success";
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "success";
			
		} else if (act.equals("modify")) {
			
			pBean.setPhraseId(phraseId);
	    	pBean.setTopicSerial(topicSerial);
	    	pBean.setPhrase(phrase);
	    	pBean.setPartOfSpeech(partOfSpeech);
	    	pBean.setUpdateDate(updateDate);
			pBean.setEditor(loggedInUser);
			
			sDAO.modifyPhrase(pBean, "modify");
			
		} else if (act.equals("delete")) {
			pBean.setPhraseId(phraseId);
			sDAO.modifyPhrase(pBean, "delete");
		}
		
		return "success";
    }
    
    /**
     * 取得removed keywords to jqgrid
     * @return
     */
    @SuppressWarnings("unchecked")
	public String getRmKeyword() {
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		if(topicSerial==null || topicSerial.isEmpty())
			return null;
		
		List<Map<String, Object>> objList = sDAO.getRmKeyword(topicSerial, sort, orderBy);
		
		int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		Iterator<?> its = objList.iterator();
        List<Map<String, Object>> pList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> pItem;
		result4 = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			pItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			pItem.put("removedId", obj[0].toString());
			pItem.put("removeItem", obj[1]);
			pItem.put("topicSerial", obj[2]);
			pItem.put("editor", obj[3]);
			pItem.put("updateDate", obj[4]);
			pItem.put("topic", obj[5]);
			pList.add(pItem);
		}
		
		rows = pList;
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result4 = new HashMap<String, Object>();
		result4.put("pageSize", pageSize);
		result4.put("total", total);
		result4.put("count", count);
		result4.put("currentPageNo", currentPageNo);
		result4.put("rows", rows);
		
		return "success";
    }
    
    /*
     * removed keyword CRUD
     */
    public String modifyRmKeyword() {
    	//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
		
		SaRemovedKeywordMap rmBean = new SaRemovedKeywordMap();
		Date updateDate=new Date();
		
		if(act.endsWith("add")) {
			
			UUID uuid = UUID.randomUUID();
	    	
	    	rmBean.setRemovedId(uuid.toString().replaceAll("-", ""));
	    	rmBean.setRemoveItem(removeItem);
	    	rmBean.setTopicSerial(topicSerial);
	    	rmBean.setUpdateDate(updateDate);
	    	rmBean.setEditor(loggedInUser);
			
			try {
				sDAO.modifyRmKeyword(rmBean, "add");
    			return "success";
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "success";
			
		} else if (act.equals("modify")) {
			
			rmBean.setRemovedId(removedId);
			rmBean.setRemoveItem(removeItem);
	    	rmBean.setTopicSerial(topicSerial);
	    	rmBean.setUpdateDate(updateDate);
	    	rmBean.setEditor(loggedInUser);
			
			
			sDAO.modifyRmKeyword(rmBean, "modify");
			
		} else if (act.equals("delete")) {
			rmBean.setRemovedId(removedId);
			sDAO.modifyRmKeyword(rmBean, "delete");
		}
		
		return "success";
    }
    
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public List<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public int getCategorySerial() {
		return categorySerial;
	}

	public void setCategorySerial(int categorySerial) {
		this.categorySerial = categorySerial;
	}

	public int getSubCategorySerial() {
		return subCategorySerial;
	}

	public void setSubCategorySerial(int subCategorySerial) {
		this.subCategorySerial = subCategorySerial;
	}

	public String getTopicSerial() {
		return topicSerial;
	}

	public void setTopicSerial(String topicSerial) {
		this.topicSerial = topicSerial;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public Map getResult() {
		return result;
	}

	public void setResult(Map result) {
		this.result = result;
	}

	public String getTypeOfArticle() {
		return typeOfArticle;
	}

	public void setTypeOfArticle(String typeOfArticle) {
		this.typeOfArticle = typeOfArticle;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getArticleIdGroup() {
		return articleIdGroup;
	}

	public void setArticleIdGroup(String articleIdGroup) {
		this.articleIdGroup = articleIdGroup;
	}

	public String getKeywordId() {
		return keywordId;
	}

	public void setKeywordId(String keywordId) {
		this.keywordId = keywordId;
	}

	public Map getResult2() {
		return result2;
	}

	public void setResult2(Map result2) {
		this.result2 = result2;
	}

	public String getAct() {
		return act;
	}

	public void setAct(String act) {
		this.act = act;
	}

	public String getDomainKnowhowName() {
		return domainKnowhowName;
	}

	public void setDomainKnowhowName(String domainKnowhowName) {
		this.domainKnowhowName = domainKnowhowName;
	}

	public String getDomainKnowhowId() {
		return domainKnowhowId;
	}

	public void setDomainKnowhowId(String domainKnowhowId) {
		this.domainKnowhowId = domainKnowhowId;
	}

	public Map getDomainKnowhowJson() {
		return domainKnowhowJson;
	}

	public void setDomainKnowhowJson(Map domainKnowhowJson) {
		this.domainKnowhowJson = domainKnowhowJson;
	}

	public String getDelKwCondition() {
		return delKwCondition;
	}

	public void setDelKwCondition(String delKwCondition) {
		this.delKwCondition = delKwCondition;
	}

	public String getPeriodId() {
		return periodId;
	}

	public void setPeriodId(String periodId) {
		this.periodId = periodId;
	}

	public String getPeriodName() {
		return periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Map getS_dkh() {
		return s_dkh;
	}

	public void setS_dkh(Map s_dkh) {
		this.s_dkh = s_dkh;
	}

	public Map getSs_dkh() {
		return ss_dkh;
	}

	public void setSs_dkh(Map ss_dkh) {
		this.ss_dkh = ss_dkh;
	}

	public String getPhraseId() {
		return phraseId;
	}

	public void setPhraseId(String phraseId) {
		this.phraseId = phraseId;
	}

	public String getPartOfSpeech() {
		return partOfSpeech;
	}

	public void setPartOfSpeech(String partOfSpeech) {
		this.partOfSpeech = partOfSpeech;
	}

	public String getPhrase() {
		return phrase;
	}

	public void setPhrase(String phrase) {
		this.phrase = phrase;
	}

	public Map getResult3() {
		return result3;
	}

	public void setResult3(Map result3) {
		this.result3 = result3;
	}

	public String getInOutId() {
		return inOutId;
	}

	public void setInOutId(String inOutId) {
		this.inOutId = inOutId;
	}

	public int getInQuantity() {
		return inQuantity;
	}

	public void setInQuantity(int inQuantity) {
		this.inQuantity = inQuantity;
	}

	public int getOutQuantity() {
		return outQuantity;
	}

	public void setOutQuantity(int outQuantity) {
		this.outQuantity = outQuantity;
	}

	public Map getResult4() {
		return result4;
	}

	public void setResult4(Map result4) {
		this.result4 = result4;
	}

	public String getRemovedId() {
		return removedId;
	}

	public void setRemovedId(String removedId) {
		this.removedId = removedId;
	}

	public String getRemoveItem() {
		return removeItem;
	}

	public void setRemoveItem(String removeItem) {
		this.removeItem = removeItem;
	}

}
