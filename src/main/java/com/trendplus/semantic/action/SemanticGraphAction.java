package com.trendplus.semantic.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.trendplus.semantic.dao.SemanticGraphDAO;
import com.trendplus.util.PageParameter;
import com.trendplus.util.Util;

public class SemanticGraphAction {
	
	private String topicSerial,topicChart;
	private String keyword, domainKnowhowId, domainKnowhowName;
	private String domainKnowhowScore, keywordScore;
	
	@Autowired
	SemanticGraphDAO sgDAO;
	
	// jqgrid
	private int total = 0; // 總頁數
	private int records = 0; // 資料總筆數
	private int pageSize = 0;
	private int currentPageNo = 0;
	private String sort;
	private String orderBy;
	private List<Map<String, Object>> rows;
	
	private Map result, wordCloudJson, inOutRecordJson, pieChartJson;
	
	
    public String initDomainKnowhowAndPeriodStatistic() {
		
		return "success";
	}
	
	/**
	 * 統計圖型
	 * 每一個時間區間(Period)與議題詞(DomainKnowhow)的關係圖
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String domainKnowhowAndPeriodStatistic() {
		if(topicSerial==null || topicSerial.isEmpty())
			return null;
		
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
        List<Map<String, Object>> objList = sgDAO.domainKnowhowAndPeriodCallCenter(topicSerial);
        rows = objList;
        int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		
		result = new HashMap<String, Object>();
		
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);
		
    	return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String wordCloud() {
		if(topicSerial==null || topicSerial.isEmpty())
			return null;
		
		wordCloudJson = new HashMap<String, Object>();
		List<Map<String, Object>> objList = sgDAO.wordCloudSummary(topicSerial);
		wordCloudJson.put("wordCloud", objList);
		
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String inOutRecord() {
		if(topicSerial==null || topicSerial.isEmpty())
			return null;
		
		inOutRecordJson = new HashMap<String, Object>();
		List<Map<String, Object>> objList = sgDAO.getInOutRecords(topicSerial);
		inOutRecordJson.put("inOutRecord", objList);
		
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String pieChart() {
		if(topicSerial==null || topicSerial.isEmpty())
			return null;
		
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		List<Map<String, Object>> objList = sgDAO.pieChartCenter(topicSerial);
		
		rows = objList;
        int count = objList.size();
		objList = Util.dataPagination(objList, page);
		
		pieChartJson = new HashMap<String, Object>();
		
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		pieChartJson = new HashMap<String, Object>();
		pieChartJson.put("pageSize", pageSize);
		pieChartJson.put("total", total);
		pieChartJson.put("count", count);
		pieChartJson.put("currentPageNo", currentPageNo);
		pieChartJson.put("rows", rows);
		
		return "success";
	}
	
	public String getTopicSerial() {
		return topicSerial;
	}
	public void setTopicSerial(String topicSerial) {
		this.topicSerial = topicSerial;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getRecords() {
		return records;
	}
	public void setRecords(int records) {
		this.records = records;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getCurrentPageNo() {
		return currentPageNo;
	}
	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public List<Map<String, Object>> getRows() {
		return rows;
	}
	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public Map getResult() {
		return result;
	}

	public void setResult(Map result) {
		this.result = result;
	}

	public String getTopicChart() {
		return topicChart;
	}

	public void setTopicChart(String topicChart) {
		this.topicChart = topicChart;
	}

	public Map getWordCloudJson() {
		return wordCloudJson;
	}

	public void setWordCloudJson(Map wordCloudJson) {
		this.wordCloudJson = wordCloudJson;
	}

	public Map getInOutRecordJson() {
		return inOutRecordJson;
	}

	public void setInOutRecordJson(Map inOutRecordJson) {
		this.inOutRecordJson = inOutRecordJson;
	}

	public Map getPieChartJson() {
		return pieChartJson;
	}

	public void setPieChartJson(Map pieChartJson) {
		this.pieChartJson = pieChartJson;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getDomainKnowhowId() {
		return domainKnowhowId;
	}

	public void setDomainKnowhowId(String domainKnowhowId) {
		this.domainKnowhowId = domainKnowhowId;
	}

	public String getDomainKnowhowName() {
		return domainKnowhowName;
	}

	public void setDomainKnowhowName(String domainKnowhowName) {
		this.domainKnowhowName = domainKnowhowName;
	}

	public String getDomainKnowhowScore() {
		return domainKnowhowScore;
	}

	public void setDomainKnowhowScore(String domainKnowhowScore) {
		this.domainKnowhowScore = domainKnowhowScore;
	}

	public String getKeywordScore() {
		return keywordScore;
	}

	public void setKeywordScore(String keywordScore) {
		this.keywordScore = keywordScore;
	}

}
