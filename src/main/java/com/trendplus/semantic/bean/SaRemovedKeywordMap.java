package com.trendplus.semantic.bean;
// Generated Nov 18, 2016 10:54:35 AM by Hibernate Tools 4.3.4.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * SaRemovedKeywordMap generated by hbm2java
 */
@Entity
@Table(name = "sa_removed_keyword_map", catalog = "brandanalysis")
public class SaRemovedKeywordMap implements java.io.Serializable {

	private String removedId;
	private String removeItem;
	private String topicSerial;
	private String editor;
	private Date updateDate;

	public SaRemovedKeywordMap() {
	}

	public SaRemovedKeywordMap(String removedId) {
		this.removedId = removedId;
	}

	public SaRemovedKeywordMap(String removedId, String removeItem, String topicSerial, String editor,
			Date updateDate) {
		this.removedId = removedId;
		this.removeItem = removeItem;
		this.topicSerial = topicSerial;
		this.editor = editor;
		this.updateDate = updateDate;
	}

	@Id

	@Column(name = "RemovedId", unique = true, nullable = false, length = 32)
	public String getRemovedId() {
		return this.removedId;
	}

	public void setRemovedId(String removedId) {
		this.removedId = removedId;
	}

	@Column(name = "RemoveItem", length = 20)
	public String getRemoveItem() {
		return this.removeItem;
	}

	public void setRemoveItem(String removeItem) {
		this.removeItem = removeItem;
	}

	@Column(name = "TopicSerial", length = 32)
	public String getTopicSerial() {
		return this.topicSerial;
	}

	public void setTopicSerial(String topicSerial) {
		this.topicSerial = topicSerial;
	}

	@Column(name = "Editor", length = 20)
	public String getEditor() {
		return this.editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdateDate", length = 19)
	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}
