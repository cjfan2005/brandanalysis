package com.trendplus.semantic.dao;

import java.util.List;
import java.util.Map;

import com.trendplus.general.bean.InOutRecord;
import com.trendplus.semantic.bean.*;


public interface SemanticDAO {
	public List<Map<String, Object>> getPeriod(String topicSerial, String sort, String orderBy);
	
	public String modifyPeriod(SaPeriod pBean, String act);
	
	public List<Map<String, Object>> getPhrase(String topicSerial, String sort, String orderBy);
	
	public String modifyPhrase(SaPhrase pBean, String act);
	
	public List<Map<String, Object>> getRmKeyword(String topicSerial, String sort, String orderBy);
	
	public String modifyRmKeyword(SaRemovedKeywordMap rmKwBean, String act);
	
	public List<Map<String, Object>> getInOutRecord(String topicSerial, String sort, String orderBy);
	
	public String modifyInOutRecord(InOutRecord pBean, String act);
	
	public String deleteArticleById(String articleId);
	
	public String insertArticle(List<SaArticle> ftaList);

	public List<Map<String, Object>> getArticle(String topicSerial, String periodId, String sort, String orderBy);
	
	public List<Map<String, Object>> getSentenceById(String articleId);
	
	public String DFTF_CallCenter (String topicSerial, String periodId);
	
	public int checkArticleList(String topicSerial, String periodId);
	
	public List<Map<String, Object>> getTfDfById(String topicSerial, String periodId, String sort, String orderBy);
	
	public List<Map<String, Object>> getCkipKeywordDetail(String keyword, String periodId, String idGroup, String sort, String orderBy);
	
	public String deleteKeywordById(String keywordId, String keyword, String topicSerial);
	
	public List<Map<String,Object>> getDomainKnowhowById(String topicSerial, String sort, String orderBy );
	
	public String modifyDomainKnowhowTopic(SaDomainKnowhowTopic dBean, String act);
	
	public List<Map<String,Object>> getDomainKnowhowByIdJson(String topicSerial);
	
	public String updateKeywordFrequencyDomainKnowhow(String keywordId, String domainKnowhowId);
	
	public String insertHowNetNTUSD(List<SaSentence> sList, String keywordId);
	
	public List<Map<String, Object>> getAllStatisticDomainKnowhowById(String topicSerial);
	
	public List<Map<String, Object>> getKeywordsFromAllStatistic(String topicSerial, String domainKnowhowId);
	
	public List<Map<String, Object>> getStatisticDomainKnowhowById(String topicSerial, String periodId);
	
	public List<Map<String, Object>> getKeywordsFromStatistic(String topicSerial, String domainKnowhowId, String periodId);
	
}
