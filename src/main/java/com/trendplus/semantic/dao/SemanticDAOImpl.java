package com.trendplus.semantic.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.jsoup.helper.StringUtil;

import com.hankcs.hanlp.HanLP;
import com.trendplus.general.bean.InOutRecord;
import com.trendplus.semantic.bean.*;
import com.trendplus.user.bean.Users;
import com.trendplus.util.SemanticUtil;
import com.trendplus.util.StringUtils;
import com.trendplus.util.Util;
import com.trendplus.util.ckipUtil;

public class SemanticDAOImpl implements SemanticDAO {
	private static final Logger logger = Logger.getLogger(SemanticDAOImpl.class);
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/**
	 * 取得topic統計區間
	 * @param topicSerial
	 * @param title
	 * @param updateDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getPeriod(String topicSerial, String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT p.PeriodId, p.TopicSerial, p.PeriodName, ";
			sql += "DATE_FORMAT(p.StartDate,'%Y-%m-%d %H:%I:%S') as StartDate,";
			sql += "DATE_FORMAT(p.EndDate,'%Y-%m-%d %H:%I:%S') as EndDate,";
			sql += "p.Editor,";
			sql += "qt.Topic ";
			sql += "FROM sa_period AS p ";
			sql += "LEFT JOIN query_topic AS qt ON qt.TopicSerial=p.TopicSerial ";
            sql += "WHERE 1=1 ";
			
            if(StringUtils.isNotBlank(topicSerial) && !topicSerial.equals("0"))
				sql += " AND p.TopicSerial ='"+topicSerial+"'";
            
			
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + orderBy +";";
			} else {
				sql += " ORDER BY ";
				sql += " p.StartDate ASC;";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	public String modifyPeriod(SaPeriod pBean, String act){
		Session session = null;
		Transaction t = null;
		
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			
			
			if(act.equals("add")) {
				session.save(pBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("modify")) {
				session.update(pBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("delete")) {
				String periodId = pBean.getPeriodId();
				/**
				 * 刪除週期為重大事項！必須刪除互相牽扯的資料流,如下
				 * in_out_record
				 * sa_article
				 * sa_sentence
				 * sa_keyword_frequency
				 * 
				 * 若有邏輯異動或新增,必須持續關注!
				 */
				
				session.createSQLQuery("Delete from sa_period WHERE PeriodId='"+periodId+"';").executeUpdate();
				session.createSQLQuery("Delete from in_out_record WHERE PeriodId='"+periodId+"';").executeUpdate();
				session.createSQLQuery("Delete from sa_article WHERE PeriodId='"+periodId+"';").executeUpdate();
				session.createSQLQuery("Delete from sa_sentence WHERE PeriodId='"+periodId+"';").executeUpdate();
				session.createSQLQuery("Delete from sa_keyword_frequency WHERE PeriodId='"+periodId+"';").executeUpdate();
				t.commit();
			}
			
			
			
		} catch (HibernateException e) {
			t.rollback();
			logger.log(Level.ERROR, "modifyPriod>" + e.getMessage());
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	} 
		}
		return "success";
	}
	
	/**
	 * 取得topic所建的片語(長詞合併)
	 * @param topicSerial
	 * @param title
	 * @param updateDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getPhrase(String topicSerial, String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT p.PhraseId, p.TopicSerial, p.Phrase, p.PartOfSpeech, ";
			sql += "DATE_FORMAT(p.UpdateDate,'%Y-%m-%d %H:%I:%S') as UpdateDate,";
			sql += "p.Editor,";
			sql += "qt.Topic ";
			sql += "FROM sa_phrase AS p ";
			sql += "LEFT JOIN query_topic AS qt ON qt.TopicSerial=p.TopicSerial ";
            sql += "WHERE 1=1 ";
			
            if(StringUtils.isNotBlank(topicSerial) && !topicSerial.equals("0"))
				sql += " AND p.TopicSerial ='"+topicSerial+"'";
            
			
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + orderBy +";";
			} else {
				sql += " ORDER BY ";
				sql += " p.UpdateDate DESC;";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	public String modifyPhrase(SaPhrase pBean, String act){
		Session session = null;
		Transaction t = null;
		
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			String strSql = "";
			
			if(act.equals("add")) {
				session.save(pBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("modify")) {
				session.update(pBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("delete")) {
				strSql = "Delete from sa_phrase WHERE PhraseId='" + pBean.getPhraseId()+ "';";
			}
			
			session.createSQLQuery(strSql).executeUpdate();
			t.commit();
			
		} catch (HibernateException e) {
			t.rollback();
			logger.log(Level.ERROR, "modifyPriod>" + e.getMessage());
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	} 
		}
		return "success";
	}
	
	/**
	 * 取得已移除關鍵字(removed keyword)
	 * @param topicSerial
	 * @param title
	 * @param updateDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getRmKeyword(String topicSerial, String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT rm.RemovedId, rm.RemoveItem, rm.TopicSerial, rm.Editor, ";
			sql += "DATE_FORMAT(rm.UpdateDate,'%Y-%m-%d %H:%I:%S') as UpdateDate, ";
			sql += "qt.topic ";
			sql += "FROM sa_removed_keyword_map AS rm ";
			sql += "LEFT JOIN query_topic AS qt ON qt.TopicSerial=rm.TopicSerial ";
            sql += "WHERE 1=1 ";
			
            if(StringUtils.isNotBlank(topicSerial) && !topicSerial.equals("0"))
				sql += " AND rm.TopicSerial ='"+topicSerial+"'";
            
			
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + orderBy +";";
			} else {
				sql += " ORDER BY ";
				sql += " rm.UpdateDate DESC;";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	public String modifyRmKeyword(SaRemovedKeywordMap rmKwBean, String act) {
		Session session = null;
		Transaction t = null;
		
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			String strSql = "";
			
			if(act.equals("add")) {
				session.save(rmKwBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("modify")) {
				session.update(rmKwBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("delete")) {
				strSql = "Delete from sa_removed_keyword_map WHERE RemovedId='" + rmKwBean.getRemovedId()+ "';";
			}
			
			session.createSQLQuery(strSql).executeUpdate();
			t.commit();
			
		} catch (HibernateException e) {
			t.rollback();
			logger.log(Level.ERROR, "modifyRmKeyword>" + e.getMessage());
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	} 
		}
		return "success";
	}
	
	/**
	 * 出貨量 Out Quantity
	 * 訂單量 InQuantity
	 * @param topicSerial
	 * @param title
	 * @param updateDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getInOutRecord(String topicSerial, String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT io.InOutId, io.PeriodId, qt.Topic, io.InQuantity, io.OutQuantity, io.Editor, ";
			sql += "DATE_FORMAT(io.UpdateDate,'%Y-%m-%d %H:%I:%S') as UpdateDate,";
			sql += "p.PeriodName, ";
			sql += "DATE_FORMAT(p.StartDate,'%Y-%m-%d') as StartDate,";
			sql += "DATE_FORMAT(p.EndDate,'%Y-%m-%d') as EndDate,";
			sql += "io.TopicSerial ";
			sql += "FROM in_out_record AS io ";
			sql += "LEFT JOIN query_topic AS qt ON qt.TopicSerial=io.TopicSerial ";
			sql += "LEFT JOIN sa_period AS p ON p.PeriodId=io.PeriodId ";
            sql += "WHERE 1=1 ";
			sql += "AND io.TopicSerial='"+topicSerial+"'";
			
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)) {
				sql += " ORDER BY ";
				sql += sort + " " + orderBy +";";
			} else {
				sql += " ORDER BY ";
				sql += " p.PeriodName ASC;";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	public String modifyInOutRecord(InOutRecord iBean, String act) {
		Session session = null;
		Transaction t = null;
		
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			String strSql = "";
			
			if(act.equals("add")) {
				session.save(iBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("modify")) {
				session.update(iBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("delete")) {
				strSql = "Delete from in_out_record WHERE InOutId='" + iBean.getInOutId()+ "';";
			}
			
			session.createSQLQuery(strSql).executeUpdate();
			t.commit();
			
		} catch (HibernateException e) {
			t.rollback();e.printStackTrace();
			logger.log(Level.ERROR, "modifyInOutRecord ->" + e.getMessage());
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	} 
		}
		return "success";
	}
	
	public String deleteArticleById(String articleId) {
       Session session = null;
	   try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			
			String strSql1= "Delete from sa_article WHERE ArticleId='"+articleId+"';";
			String strSql2 = "Delete from sa_sentence WHERE ArticleId='"+articleId+"';";
			String strSql3 = "Delete from sa_keyword_frequency WHERE ArticleIdGroup='"+articleId+"';";
			
			
			session.createSQLQuery(strSql1).executeUpdate();
			session.createSQLQuery(strSql2).executeUpdate();
			session.createSQLQuery(strSql3).executeUpdate();
			t.commit();
			
		} catch (HibernateException e) {
			logger.log(Level.ERROR, "method: deleteArticleById=>" + e.getMessage());
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	} 
		}
    	return "success";
	}
	
	public String insertArticle(List<SaArticle> aList) {
		Session session = null;
		Transaction t = null; 
		
		try {
			session = sessionFactory.openSession();
			t= session.beginTransaction();
			SaArticle sa = null;
			
			int categorySerial=0, subcategorySerial=0;
			String topicSerial = null;
			String title = null, content = null;
			String line=null, periodId = null;;
			Date insertDate = null;
			
			//insert article
			sa = new SaArticle();
			categorySerial = aList.get(0).getCategorySerial();
			subcategorySerial = aList.get(0).getSubcategorySerial();
			topicSerial = aList.get(0).getTopicSerial().toString();
			periodId = aList.get(0).getPeriodId();
			
			title = aList.get(0).getTitle();
			content = aList.get(0).getContent();
			
			BufferedReader bufReader = new BufferedReader(new StringReader(content));
			StringBuffer sb = new StringBuffer();
			try {
				while( (line=bufReader.readLine()) != null )
				{
					if(line.trim().length()>0)
					  sb.append(line.trim());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			UUID uuid = UUID.randomUUID();
	        String articleId = uuid.toString().replaceAll("-", "");
			sa.setArticleId(articleId);
			sa.setCategorySerial(categorySerial);
			sa.setSubcategorySerial(subcategorySerial);
			sa.setTopicSerial(topicSerial);
			sa.setPeriodId(periodId);
			
			sa.setTitle(title);
			sa.setContent(sb.toString());
			sa.setEditor(aList.get(0).getEditor());
			sa.setStatus(1);
			insertDate = new Date();
			sa.setInsertDate(insertDate);
			sa.setUpdateDate(null);
			session.save(sa);
			
			t.commit();
			
			/**
			 * 1. get phraseList by topicSerial.
			 * 2. get sa_removed_keyword_map by topicSerial.
			 * 3. readLine content to do Split and CKIP.
			 * 4. Write to db(sa_sentence)
			 */
			List<SaPhrase> phraseList = getSaPhraseList(topicSerial);
			List<SaRemovedKeywordMap> removedKwList = getSaRemovedKeyword(topicSerial);
			SplitAndCkip(phraseList, removedKwList, articleId, topicSerial, periodId, content, insertDate);
			
		} catch (HibernateException e) {
			t.rollback();
			logger.error(e);
		} finally {
			if(session.isOpen())
				session.close();
		}
		return "success";
	}

	/**
	 * 長詞片語合併。
	 * 是否有片語不需要在作ckip & 特殊片語保留功能
	 * @param topicSerial
	 * @return removedKwList
	 */
	@SuppressWarnings("unchecked")
	private List<SaPhrase> getSaPhraseList(String topicSerial) {
		Session session = null;
		     
		List<SaPhrase> phraseList = null;
		try {
			session = sessionFactory.openSession();
			Criteria c = session.createCriteria(SaPhrase.class);
			c.add(Restrictions.eq("topicSerial", topicSerial));
			
			phraseList = c.list();
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(session.isOpen())
				session.close();
		}
		
		return phraseList;
	}
	
	/**
	 * 過濾不必要字詞。
	 * 取得已被使用者移除不再被收錄至sa_keyword_frequency的keyword.
	 * @param topicSerial
	 * @return removedKwList
	 */
	@SuppressWarnings("unchecked")
	private List<SaRemovedKeywordMap> getSaRemovedKeyword(String topicSerial) {
		Session session = null;
		     
		List<SaRemovedKeywordMap> removedKwList = null;
		try {
			session = sessionFactory.openSession();
			Criteria c = session.createCriteria(SaRemovedKeywordMap.class);
			c.add(Restrictions.eq("topicSerial", topicSerial));
			
			removedKwList = c.list();
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(session.isOpen())
				session.close();
		}
		
		return removedKwList;
	}

	/*
	 * cki斷句斷詞。
	 * 這是從新增文章延續的方法,用來斷句、斷詞(ckip)、過濾詞性並塞入db(sa+sentence)
	 * 
	 */
	private String SplitAndCkip(List<SaPhrase> phraseList, List<SaRemovedKeywordMap> removedKwList, String articleId, String topicSerial, String periodId, String content, Date insertDate) {
		//去除標點符號並斷行
		content = content.trim().replaceAll("(?i)[^a-zA-Z0-9\u4E00-\u9FA5]", "\n"); 
		BufferedReader bufReader = new BufferedReader(new StringReader(content));
		String line=null;
		SaSentence ss = null;
		Session session = null;
	    Transaction t =null;
		try {
			session = sessionFactory.openSession();
			t= session.beginTransaction();
			
			//把大篇文章 開始斷句(line)
			while( (line=bufReader.readLine()) != null )
			{
				if(line.length()>0) {
					ss = new SaSentence();
					UUID uuid = UUID.randomUUID();
			        ss.setSentenceId(uuid.toString().replaceAll("-", ""));
					ss.setArticleId(articleId);
					ss.setTopicSerial(topicSerial);
					ss.setPeriodId(periodId);
					ss.setContent(line);
					
					//簡體轉繁體
					line = HanLP.convertToTraditionalChinese(line);
					String ckipResult = ckipUtil.ckipTransformer(phraseList, line);
					
					ss.setCkipTerm(ckipResult);
					if(!ckipResult.trim().isEmpty()) {
						//removedKwList: 不再將移除字詞再次加入
						String ckipTermFiltered = ckipUtil.ckipFilter(ckipResult, removedKwList);
						ss.setCkipTermFiltered(ckipTermFiltered);
					}
					ss.setInsertDate(insertDate);
					ss.setUpdateDate(null);
					session.save(ss);
				}
			}
			t.commit();
			
		} catch (IOException e) {
			t.rollback();
			e.printStackTrace();
		} finally {
			if(session.isOpen())
				session.close();
		}
		return "success";
	}
	
	
	/**
	 * Get All Articles
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getArticle(String topicSerial, String periodId, String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT a.ArticleId, a.PeriodId, a.Title, a.Content, a.Editor, ";
			sql += "DATE_FORMAT(a.InsertDate,'%Y-%m-%d %H:%I:%S') as InsertDate,";
			sql += "DATE_FORMAT(a.UpdateDate,'%Y-%m-%d %H:%I:%S') as UpdateDate,";
			sql += "qt.Topic, sm.StatusName, ";
			sql += "p.PeriodName,";
			sql += "DATE_FORMAT(p.StartDate,'%Y-%m-%d') as StartDate,";
			sql += "DATE_FORMAT(p.EndDate,'%Y-%m-%d') as EndDate ";
			sql += "FROM sa_article AS a ";
			sql += "LEFT JOIN query_topic AS qt ON qt.TopicSerial=a.TopicSerial ";
			sql += "LEFT JOIN sa_period AS p ON a.PeriodId=p.PeriodId ";
			sql += "LEFT JOIN sa_status_map AS sm ON a.Status=sm.StatusId ";
            sql += "WHERE 1=1 ";
            
            if(StringUtils.isNotBlank(periodId))
            	sql += "AND a.PeriodId = '"+periodId+"' ";
            
            if(StringUtils.isNotBlank(topicSerial) && !topicSerial.equals("0"))
				sql += " AND a.TopicSerial='"+topicSerial+"'";
			
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + orderBy +";";
			} else {
				sql += " ORDER BY ";
				sql += " p.startDate ASC;";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
    /*
     * 利用articleId,
     * 取得某一篇文章(article)的所有句子
     * @see com.trendplus.semantic.dao.SemanticDAO#getSentenceById(java.lang.String, java.lang.String, java.lang.String)
     */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getSentenceById(String articleId) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT s.SentenceId, s.ArticleId, qt.Topic, s.Content, s.CkipTerm, s.CkipTermFiltered, s.IntentionNTUSD, s.HowNetScore ";
			sql += "FROM sa_sentence AS s ";
			sql += "LEFT JOIN query_topic AS qt ON qt.TopicSerial=s.TopicSerial ";
            sql += "WHERE 1=1 ";
			
            if(StringUtils.isNotBlank(articleId))
				sql += " AND s.ArticleId ='"+articleId+"';";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	/*
	 * Steps
	 * 1. 欲計算時間區間內的文章鎖定。
	 * 2. 刪除時間區間內所有的資料。
	 * 3. 依時間區間、topicalSerial撈出要計算的資料。
	 * 4. 計算TF/DF並寫入資料庫。(若又出現相同關鍵字(term=tmp_kw),自動設定為相同的議題詞(DomainKnowhowId))
	 * 5. 時間區間內的文章解鎖。
	 * 6. 自動計算HowNet, NTUSD(僅計算DomainKnowhowI != null的資訊).
	 * @param 
	 */
	public String DFTF_CallCenter (String topicSerial, String periodId) {
		logger.log(Level.INFO, "Start DFTF_CallCenter, TopicSerial:" + topicSerial + ", periodId:" + periodId);
		
		//1.
		updateArticleStatus(topicSerial, periodId, 3);//'3' means locked
	    //2.
		deleteKeywordFreqById(topicSerial, periodId);
		//3.
		List<Map<String, Object>> queryList = getTfDfData(topicSerial, periodId);
		List<String> list = new ArrayList<String>();
		
		if(queryList.size() != 0) {
			
			Iterator<?> its = queryList.iterator();
			
			while (its.hasNext()) {
				
				try {
					Object[] obj = (Object[]) its.next();
					// length>1:若出現單一個字,不要存入。如：的,如,是,成..等。// 數字也刪去
					if(obj[1]!=null && obj[1].toString().trim().length()>1 && !NumberUtils.isNumber(obj[1].toString())) { 
						String[] splitTerm = obj[1].toString().split(";");
						for(int i=0; i<splitTerm.length; i++) {
							if(splitTerm[i].length()!=0)
								list.add(obj[0]+";"+splitTerm[i]); //combine: articleId;ckipTerms
						}
					}
				} catch (Exception e) {
					updateArticleStatus(topicSerial, periodId, 1);//rollback or update.
					e.printStackTrace();
				} 
				
			}
		}
		
		//4.
		//抓取所有過去已填好的關鍵字與議題詞,用來自動化
		List<SaKeywordFrequency> autoDKH_List = getDomainKnowById(topicSerial) ;
		calTFDF(topicSerial, periodId, list, autoDKH_List);//計算＆Database
		//5.
		updateArticleStatus(topicSerial, periodId, 2);//2: tf/df ok
		
		//6.
		autoCalHowNetNTUSD(topicSerial, periodId);
		logger.log(Level.INFO, "END DFTF_CallCenter, TopicSerial:" + topicSerial);
		return "success";
	}
	
	/**
	 * Check Articles in which Status.
	 * if size!=0 : return success;
	 * 若有LOCKED(=3)的項目,表示TF/DF執行中,不可再次執行。
	 */
	public int checkArticleList(String topicSerial, String periodId) {
		int counter = 0;
		Session session = null;
    	String strSql = "";
    	
    	try {
			session = sessionFactory.openSession();
			strSql = "SELECT COUNT(ArticleId) FROM sa_article WHERE Status='3' AND TopicSerial='"+topicSerial+"' ";
			
			if(StringUtils.isNotBlank(periodId))
				 strSql += "AND periodId='" + periodId + "'";
			
			strSql += ";";
				
			
			Query query = session.createSQLQuery(strSql.toString());
			counter = Integer.parseInt(query.uniqueResult().toString());
			
			logger.log(Level.INFO, "check article status, TopicSerial:" + topicSerial +", PeriodId:" + periodId);
		} catch (HibernateException e) {
			logger.log(Level.ERROR, "check article status, TopicSerial:" + topicSerial +", PeriodId:" + periodId);
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	}
		}
    	return counter;
	}
	
	private void updateArticleStatus(String topicSerial, String periodId, int statusId) {
		Session session = null;
    	String strSql = "";
    	
    	try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			strSql = "UPDATE sa_article SET Status=" + statusId + " WHERE TopicSerial='"+topicSerial+"' ";
			
			if(StringUtils.isNotBlank(periodId))
				 strSql += "AND periodId='" + periodId + "'";
			
			strSql += ";";
			
			session.createSQLQuery(strSql).executeUpdate();
			t.commit();
			logger.log(Level.INFO, "update article status, TopicSerial:" + topicSerial + ", StatusId=" + statusId);
		} catch (HibernateException e) {
			logger.log(Level.ERROR, "update article status, TopicSerial:" + topicSerial + ", StatusId=" + statusId);
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	}
		}
	}
	
	/**
     * keyword在sa_sentence出現的句子
     * 計算句子howNet與ntusd
     * @return
     */
    private String autoCalHowNetNTUSD(String topicSerial, String periodId) {
    	/*
    	 * 1.利用topicSerialSerial, periodId在sa_keyword_frequency取得所有關鍵字(keywords).
    	 * 2.關鍵字loop計算出情緒分數
    	 */
    	
    	//1.
    	List<SaKeywordFrequency> kfList = getKeyword4AutoCalSentimentScore(topicSerial, periodId);
    	
    	List<SaHowNetMap> howNetList= SemanticUtil.parseHowNetMapExcel();
    	List<SaNtusdMap> ntusdList= SemanticUtil.parseNTUSDMapExcel();
    	
    	//2. LOOP
    	for(int x=0; x<kfList.size(); x++) {
    		String keyword = kfList.get(x).getKeyword();
    		String articleIdGroup = kfList.get(x).getArticleIdGroup();
    		String keywordId = kfList.get(x).getKeywordId();
    		
    		List<Map<String, Object>> kList = getSentence4AutoCalHowNetNtusd(keyword, articleIdGroup, periodId);
        	
        	SaSentence sBean = null;
        	Date cd = new Date();
        	List<SaSentence> sList = new ArrayList<SaSentence>();
        	String content="";
    	    double value=0, intentionNTUSD=0, howNetScore=0;
        	for(int i=0; i<kList.size(); i++) {
        		
        		content = kList.get(i).get("content").toString();
        		
        		//HowNet程度詞
        		for(int j=0; j<howNetList.size(); j++) {
        			String tnHN = howNetList.get(j).getTraditionalName(), 
        				   snHN=howNetList.get(j).getSimplifiedName();
            		double valHN = howNetList.get(j).getValue();
            		
            		if(content.contains(tnHN)) {
            			value = valHN;
            			howNetScore = valHN;
            		} else if(content.contains(snHN)) {
            			value = valHN;
            			howNetScore = valHN;
            		} else {
            			value = 0;
            		}
        		}
        		
        		//NTUSD
        		for(int k=0; k<ntusdList.size(); k++) {
        			String tnSD = ntusdList.get(k).getTraditionalName(), 
         				   snSD = ntusdList.get(k).getSimplifiedName();
             		double valSD = ntusdList.get(k).getValue();
             		
        			if(content.contains(tnSD)) {
        				value = value*valSD;
        				
        				if(valSD==1) 
        					intentionNTUSD=1;
        				else
        					intentionNTUSD=-1;
        			}else if(content.contains(snSD)) {
        				value = value*valSD;
        				
        				if(valSD==1) 
        					intentionNTUSD=1;
        				else
        					intentionNTUSD=-1;
        				
        			} else {
        				value = 0;
        			}
        		}
        		
        		//arrange to Sa_sentence Bean
        		sBean = new SaSentence();
        	    sBean.setSentenceId(kList.get(i).get("sentenceId").toString());
        	    sBean.setArticleId(kList.get(i).get("articleId").toString());
        	    sBean.setTopicSerial(kList.get(i).get("topicSerial").toString());
        	    sBean.setContent(kList.get(i).get("content").toString());
        	    sBean.setCkipTerm(kList.get(i).get("ckipTerm").toString());
        	    sBean.setCkipTermFiltered(kList.get(i).get("ckipTermFiltered").toString());
        	    sBean.setIntentionNtusd(intentionNTUSD);
        	    sBean.setHowNetScore(howNetScore);
        	    if(kList.get(i).get("insertDate")!=null)
        	    	sBean.setInsertDate(Util.getDateFromStr(kList.get(i).get("insertDate").toString().replaceAll("-", "/")));
        	    else
        	    	sBean.setInsertDate(cd);
        	    
        	    sBean.setUpdateDate(cd);
        	    
        	    if(kList.get(i).get("periodId")!=null)
        	    	sBean.setPeriodId(kList.get(i).get("periodId").toString());
        	    else 
        	    	sBean.setPeriodId("");
        	    	
        	    sList.add(sBean);
        	}
        	
        	insertHowNetNTUSD(sList,keywordId);
    	}
    	return "success";
    }
    
    /**
	 * 擷取所有已加上議題的關鍵字
	 * 用來：計算情緒分數的原始資料(HOWNET, NTUSD)
	 */
	@SuppressWarnings("unchecked")
	private List<SaKeywordFrequency> getKeyword4AutoCalSentimentScore(String topicSerial, String periodId) {
		List<Map<String, Object>> kfList = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT ";
			sql += " KeywordId, ArticleIdGroup, Keyword ";

			sql += " from sa_keyword_frequency ";
            sql += " WHERE ";
			
			sql += " TopicSerial='"+topicSerial+"' AND PeriodId='"+periodId+"' AND ";
			sql += " DomainKnowhowId IS NOT NULL; "; //只計算議題詞不為空的項目
			
			Query query = session.createSQLQuery(sql.toString());
			kfList = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		
		//arrange data
		List<SaKeywordFrequency> arrangeList = new ArrayList<SaKeywordFrequency>();
		SaKeywordFrequency pBean;
		Iterator<?> its = kfList.iterator();
		while (its.hasNext()) {
			Object[] obj = (Object[]) its.next();
			pBean = new SaKeywordFrequency();
			
			pBean.setKeywordId(obj[0].toString());
			pBean.setArticleIdGroup(obj[1].toString());
			pBean.setKeyword(obj[2].toString());
			
			arrangeList.add(pBean);
		}
		
		return arrangeList;
	}
    
    /**
     * 取得欲計算情緒分數的所有句子(Sa_Sentence)
     * return all keyword sentence List
     */
    public List<Map<String,Object>> getSentence4AutoCalHowNetNtusd(String keyword, String articleIdGroup, String periodId) {
    	
    	if(articleIdGroup==null ||articleIdGroup.isEmpty() ||
    			keyword==null || keyword.isEmpty()	)
			return null;
    	
    	/**
		 * 處理ArticleId. ex: 18,21,23轉化成'18','21','23'
		 * 因為articleId為varchar(32)格式
		 */
		StringBuffer sb = new StringBuffer();
		String[] aSplit = articleIdGroup.split(",");
		for(String tmpId : aSplit) {
			sb.append("'"+tmpId+"',");
		}
		articleIdGroup = sb.toString().substring(0, sb.length()-1);
    	
    	List<Map<String, Object>> objList = getCkipKeywordDetail(keyword, periodId, articleIdGroup, null, null);
    	
    	List<Map<String, Object>> kList = new ArrayList<Map<String, Object>>();
    	Iterator<?> its = objList.iterator();
		
		Map<String, Object> kItem;
		
		while (its.hasNext()) {
			kItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			kItem.put("sentenceId", obj[0].toString());
			kItem.put("articleId", obj[1].toString());
			kItem.put("topicSerial", obj[2].toString());
			kItem.put("content", obj[3]);
			kItem.put("ckipTerm", obj[4]);
			kItem.put("ckipTermFiltered", obj[5]);
			kItem.put("intentionNTUSD", obj[6]);//domain-knowhow-name
			kItem.put("howNetScore", obj[7]);
			kItem.put("insertDate", obj[8]);
			kItem.put("updateDate", obj[9]);
			kItem.put("periodId", obj[10]);
			kList.add(kItem);
		}
    	return kList;
    }
	
	/**
	 * 依照所選時間區間，重新計算，刪除時間區間內的舊資料,
	 * 時間區間條件：insertDate  
	 * @param topicSerial
	 */
	private void deleteKeywordFreqById(String topicSerial, String periodId) {
    	Session session = null;
    	String strSql = "";
    	
    	try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			strSql = "Delete from sa_keyword_frequency WHERE TopicSerial='"+topicSerial+"' ";
			
			if(StringUtils.isNotBlank(periodId))
				 strSql += "AND periodId='" + periodId + "'";
			
			strSql += ";";
			
			session.createSQLQuery(strSql).executeUpdate();
			t.commit();
			logger.log(Level.INFO, "deleteKeywordFreqById successful, TopicSerial:" + topicSerial);
		} catch (HibernateException e) {
			logger.log(Level.ERROR, "deleteKeywordFreqById=>" + e.getMessage());
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	}
		}
	}
	
	/**
	 * 抓取TF/DF的句子資料( Data from sa_sentence )
	 * @param topicSerial
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<Map<String, Object>> getTfDfData(String topicSerial, String periodId) {
		List<Map<String, Object>> querylist = new ArrayList<Map<String, Object>>();
		
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			sql += "select ArticleId, CkipTermFiltered,";
			sql += "DATE_FORMAT(InsertDate,'%Y/%m/%d %H:%I:%S') as InsertDate ";
			sql += "from sa_sentence ";
			sql += "WHERE ";
			sql += "TopicSerial ='"+topicSerial+"' ";
			
			if(StringUtils.isNotBlank(periodId))
			   sql += "AND periodId='" + periodId + "'";
			
			sql += " Order By InsertDate ASC;";
			
				
			Query query = session.createSQLQuery(sql.toString());
			querylist = query.list();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(session.isOpen())
				session.close();
		}
		
		return querylist;
	}
	
	/**
	 * 計算出TF/DF值, 並寫入資料庫。
	 * @param topicSerial
	 * @param rawList
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private Set calTFDF(String topicSerial, String periodId, List<String> rawList, List<SaKeywordFrequency> autoDKH_List) {
		if(rawList.size()==0)
			return null;
		//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
        
		Date currentDate = new Date();
		List<String> articleIdList = new ArrayList<String>();
		List<String> TfList = new ArrayList<String>();
		
		for(int i=0; i<rawList.size(); i++) {
			String[] splitTerm = rawList.get(i).toString().split(";");//articleId;ckipTerm
			articleIdList.add(splitTerm[0]); //add id to idList.
			
			if(splitTerm[1].length()>1 && !NumberUtils.isNumber(splitTerm[1].toString()))//若出現單一個字,不要存入。如：的,如,是,成..等。
				TfList.add(splitTerm[1]); //add term to TfList.
		}
		
		//For ArticleId
		Set<String> uniqueArticleIdSet = new HashSet<String>(articleIdList);
		
		//For RawData
		Set<String> uniqueRawList = new HashSet<String>(rawList);
		
		//For CkipKeyword
		Set<String> uniqueTermSet = new HashSet<String>(TfList);
		
		//Bean and Bean List.
		SaKeywordFrequency kf = null; Session session=null; Transaction t =null;
		//List<SaKeywordFrequency> kfList = new ArrayList<SaKeywordFrequency>();
		
		
	    try {
	    	session = sessionFactory.openSession(); 
	    	t= session.beginTransaction();
	    	
	    	//Compute TF & DF
			for (String term : uniqueTermSet) {
			    //System.out.println("TF-> " + term + ": TF=" + Collections.frequency(TfList, term));
			    StringBuffer sb = new StringBuffer();
			    int dfCounter = 0;
			    for(String articleId : uniqueArticleIdSet) {
			    	String str = articleId+";"+term;
			    	if(uniqueRawList.contains(str)) {
			    		dfCounter++;
			    		sb.append(articleId+","); //組字串如:articleId_1,articleId_2,articleId_3,...
			    	}
			    }
			    //System.out.println("DF-> " + term +", DF=" + dfCounter + ", ArticleId=" + sb.toString().substring(0, sb.length()-1));
			    
			    //arrange to Bean.
			    if(dfCounter >= 1 && Collections.frequency(TfList, term)>=2) { //篩選DF>1與TF>=2數值
			    	kf = new SaKeywordFrequency();
			    	// Creating a random UUID 
			        UUID uuid = UUID.randomUUID();
			    	kf.setKeywordId(uuid.toString().replaceAll("-", ""));
				    kf.setArticleIdGroup(sb.toString().substring(0, sb.length()-1));
				    kf.setTopicSerial(topicSerial);
				    kf.setKeyword(term);
				    
				    //判斷是否需要自動帶入議題詞
				    if(autoDKH_List.size()>0) {
				    	for(int i=0; i<autoDKH_List.size(); i++) {
				    		String tmp_kw = autoDKH_List.get(i).getKeyword();
				    		//假若又出現相同關鍵字(term=tmp_kw),設定為相同的議題詞(DomainKnowhowId)
				    		if(tmp_kw.equalsIgnoreCase(term)) {
				    			
				    			kf.setDomainKnowhowId(autoDKH_List.get(i).getDomainKnowhowId());
				    		}
				    	}
				    }
				    
				    kf.setTermFrequency(Collections.frequency(TfList, term));
				    kf.setDocFrequency(dfCounter);
				    kf.setEditor(loggedInUser);
				    kf.setInsertDate(currentDate);
				    kf.setUpdateDate(currentDate);
				    kf.setPeriodId(periodId);//periodId null or has value
				    session.save(kf);
			    }
			}
			t.commit();
			
		} catch (Exception e) {
			t.rollback();
			e.printStackTrace();
		} finally {
			if(session.isOpen())
				session.close();
		}
		return uniqueArticleIdSet;
	}
	
	/**
	 * 擷取所有已加上議題的關鍵字(by TopicSerial)
	 * 用來：自動將已設過的議題詞的關鍵字加上議題詞
	 */
	private List<SaKeywordFrequency> getDomainKnowById(String topicSerial) {
		List<Map<String, Object>> kfList = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT ";
			sql += " DISTINCT(Keyword), DomainKnowhowId ";

			sql += " from sa_keyword_frequency ";
            sql += " WHERE ";
			
			sql += " TopicSerial ='"+topicSerial+"' AND ";
			sql += " DomainKnowhowId IS NOT NULL; ";
			
			Query query = session.createSQLQuery(sql.toString());
			kfList = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		
		//arrange data
		List<SaKeywordFrequency> arrangeList = new ArrayList<SaKeywordFrequency>();
		SaKeywordFrequency pBean;
		Iterator<?> its = kfList.iterator();
		while (its.hasNext()) {
			Object[] obj = (Object[]) its.next();
			pBean = new SaKeywordFrequency();
			
			pBean.setKeyword(obj[0].toString());
			pBean.setDomainKnowhowId(obj[1].toString());
			
			arrangeList.add(pBean);
		}
		
		return arrangeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getTfDfById(String topicSerial, String periodId, String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT kf.KeywordId, kf.ArticleIdGroup, kf.TopicSerial, kf.Keyword,";
			sql += "dkt.Name, kf.TermFrequency, kf.DocFrequency, kf.SentimentScore, kf.Editor,";
			sql += "DATE_FORMAT(kf.UpdateDate,'%Y-%m-%d %H:%I:%S') as UpdateDate, ";
			sql += "kf.periodId, p.PeriodName,";
			sql += "DATE_FORMAT(kf.UpdateDate,'%Y-%m-%d') as StartDate, ";
			sql += "DATE_FORMAT(p.EndDate,'%Y-%m-%d') as EndDate ";
			
			sql += "FROM sa_keyword_frequency AS kf ";
			sql += "LEFT JOIN query_topic AS qt ON qt.TopicSerial=kf.TopicSerial ";
			sql += "LEFT JOIN sa_domain_knowhow_topic AS dkt ON dkt.DomainKnowhowId=kf.DomainKnowhowId AND dkt.TopicSerial=kf.TopicSerial ";
			sql += "LEFT JOIN sa_period AS p ON p.PeriodId=kf.PeriodId AND p.TopicSerial=kf.TopicSerial ";
            sql += "WHERE ";
			sql += "kf.TopicSerial ='"+topicSerial+"' ";
			
			if(StringUtils.isNotBlank(periodId))
				   sql += "AND kf.periodId='" + periodId + "' ";
            
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + orderBy +";";
			} else {
				sql += " ORDER BY";
				sql += " kf.DocFrequency DESC, kf.TermFrequency DESC;";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getCkipKeywordDetail(String keyword, String periodId, String idGroup, String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		
		try {
			
			sql += "SELECT s.SentenceId, s.ArticleId, s.TopicSerial, s.Content, s.CkipTerm, s.CkipTermFiltered, s.IntentionNTUSD, s.HowNetScore,";
			sql += "DATE_FORMAT(s.InsertDate,'%Y-%m-%d %H:%I:%S') as InsertDate, ";
			sql += "DATE_FORMAT(s.UpdateDate,'%Y-%m-%d %H:%I:%S') as UpdateDate, ";
			sql += "s.PeriodId ";
			
			sql += "FROM sa_sentence AS s ";
            sql += "WHERE ";
			sql += "s.CkipTermFiltered like '%"+keyword+"%' AND ";
			sql += "s.ArticleId in ("+idGroup+") ";
			
			if(StringUtils.isNotBlank(periodId))
				sql += "AND periodId='" + periodId + "' ";
            
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + orderBy +";";
			} else {
				sql += "ORDER BY ";
				sql += "s.InsertDate ASC;";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		
		return list;
	}
	
	/**
	 * 刪除不必要的keyword in SaKeywordFreq.
	 */
	public String deleteKeywordById(String keywordId, String keyword, String topicSerial) {
		Session sessionKF = null, sessionRK = null, sessionS=null;
		//取得目前登入使用者角色
		String loggedInUser = ServletActionContext.getRequest().getSession().getAttribute("loggedInUser").toString();
		
		try {
			//1. sa_Keyword_frequency
			sessionKF = sessionFactory.openSession();
			Transaction tKF = sessionKF.beginTransaction();
			String strSql = "";
			strSql = "Delete from sa_keyword_frequency WHERE KeywordId='"+keywordId+"'";
			sessionKF.createSQLQuery(strSql).executeUpdate();
			
			//2. Sa_removed_keyword_map
			sessionRK = sessionFactory.openSession();
			Transaction tRK = sessionRK.beginTransaction();
			SaRemovedKeywordMap rk = new SaRemovedKeywordMap();
			rk.setRemovedId(keywordId);
			rk.setRemoveItem(keyword);
			rk.setTopicSerial(topicSerial);
			rk.setEditor(loggedInUser);
			rk.setUpdateDate(new Date());
			sessionRK.save(rk);
			
			//3. sa_sentence 把相關的ckipTermFiltered移除掉！
			sessionS = sessionFactory.openSession();
			Transaction tS = sessionS.beginTransaction();
			String sql="", empty="";
			/*
			 * 將efg開頭、結尾的字串取代成zzz
               UPDATE `table` SET `column` = REPLACE(`column`, 'efg', 'zzz') WHERE `column` LIKE '%efg%';
			 */
			sql ="UPDATE sa_sentence SET CkipTermFiltered = REPLACE(CkipTermFiltered, '" + keyword + "', '" + empty + "') WHERE CkipTermFiltered LIKE '%" + keyword + "%';";
			sessionS.createSQLQuery(sql).executeUpdate();
			
			tKF.commit();
			tRK.commit();
			tS.commit();
			
		} catch (HibernateException e) {
			logger.log(Level.ERROR, "deleteKeywordById=>" + e.getMessage());
			
		} finally {
			if (sessionKF.isOpen() || sessionRK.isOpen()) {
        		sessionKF.close(); sessionRK.close();
        	} 
		}
    	return "success";
	}
	
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getDomainKnowhowById(String topicSerial, String sort, String orderBy ) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		
		try {
			
			sql += "SELECT d.DomainKnowhowId, d.TopicSerial,qt.Topic, d.Name, d.Editor,";
			sql += "DATE_FORMAT(d.UpdateDate,'%Y-%m-%d %H:%I:%S') as UpdateDate ";
			
			sql += "FROM sa_domain_knowhow_topic AS d ";
			sql += "LEFT JOIN query_topic AS qt ON qt.TopicSerial=d.TopicSerial ";
            sql += "WHERE ";
			sql += "d.TopicSerial='"+topicSerial+"' ";
			
            
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + orderBy +";";
			} else {
				sql += "ORDER BY ";
				sql += "d.UpdateDate DESC;";
			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		
		return list;
	}
	
	public String modifyDomainKnowhowTopic(SaDomainKnowhowTopic dBean, String act){
		Session session = null;
		
		try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			
			if(act.equals("add")) {
				session.save(dBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("modify")) {
				session.update(dBean);
				t.commit();
				
				return "success";
				
			} else if(act.equals("delete")) {
				String deomainKnowhowId = dBean.getDomainKnowhowId();
				session.createSQLQuery("Delete from sa_domain_knowhow_topic WHERE DomainKnowhowId='"+deomainKnowhowId+"';").executeUpdate();
				session.createSQLQuery("Delete from sa_keyword_frequency WHERE DomainKnowhowId='"+deomainKnowhowId+"';").executeUpdate();
				t.commit();
			}
			
		} catch (HibernateException e) {
			logger.log(Level.ERROR, "modifyDomainKnowhowTopic=>" + e.getMessage());
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	} 
		}
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getDomainKnowhowByIdJson(String topicSerial) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		
		try {
			
			sql += "SELECT d.DomainKnowhowId, d.TopicSerial, d.Name ";
			
			sql += "FROM sa_domain_knowhow_topic AS d ";
            sql += "WHERE ";
			sql += "d.TopicSerial='"+topicSerial+"' OR d.TopicSerial='0' ";
			
			sql += "ORDER BY ";
			sql += "d.UpdateDate ASC;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		
		return list;
	}
	
	public String updateKeywordFrequencyDomainKnowhow(String keywordId, String domainKnowhowId) {
		Session session = null;
    	String strSql = "";
    	String cdStr = Util.convertDate2Str(new Date());
    	
    	try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			strSql = "UPDATE sa_keyword_frequency SET DomainKnowhowId='"+domainKnowhowId+"' ,UpdateDate='"+cdStr+"' WHERE KeywordId='" + keywordId + "';";
			
			session.createSQLQuery(strSql).executeUpdate();
			t.commit();
			logger.log(Level.INFO, "update sa_keyword_frequency, KeywordId:" + keywordId + ", DomainKnowhowId=" + domainKnowhowId);
		} catch (HibernateException e) {
			logger.log(Level.ERROR, "update sa_keyword_frequency, KeywordId:" + keywordId + ", DomainKnowhowId=" + domainKnowhowId);
			
		} finally {
			if (session.isOpen()) {
        		session.close();
        	}
		}
    	return "success";
	}
	
	/**
	 * 更新句子howNet, NTUSD分數 -> SaSentence.
	 * 更新關鍵詞(Keyword)SentimentScore總分 ->Sa_keyword_frequency.
	 */
	public String insertHowNetNTUSD(List<SaSentence> sList,String keywordId) {
		double sentimentScore =0; String strSql="";
		Session sessionS = null, sessionKF = null;
		Transaction tS = null;
		Transaction tKF = null;
		try {
			sessionS = sessionFactory.openSession();
			tS = sessionS.beginTransaction();
			SaSentence sBean = null;
			for(int i=0; i<sList.size(); i++) {
				//update sa_sentence
				sBean = new SaSentence();
				sBean.setSentenceId(sList.get(i).getSentenceId());
				sBean.setArticleId(sList.get(i).getArticleId());
				sBean.setTopicSerial(sList.get(i).getTopicSerial());
				sBean.setContent(sList.get(i).getContent());
				sBean.setCkipTerm(sList.get(i).getCkipTerm());
				sBean.setCkipTermFiltered(sList.get(i).getCkipTermFiltered());
				sBean.setIntentionNtusd(sList.get(i).getIntentionNtusd());
				sBean.setHowNetScore(sList.get(i).getHowNetScore());
				sBean.setInsertDate(sList.get(i).getInsertDate());
				sBean.setUpdateDate(sList.get(i).getUpdateDate());
				sBean.setPeriodId(sList.get(i).getPeriodId());
				sentimentScore += (sList.get(i).getIntentionNtusd()*sList.get(i).getHowNetScore());
				sessionS.update(sBean);
			}
			
			//update Sa_keyword_frequency
			sessionKF = sessionFactory.openSession();
			tKF = sessionKF.beginTransaction();
			
			BigDecimal sScore= new BigDecimal(sentimentScore); 
			sScore = sScore.setScale(2, BigDecimal.ROUND_HALF_UP);//小數後面2位, 四捨五入 
			strSql = "UPDATE sa_keyword_frequency SET SentimentScore='" + sScore + "' WHERE KeywordId='" + keywordId + "';";
			
			sessionKF.createSQLQuery(strSql).executeUpdate();
			
			tKF.commit();
			tS.commit();
		} catch (HibernateException e) {
			tKF.rollback();
			tS.rollback();
			logger.log(Level.ERROR, "insertHowNetNTUSD=>" + e.getMessage());
			
		} finally {
			if (sessionS.isOpen()||sessionKF.isOpen()) {
        		sessionS.close();
        		sessionKF.close();
        	} 
		}
		return "success";
	}
	
	/**
	 * 依據TopicSerial, PeriodId
	 * 抓取議題詞統計資料主表
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getStatisticDomainKnowhowById(String topicSerial, String periodId ) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		
		try {
			sql += " SELECT DISTINCT(dkt.DomainKnowhowId),dkt.Name, ";
			//計數該議題詞keyword
			sql += " (SELECT  COUNT(sub1.KeywordId)  FROM  sa_keyword_frequency AS sub1 ";
			sql += " WHERE  sub1.DomainKnowhowId=kf.DomainKnowhowId AND  sub1.TopicSerial=kf.TopicSerial AND sub1.PeriodId=kf.PeriodId ";
			sql += " ) as counter, ";
			
			//計算總分
			sql += " ROUND(";
			sql += " (SELECT SUM(sub2.SentimentScore) FROM sa_keyword_frequency as sub2 ";
			sql += " WHERE sub2.DomainKnowhowId=kf.DomainKnowhowId AND sub2.TopicSerial=kf.TopicSerial AND ";
			sql += " sub2.PeriodId=kf.PeriodId AND sub2.SentimentScore IS NOT NULL ";
			sql += " ),2) as totalScore, ";
			
			//正分總和
			sql += " ROUND(";
			sql += " (SELECT SUM(sub3.SentimentScore) FROM sa_keyword_frequency as sub3 ";
			sql += " WHERE sub3.DomainKnowhowId=kf.DomainKnowhowId AND sub3.TopicSerial=kf.TopicSerial AND ";
			sql += " sub3.PeriodId=kf.PeriodId AND sub3.SentimentScore >0 AND sub3.SentimentScore IS NOT NULL ";
			sql += " ),2) as positiveScore, ";
			
			//負分總和
			sql += " ROUND(";
			sql += " (SELECT SUM(sub4.SentimentScore) FROM sa_keyword_frequency as sub4 ";
			sql += " WHERE sub4.DomainKnowhowId=kf.DomainKnowhowId AND sub4.TopicSerial=kf.TopicSerial AND ";
			sql += " sub4.PeriodId=kf.PeriodId AND sub4.SentimentScore <0 AND sub4.SentimentScore IS NOT NULL ";
			sql += " ),2) as negativeScore, ";
			
			sql += " kf.TopicSerial, kf.PeriodId ";
			
			sql += " FROM sa_keyword_frequency AS kf ";
			sql += " LEFT JOIN sa_period AS sp ON kf.PeriodId=sp.PeriodId AND kf.TopicSerial=sp.TopicSerial ";
			sql += " LEFT JOIN sa_domain_knowhow_topic AS dkt ON kf.TopicSerial=dkt.TopicSerial AND kf.DomainKnowhowId=dkt.DomainKnowhowId ";
						
            sql += "WHERE ";
			sql += "kf.TopicSerial= '"+topicSerial+"' AND kf.PeriodId= '"+periodId+"' AND kf.DomainKnowhowId IS NOT NULL;";
			
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		
		return list;
	}
	
	/**
	 * 依據TopicSerial
	 * 抓取議題詞統計資料主表
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getAllStatisticDomainKnowhowById(String topicSerial) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		
		try {
			sql += " SELECT DISTINCT(dkt.DomainKnowhowId),dkt.Name, ";
			//計數該議題詞keyword
			sql += " (SELECT  COUNT(sub1.KeywordId)  FROM  sa_keyword_frequency AS sub1 ";
			sql += " WHERE  sub1.DomainKnowhowId=kf.DomainKnowhowId AND  sub1.TopicSerial=kf.TopicSerial ";
			sql += " ) as counter, ";
			
			//計算總分
			sql += " ROUND(";
			sql += " (SELECT SUM(sub2.SentimentScore) FROM sa_keyword_frequency as sub2 ";
			sql += " WHERE sub2.DomainKnowhowId=kf.DomainKnowhowId AND sub2.TopicSerial=kf.TopicSerial AND ";
			sql += " sub2.SentimentScore IS NOT NULL ";
			sql += " ),2) as totalScore, ";
			
			//正分總和
			sql += " ROUND(";
			sql += " (SELECT SUM(sub3.SentimentScore) FROM sa_keyword_frequency as sub3 ";
			sql += " WHERE sub3.DomainKnowhowId=kf.DomainKnowhowId AND sub3.TopicSerial=kf.TopicSerial AND ";
			sql += " sub3.SentimentScore >0 AND sub3.SentimentScore IS NOT NULL ";
			sql += " ),2) as positiveScore, ";
			
			//負分總和
			sql += " ROUND(";
			sql += " (SELECT SUM(sub4.SentimentScore) FROM sa_keyword_frequency as sub4 ";
			sql += " WHERE sub4.DomainKnowhowId=kf.DomainKnowhowId AND sub4.TopicSerial=kf.TopicSerial AND ";
			sql += " sub4.SentimentScore <0 AND sub4.SentimentScore IS NOT NULL ";
			sql += " ),2) as negativeScore, ";
			
			sql += " kf.TopicSerial ";
			
			sql += " FROM sa_keyword_frequency AS kf ";
			sql += " LEFT JOIN sa_domain_knowhow_topic AS dkt ON kf.TopicSerial=dkt.TopicSerial AND kf.DomainKnowhowId=dkt.DomainKnowhowId ";
						
            sql += "WHERE ";
			sql += "kf.TopicSerial= '"+topicSerial+"' AND kf.DomainKnowhowId IS NOT NULL;";
			
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		
		return list;
	}
	
	/**
	 * subGrid for getStatisticDomainKnowhowById() method.
	 * @param topicSerial
	 * @param domainKnowhowId
	 * @param periodId
	 * @return
	 */
	public List<Map<String, Object>> getKeywordsFromStatistic(String topicSerial, String domainKnowhowId, String periodId) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "" ;//, spam1 = "'%", spam2="%'"; 
		
		try {
			sql += " SELECT kf.Keyword, SUM(kf.TermFrequency) AS TF, SUM(kf.DocFrequency) AS DF ";
			/***
			sql += " ( SELECT COUNT(sub1.ArticleId) FROM sa_sentence AS sub1 ";
			sql += " WHERE sub1.TopicSerial=kf.TopicSerial AND sub1.PeriodId=kf.PeriodId AND ";
			sql += " sub1.IntentionNTUSD > 0 AND sub1.IntentionNTUSD IS NOT NULL AND ";
			sql += " sub1.Content like "+spam1+"kf.Keyword"+spam2 ;
			sql += " ) as positiveNTUSD, ";
			//計算總分
			sql += " ( SELECT COUNT(sub2.ArticleId) FROM sa_sentence AS sub2 ";
			sql += " WHERE sub2.TopicSerial=kf.TopicSerial AND sub2.PeriodId=kf.PeriodId AND ";
			sql += " sub2.IntentionNTUSD < 0 AND sub2.IntentionNTUSD IS NOT NULL AND ";
			sql += " sub2.Content like "+spam1+"kf.Keyword"+spam2 ;
			sql += " ) as negativeNTUSD ";
			***/
			sql += " FROM sa_keyword_frequency AS kf ";
			//sql += " LEFT JOIN sa_sentence AS ss ON ss.TopicSerial=kf.TopicSerial AND ss.PeriodId=kf.PeriodId ";
						
            sql += "WHERE ";
			sql += "kf.TopicSerial= '"+topicSerial+"' AND kf.PeriodId= '"+periodId+"' AND kf.DomainKnowhowId= '"+domainKnowhowId+"' ";
			sql += "GROUP BY kf.Keyword ";
			sql += "ORDER BY TF DESC;";
			
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}

	/**
	 * subGrid for getAllStatisticDomainKnowhowById() method.
	 * @param topicSerial
	 * @param domainKnowhowId
	 * @param periodId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getKeywordsFromAllStatistic(String topicSerial, String domainKnowhowId) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "" ;//, spam1 = "'%", spam2="%'"; 
		
		try {
			sql += " SELECT kf.Keyword, sum(kf.TermFrequency) AS TF, sum(kf.DocFrequency) AS DF ";
			sql += " FROM sa_keyword_frequency AS kf ";
						
            sql += "WHERE ";
			sql += "kf.TopicSerial= '"+topicSerial+"' AND kf.DomainKnowhowId= '"+domainKnowhowId+"' ";
			sql += "GROUP BY kf.Keyword ";
			sql += "ORDER BY TF DESC;";
			
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
}
