package com.trendplus.semantic.dao;

import java.util.List;
import java.util.Map;

import com.trendplus.general.bean.InOutRecord;

public interface SemanticGraphDAO {
	public List<Map<String,Object>> domainKnowhowAndPeriodCallCenter(String topicSerial);
	
	public List<Map<String, Object>> wordCloudSummary(String topicSerial);
	
	public List<Map<String, Object>> getInOutRecords(String topicSerial);
	
	public List<Map<String, Object>> pieChartCenter(String topicSerial);
	
}
