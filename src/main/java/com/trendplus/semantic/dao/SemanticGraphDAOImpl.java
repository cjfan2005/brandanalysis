package com.trendplus.semantic.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.trendplus.general.bean.InOutRecord;
import com.trendplus.semantic.bean.SaArticle;
import com.trendplus.semantic.bean.SaDomainKnowhowTopic;
import com.trendplus.semantic.bean.SaKeywordFrequency;
import com.trendplus.semantic.bean.SaPeriod;
import com.trendplus.util.StringUtils;

public class SemanticGraphDAOImpl implements SemanticGraphDAO {
	private static final Logger logger = Logger.getLogger(SemanticGraphDAOImpl.class);
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/**
	 * 產出圖型的call center
	 */
	public List<Map<String,Object>> domainKnowhowAndPeriodCallCenter(String topicSerial) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> item;
		
		//取得原始資料
		List<Map<String, Object>> kfList = domainKnowhowAndPeriodStatistic(topicSerial);
		
		//找出有哪些Period
		List<SaPeriod> periodList = getPeriodList(kfList);
		
		//找出有哪些Domain know how
		List<SaDomainKnowhowTopic> domainKnowhowTopicList = getDomainKnowhowTopicList(kfList);
		
		//分析
		for(int i=0; i<periodList.size(); i++) {
			
			String periodId = periodList.get(i).getPeriodId();
			String periodName = periodList.get(i).getPeriodName();
			
			for(int j=0; j<domainKnowhowTopicList.size(); j++) {
				item = new HashMap<String, Object>();
				String dkhId = domainKnowhowTopicList.get(j).getDomainKnowhowId();
				String dkhName = domainKnowhowTopicList.get(j).getName();
				
				int tf = 0, df =0;
				
				for(int k=0; k<kfList.size(); k++) {
					
					String tmpPeriodId = kfList.get(k).get("periodId").toString();
					String tmpPeriodName = kfList.get(k).get("periodName").toString();
					String tmpDkhId = kfList.get(k).get("domainKnowhowId").toString();
					String tmpDkhName =kfList.get(k).get("domainKnowhowName").toString();
					
					if(periodId.equals(tmpPeriodId) && periodName.equals(tmpPeriodName) && 
					   dkhId.equals(tmpDkhId) && dkhName.equals(tmpDkhName)) {
						//to sum up DF/TF
						int tem_tf = Integer.parseInt(kfList.get(k).get("termFrequency").toString());
						int tem_df = Integer.parseInt(kfList.get(k).get("docFrequency").toString());
						
						tf += tem_tf;
						df += tem_df;
						//System.out.println(df + ", " + tf);
					}
				}
				//add data to map
				item.put("periodName", periodName);
				item.put("domainKnowhowName", dkhName);
				item.put("TF", tf);
				item.put("DF", df);
				list.add(item);
			}
		}
		return list;
	}
	
	/**
	 * 利用topicSerial取得sa_keyword_frequency的資料
	 * 資料需再作整理
	 * 
	 */
	public List<Map<String, Object>> domainKnowhowAndPeriodStatistic(String topicSerial) {
		
		List<Map<String, Object>> kfList = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT ";
			sql += " DISTINCT (kf.Keyword), p.PeriodName, kf.PeriodId, ";
			sql += " dkt.Name, kf.DomainKnowhowId, ";
			sql += " kf.TermFrequency, kf.DocFrequency, kf.SentimentScore ";

			sql += " FROM sa_keyword_frequency AS kf ";
			sql += " LEFT JOIN sa_domain_knowhow_topic AS dkt ON kf.TopicSerial=dkt.TopicSerial AND kf.DomainKnowhowId=dkt.DomainKnowhowId ";
			sql += " LEFT JOIN sa_period AS p ON kf.TopicSerial=p.TopicSerial AND kf.PeriodId=p.PeriodId ";
            sql += " WHERE ";
			
			sql += " kf.TopicSerial ='"+topicSerial+"' AND ";
			sql += " kf.SentimentScore IS NOT NULL  AND ";
			sql += " kf.DomainKnowhowId IS NOT NULL ";
            
			sql += " ORDER BY ";
			sql += " p.PeriodName ASC;";
			
			Query query = session.createSQLQuery(sql.toString());
			kfList = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		
		//arrange data
		List<Map<String, Object>> arrangeList = new ArrayList<Map<String, Object>>();
		Map<String, Object> kfItem;
		Iterator<?> its = kfList.iterator();
		while (its.hasNext()) {
			Object[] obj = (Object[]) its.next();
			kfItem = new HashMap<String, Object>();
			
			kfItem.put("keyword", obj[0]);
			kfItem.put("periodName", obj[1]);
			kfItem.put("periodId", obj[2]);
			kfItem.put("domainKnowhowName", obj[3]);
			kfItem.put("domainKnowhowId", obj[4]);
			kfItem.put("termFrequency", obj[5]);
			kfItem.put("docFrequency", obj[6]);
			kfItem.put("sentimentScore", obj[7]);
			
			arrangeList.add(kfItem);
		}
		
		return arrangeList;
	}
	
	private List<SaPeriod> getPeriodList(List<Map<String, Object>> kfList) {
		
		List<String> periodList = new ArrayList<String>();
		
		
		for(int i=0; i<kfList.size(); i++) {
			periodList.add(kfList.get(i).get("periodName").toString()+"@"+kfList.get(i).get("periodId").toString());
		}
		
		Set<String> periodSet = new TreeSet<>(periodList);//treeSet可以做初步排序!
		
		List<SaPeriod> pList = new ArrayList<SaPeriod>();
		SaPeriod pBean = null;
		
		for (String temp : periodSet){
        	pBean = new SaPeriod();
        	String[] splitStr = temp.split("@");
        	pBean.setPeriodId(splitStr[1]);
        	pBean.setPeriodName(splitStr[0]);
        	pList.add(pBean);
        }
		
		return pList;
	}
	
    private List<SaDomainKnowhowTopic> getDomainKnowhowTopicList(List<Map<String, Object>> kfList) {
    	List<String> dkhList = new ArrayList<String>();
		
		
		for(int i=0; i<kfList.size(); i++) {
			dkhList.add(kfList.get(i).get("domainKnowhowId").toString()+"@"+kfList.get(i).get("domainKnowhowName").toString());
		}
		
		Set<String> dkhSet = new HashSet<String>(dkhList);
		
		List<SaDomainKnowhowTopic> domainKnowHowList = new ArrayList<SaDomainKnowhowTopic>();
		SaDomainKnowhowTopic dBean = null;
		
		for (String temp : dkhSet){
        	dBean = new SaDomainKnowhowTopic();
        	String[] splitStr = temp.split("@");
        	dBean.setDomainKnowhowId(splitStr[0]);
        	dBean.setName(splitStr[1]);
        	domainKnowHowList.add(dBean);
        }
		
		return domainKnowHowList;
	}
    
    /**
	 * 利用topicSerial取得sa_keyword_frequency的資料
	 * 資料需再作整理
	 * 
	 */
	public List<Map<String, Object>> wordCloudSummary(String topicSerial) {
		
		List<Map<String, Object>> kfList = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += "SELECT ";
			sql += " kf.Keyword, sum(kf.TermFrequency + kf.DocFrequency*2) "; //DF加權2倍

			sql += " FROM sa_keyword_frequency AS kf ";
            sql += " WHERE ";
			
			sql += " kf.TopicSerial ='"+topicSerial+"' AND ";
			sql += " kf.SentimentScore IS NOT NULL  AND ";
			sql += " kf.DomainKnowhowId IS NOT NULL ";
            
			sql += " GROUP BY kf.Keyword; ";
			
			Query query = session.createSQLQuery(sql.toString());
			kfList = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		
		//arrange data
		List<Map<String, Object>> arrangeList = new ArrayList<Map<String, Object>>();
		Map<String, Object> kfItem;
		Iterator<?> its = kfList.iterator();
		while (its.hasNext()) {
			Object[] obj = (Object[]) its.next();
			kfItem = new HashMap<String, Object>();
			
			kfItem.put("keyword", obj[0]);
			kfItem.put("weight", obj[1]);//權重分數
			
			arrangeList.add(kfItem);
		}
		
		return arrangeList;
	}
	
	@SuppressWarnings({ "unchecked" })
	public List<Map<String, Object>> getInOutRecords(String topicSerial) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "" ;//, spam1 = "'%", spam2="%'"; 
		
		try {
			sql += "SELECT p.PeriodName, SUM(io.InQuantity), SUM(io.OutQuantity) ";
			sql += "FROM in_out_record AS io ";
			sql += "LEFT JOIN sa_period AS p ON p.PeriodId=io.PeriodId ";
            sql += "WHERE ";
			sql += "io.TopicSerial= '"+topicSerial+"' ";
			sql += "GROUP BY io.PeriodId ";
			sql += "ORDER BY p.PeriodName;";
			
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		
		if(list.size()==0) {
			return null;
		}
		
		//arrange data
		List<Map<String, Object>> arrangeList = new ArrayList<Map<String, Object>>();
		Map<String, Object> ioItem;
		Iterator<?> its = list.iterator();
		while (its.hasNext()) {
			Object[] obj = (Object[]) its.next();
			ioItem = new HashMap<String, Object>();
			
			ioItem.put("periodName", obj[0]);
			ioItem.put("inQuantity", obj[1]);//訂單、進貨量
			ioItem.put("outQuantity", obj[2]);//銷售、出貨、下載量
			
			arrangeList.add(ioItem);
		}
		
		return arrangeList;
	}
	
	@SuppressWarnings({ "unchecked" })
	public List<Map<String, Object>> pieChartCenter(String topicSerial) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		
		try {
			sql += "SELECT ";
			sql += "DISTINCT kf.Keyword, ";
			sql += "dk.DomainKnowhowId, dk.Name, ";
			
			//sub1:議題詞的所有關鍵字總和
			sql += "( SELECT SUM(sub1.TermFrequency) ";
			sql += "FROM sa_keyword_frequency AS sub1 ";
			sql += "WHERE sub1.TopicSerial=kf.TopicSerial AND sub1.DomainKnowhowId=kf.DomainKnowhowId ";
			sql += ") AS f1,";
			
			//sub2:關鍵字個數
			sql += "( SELECT SUM(sub2.TermFrequency) ";
			sql += "FROM sa_keyword_frequency AS sub2 ";
			sql += "WHERE sub2.TopicSerial=kf.TopicSerial AND sub2.DomainKnowhowId=kf.DomainKnowhowId AND kf.Keyword=sub2.Keyword ";
			sql += ") AS f2,";
			
			//sub3:所有議題詞總和
			sql += "( SELECT SUM(sub3.TermFrequency) ";
			sql += "FROM sa_keyword_frequency AS sub3 ";
			sql += "WHERE sub3.TopicSerial=kf.TopicSerial ";
			sql += ") AS f3, ";
			
			//情緒計算總分
			sql += " ROUND(";
			sql += " (SELECT SUM(sub4.SentimentScore) FROM sa_keyword_frequency as sub4 ";
			sql += " WHERE sub4.DomainKnowhowId=kf.DomainKnowhowId AND sub4.TopicSerial=kf.TopicSerial AND ";
			sql += " sub4.SentimentScore IS NOT NULL ";
			sql += " ),2) as totalScore, ";
			
			//情緒正分總和
			sql += " ROUND(";
			sql += " (SELECT SUM(sub5.SentimentScore) FROM sa_keyword_frequency as sub5 ";
			sql += " WHERE sub5.DomainKnowhowId=kf.DomainKnowhowId AND sub5.TopicSerial=kf.TopicSerial AND ";
			sql += " sub5.SentimentScore >0 AND sub5.SentimentScore IS NOT NULL ";
			sql += " ),2) as positiveScore, ";
			
			//情緒負分總和
			sql += " ROUND(";
			sql += " (SELECT SUM(sub6.SentimentScore) FROM sa_keyword_frequency as sub6 ";
			sql += " WHERE sub6.DomainKnowhowId=kf.DomainKnowhowId AND sub6.TopicSerial=kf.TopicSerial AND ";
			sql += " sub6.SentimentScore <0 AND sub6.SentimentScore IS NOT NULL ";
			sql += " ),2) as negativeScore, ";
			
			sql += " ROUND( (SELECT SUM(sub7.SentimentScore) FROM sa_keyword_frequency as sub7 ";
			sql += " WHERE sub7.Keyword=kf.Keyword AND sub7.TopicSerial=kf.TopicSerial AND sub7.SentimentScore IS NOT NULL  ),0) as KWScore ";
			
            sql += "FROM sa_keyword_frequency AS kf ";
            
			sql += "LEFT JOIN sa_domain_knowhow_topic AS dk ON dk.TopicSerial=kf.TopicSerial AND dk.DomainKnowhowId=kf.DomainKnowhowId ";
			sql += "WHERE ";
			sql += "kf.TopicSerial= '"+topicSerial+"' ";
			sql += "AND dk.DomainKnowhowId IS NOT NULL AND dk.DomainKnowhowId<>'' ";
			sql += "ORDER BY dk.DomainKnowhowId ASC;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		
		if(list.size()==0) {
			return null;
		}
		
		//arrange data
		List<Map<String, Object>> arrangeList = new ArrayList<Map<String, Object>>();
		Map<String, Object> ioItem;
		Iterator<?> its = list.iterator();
		while (its.hasNext()) {
			Object[] obj = (Object[]) its.next();
			ioItem = new HashMap<String, Object>();
			
			ioItem.put("keyword", obj[0]);
			ioItem.put("domainKnowhowId", obj[1]);
			ioItem.put("domainKnowhowName", obj[2]);
			
			if(obj[3]!=null && obj[4]!=null && obj[5]!=null) {
				//計算pieChart內圈與外圈的百分比成份
				double totalDomainKnowhowCount = Double.parseDouble(obj[3].toString());//f1
				double totalKeywordCount = Double.parseDouble(obj[4].toString());//f2
				double totalCount = Double.parseDouble(obj[5].toString());//f3
				
				double domainKnowhowPercentage = (totalDomainKnowhowCount/totalCount)*100.0;
				double keywordPercentage = (totalKeywordCount/totalCount)*100.0;
				
				//四捨五入,取小數點後二位
				BigDecimal domainKnowhowScore= new BigDecimal(domainKnowhowPercentage); 
				domainKnowhowScore = domainKnowhowScore.setScale(2, BigDecimal.ROUND_HALF_UP);//小數後面2位, 四捨五入 
				
				//四捨五入,取小數點後二位
				BigDecimal keywordScore= new BigDecimal(keywordPercentage); 
				keywordScore = keywordScore.setScale(2, BigDecimal.ROUND_HALF_UP);//小數後面2位, 四捨五入 
				
				ioItem.put("domainKnowhowScore", domainKnowhowScore);
				ioItem.put("keywordScore", keywordScore);
			}
			
			ioItem.put("totalScore", obj[6]);
			ioItem.put("positiveScore", obj[7]);
			ioItem.put("negativeScore", obj[8]);
			ioItem.put("sentimentScore", obj[9]);
			
			arrangeList.add(ioItem);
		}
		
		return arrangeList;
	}
}
