package com.trendplus.user.action;

import java.util.ResourceBundle;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.trendplus.util.Util;


public class LogoutAction extends ActionSupport{
	ResourceBundle rbMsg = ResourceBundle.getBundle(Util.SYS_MSG_Control);
	// all struts logic from here
	public String execute() {

		ServletActionContext.getRequest().getSession().invalidate();
		addActionMessage(rbMsg.getString("LogoutSuccess"));
		return "logout";

	}
}
