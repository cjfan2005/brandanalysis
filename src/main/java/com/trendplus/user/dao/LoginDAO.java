package com.trendplus.user.dao;

import com.trendplus.user.bean.LoginInfo;

public interface LoginDAO {

	//public String validateUser(String userId, String password);
	public String validateUser(LoginInfo loginInfo);
}
