package com.trendplus.util;

import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.trendplus.semantic.bean.SaHowNetMap;
import com.trendplus.semantic.bean.SaNtusdMap;

public class SemanticUtil {
	
	/**
	 * HowNetMap.xlsx
     * 解析excel檔
     */
    public static List<SaHowNetMap> parseHowNetMapExcel() {
    	List<SaHowNetMap> howNetList = null;
		try
        {
			//String ddeFile = "D:/ba_doc/HowNetMap.xlsx"; //for windows OS
			//String[] ddeFile = "/home/kent/dev/ba_doc/HowNetMap.xlsx";//for linux, Mac.
		    String ddeFile = "/Users/ba_doc/HowNetMap.xlsx";
			
            FileInputStream file = new FileInputStream(new File(ddeFile.toString()));
 
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            //HSSFWorkbook workbook = new HSSFWorkbook(file);////for .xls file
 
            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            //HSSFSheet sheet = workbook.getSheetAt(0); //for .xls file
            
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            
            StringBuffer sb = new StringBuffer();
            
            while (rowIterator.hasNext()) 
            {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();
                String rowRes = "";
                while (cellIterator.hasNext()) 
                {
                    Cell cell = cellIterator.next();
                    String cellRes = "";
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) 
                    {
                        case Cell.CELL_TYPE_NUMERIC:
                        	if (HSSFDateUtil.isCellDateFormatted(cell)) {
                                Date date = cell.getDateCellValue();
                                cellRes = DateFormatUtils.format(date, "yyyy-MM-dd")+ "@";
                            } else {
                            	cellRes = String.valueOf(cell.getNumericCellValue())+ "@";
                            }
                            break;
                            
                        case Cell.CELL_TYPE_STRING:
                        	cellRes = cell.getStringCellValue().toString() + "@";
                            //System.out.print(cell.getStringCellValue() + "#");
                            break;
                            
                        case Cell.CELL_TYPE_FORMULA:
                        	switch(cell.getCachedFormulaResultType()) {
	                            case Cell.CELL_TYPE_NUMERIC:
	                            	cellRes = String.valueOf(cell.getNumericCellValue())+ "@";
	                                //System.out.println("Last evaluated as: " + cell.getNumericCellValue());
	                                break;
	                            case Cell.CELL_TYPE_STRING:
	                            	cellRes = cell.getStringCellValue() + "@";
	                                //System.out.println("Last evaluated as \"" + cell.getRichStringCellValue() + "\"");
	                                break;
                            }
                			break;
                    }
                    rowRes += cellRes.trim();
                }
                if(rowRes.length()!=0)
                	sb.append(rowRes+";");
            }
            file.close();
            //不為過@不为过@5.00@;
            howNetList = arrageToSaHowNetMapBean(sb);
			
		} 
        catch (Exception e) {
            e.printStackTrace();
        }
		return howNetList;
    }
    
    /**
     * HowNet -> SaHowNetMap
     * excel parse 出來的結果bean list 
     */
    public static List<SaHowNetMap> arrageToSaHowNetMapBean(StringBuffer sb) throws ParseException {
		String[] lines = sb.toString().trim().split(";");
		int i = 0;
		
		//不為過@不为过@5.00@;
        List<SaHowNetMap> list = new ArrayList<SaHowNetMap>();
		for(String row: lines){
			if(i>0) {//標題不需要放到bean
				SaHowNetMap bean = new SaHowNetMap();
			    String[] rowResSplit = row.split("@");
			    
			    String traditionalName="",simplifiedName="";
			    double value=0;
			    
			    traditionalName = rowResSplit[0];
			    simplifiedName = rowResSplit[1];
			    value = Double.parseDouble(rowResSplit[2].toString());
			    
			    bean.setTraditionalName(traditionalName);
			    bean.setSimplifiedName(simplifiedName);
			    bean.setValue(value);
			    
			    list.add(bean);
			}
			i++;
		}
		return list;
	}
    
/******************************************************************************************************/
    /**
	 * NTUSD_Map.xlsx
     * 解析excel檔
     */
    public static List<SaNtusdMap> parseNTUSDMapExcel() {
    	List<SaNtusdMap> ntusdList= null;
		try
        {
			//String ddeFile = "D:/ba_doc/HowNetMap.xlsx"; //for windows OS
			//String[] ddeFile = "/home/kent/dev/ba_doc/HowNetMap.xlsx";//for linux, Mac.
		    String ddeFile = "/Users/ba_doc/NTUSD_Map.xlsx";
			
            FileInputStream file = new FileInputStream(new File(ddeFile.toString()));
 
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            //HSSFWorkbook workbook = new HSSFWorkbook(file);////for .xls file
 
            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            //HSSFSheet sheet = workbook.getSheetAt(0); //for .xls file
            
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            
            StringBuffer sb = new StringBuffer();
            while (rowIterator.hasNext()) 
            {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();
                String rowRes = "";
                while (cellIterator.hasNext()) 
                {
                    Cell cell = cellIterator.next();
                    String cellRes = "";
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) 
                    {
                        case Cell.CELL_TYPE_NUMERIC:
                        	if (HSSFDateUtil.isCellDateFormatted(cell)) {
                                Date date = cell.getDateCellValue();
                                cellRes = DateFormatUtils.format(date, "yyyy-MM-dd")+ "@";
                            } else {
                            	cellRes = String.valueOf(cell.getNumericCellValue())+ "@";
                            }
                            break;
                            
                        case Cell.CELL_TYPE_STRING:
                        	cellRes = cell.getStringCellValue().toString() + "@";
                            //System.out.print(cell.getStringCellValue() + "#");
                            break;
                            
                        case Cell.CELL_TYPE_FORMULA:
                        	switch(cell.getCachedFormulaResultType()) {
	                            case Cell.CELL_TYPE_NUMERIC:
	                            	cellRes = String.valueOf(cell.getNumericCellValue())+ "@";
	                                //System.out.println("Last evaluated as: " + cell.getNumericCellValue());
	                                break;
	                            case Cell.CELL_TYPE_STRING:
	                            	cellRes = cell.getStringCellValue() + "@";
	                                //System.out.println("Last evaluated as \"" + cell.getRichStringCellValue() + "\"");
	                                break;
                            }
                			break;
                    }
                    rowRes += cellRes.trim();
                }
                if(rowRes.length()!=0)
                	sb.append(rowRes+";");
            }
            file.close();
            //不為過@不为过@5.00@;
            ntusdList= arrageToSaNtusdMapBean(sb);
		} 
        catch (Exception e) {
            e.printStackTrace();
        }
		return ntusdList;
    }
    
    /**
     * NTU-SD -> SaNtusdMap
     * excel parse 出來的結果bean list 
     */
    public static List<SaNtusdMap> arrageToSaNtusdMapBean(StringBuffer sb) throws ParseException {
		String[] lines = sb.toString().trim().split(";");
		int i = 0;
		
		//不為過@不为过@5.00@;
        List<SaNtusdMap> list = new ArrayList<SaNtusdMap>();
		for(String row: lines){
			if(i>0) {//標題不需要放到bean
				SaNtusdMap bean = new SaNtusdMap();
			    String[] rowResSplit = row.split("@");
			    
			    String traditionalName="",simplifiedName="";
			    double value=0;
			    
			    traditionalName = rowResSplit[0];
			    simplifiedName = rowResSplit[1];
			    value = Double.parseDouble((rowResSplit[2].toString()));
			    
			    bean.setTraditionalName(traditionalName);
			    bean.setSimplifiedName(simplifiedName);
			    bean.setValue(value);
			    
			    list.add(bean);
			}
			i++;
		}
		return list;
	}
 
}
