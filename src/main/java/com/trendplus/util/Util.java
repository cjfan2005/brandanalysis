package com.trendplus.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ParameterAware;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.jsoup.Jsoup;

import com.trendplus.user.bean.LoginInfo;
/***
 * some useful tools
 * @author Kent fan
 *
 */

public class Util  implements SessionAware, RequestAware, ParameterAware {
	public final static String SYS_MSG_Control = "com.trendplus.util.Messages";
	public static Map<String, Object> requestMap;
//	public static Map<String, Object> sessionMap;
//	public static Map<String, String[]> parameterMap;
//	public static Map<String, Object> jsonMap;
//	public static HttpServletRequest request;
//	public static HttpURLConnection conn;
	
	
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    public static final SimpleDateFormat PageDateFormat = new SimpleDateFormat("yyyy/MM/dd");
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat sybaseDateTimeFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
    private static String defaultDatePattern = "yyyy-MM-dd";
    

	public void setParameters(Map<String, String[]> arg0) {
		// TODO Auto-generated method stub	
	}
	public void setRequest(Map<String, Object> requestMap) {
		Util.requestMap = requestMap;
	}
	
//	public void setSession(Map<String, Object> sessionMap) {
//		this.sessionMap = sessionMap;
//	}
//	
//	public HttpServletRequest getRequest() {
//		return request;
//	}
//
//	public void setRequest(HttpServletRequest request) {
//		this.request = request;
//	}
    
    /**
     * 過濾所有html標籤
     * @param html
     * @return text(String)
     */
	public static String html2text(String html) {
	    return Jsoup.parse(html).text();
	}
	
    /**
	 * 依PageParameter參數，顯示list資料
	 * 
	 * @param dataList
	 * @param page
	 * @return
	 * 
	 * @author kent
	 * @since 2013/07/09
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List dataPagination(List dataList, PageParameter page){
		
		List list = new ArrayList();
		
		int currentPageNo = page.getCurrentPageNo();
		int pageSize = page.getPageSize();
		int totalPage = dataList.size() / pageSize + 1;
		
		int firstResult = (currentPageNo - 1) * pageSize;
		int lastResult = currentPageNo * pageSize;
		
		if(dataList.size() > pageSize){
			if (currentPageNo == totalPage) {
				for (int i = firstResult; i < dataList.size(); i++) {
					list.add(dataList.get(i));
				}
			}else{
				for (int i = firstResult; i < lastResult; i++) {
					list.add(dataList.get(i));
				}
			}
		}else{
			list = dataList;
		}
		return list;
	}
	
	public static LoginInfo getLoginInfo() {

		HttpServletRequest req = ServletActionContext.getRequest();

		String user = (String) req.getSession().getAttribute("loggedInUser");
		String ip = (String) req.getSession().getAttribute("loginIp");
		String password = (String) req.getSession().getAttribute("loginPwd");

		LoginInfo loginInfo = new LoginInfo();

		loginInfo.setUserId(user);
		loginInfo.setClientIpAddr(ip);
		loginInfo.setPassword(password);
		
		return loginInfo;
	}
    
    /**
     * 099/09/26 17:23:12 -> 2010-09-26 17:23:12
     * 099/09/26          -> 2010-09-26
     * kent.fan amended. 
     * Accept date or datetime format conversion
     * @param twDateStr
     * @return
     */
    public static String convertTwDate2EnDate(String twDateStr){
        try{
            if(twDateStr.length() == 18 || twDateStr.length() == 9){
                String returnStr = String.valueOf(Integer.parseInt(twDateStr.substring(0, 3)) + 1911) + "-" + twDateStr.substring(4,6)
                +"-"+twDateStr.substring(7); 
                return returnStr;
            }
        }catch(Exception e){
            e.getMessage();
        }
        return null;
    }
    
    /**
     * 將西元年轉成民國年
     * @param enDateStr
     * @return
     */
    public static String convertEnDate2TwDate(String enDateStr){
        try{
            if(enDateStr.length() == 19 || enDateStr.length() == 10){
                String twYear = String.valueOf(Integer.parseInt(enDateStr.substring(0, 4)) - 1911);
                if (twYear.length()==2){
                    twYear="0"+twYear;
                }
                String returnStr = twYear + enDateStr.substring(4); 
                return returnStr;
            } else if (enDateStr.length()==0||enDateStr==null){
                return enDateStr;
            }
        }catch(Exception e){
            e.getMessage();
        }
        return null;
    }
    
    /**
     * 2010/09/26 17:23:12 -> java.util.Date
     * @param enDateStr
     * @return
     */
    public static java.util.Date getDateFromStr(String enDateStr){
        try {
            return dateFormat.parse(enDateStr);
            
        } catch (ParseException e) {
        	e.getMessage();
        }
        return null;
    }
    
    /**
     * 2010-09-26 17:23:12 -> java.util.Date
     * @param enDateStr
     * @return
     */
    public static java.util.Date getDateOnlyFromStr(String enDateStr){
        try {
            return sdf.parse(enDateStr);
            
        } catch (ParseException e) {
        	e.getMessage();
        }
        return null;
    }
    
    /**
     * java.util.Date -> 2011/12/12 19:31:33
     * @param date
     * @return
     */
    public static String convertDate2Str(java.util.Date date){
        try{
            return dateFormat.format(date);
        }catch(Exception e){
        	e.getMessage();
        }
        return "error";
    }
    
    /**
     * 亂數密碼產生器
     * @return
     */
    public static String getRandomPwd() {
        java.util.Random r = new java.util.Random();
        int rnd = 0;
        String pwd = "";
        for (int x = 0; x < 10; x++) {
            rnd = r.nextInt(94) + 33;
            pwd = pwd + (char)rnd;
        }
        return pwd;
    }
    
    /**
     * kent add
     * 日期格式轉換 095/03/08 -> 2006/03/08
     * @param dateStr
     * @return String
     */
    public static String transADDateString(String dateStr) {
        String tmpStr = null;

        try {
          if (dateStr.trim().length() == 9) {
            tmpStr = String.valueOf(Integer.parseInt(dateStr.substring(0, 3)) +
                                    1911) + dateStr.substring(3, 9);
          }
          else {
            tmpStr = dateStr;
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        finally {
          return tmpStr;
        }
      }
    
    /**
     * kent add
     * 日期格式轉換 095/03/08 -> 2006/03/08
     * @param dateStr
     * @return Date
     */
    public static Date transADDate(String dateStr) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");     
        String tmpStr = null;
        Date applyDateTemp = new Date();
        try {
          if (dateStr.trim().length() == 9) {
            tmpStr = String.valueOf(Integer.parseInt(dateStr.substring(0, 3)) +
                                    1911) + dateStr.substring(3, 9);
            applyDateTemp = dateFormat.parse(tmpStr);
          }else {
//            tmpStr = dateStr;
              applyDateTemp = null;
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        finally {
          return applyDateTemp;
        }
      }
    

    
    public static String format(Date date, String pattern){ 
        return date == null ?  " " : new   SimpleDateFormat(pattern).format(date);  
    } 
    
    /**
     * kent add
     * 日期格式轉換 2006/03/08 -> 095/03/08
     * @param Date
     * @return dateStr
     */
    public static String transDateString(Date date) {       
        String tmpStr = null;
        System.out.println("date===="+date);
        try {
            /**
             * 從資料庫轉出來的格式是"2010-11-24"
             */
            String dateStr = format(date, defaultDatePattern);

            System.out.println("dateStr===="+dateStr);
          if (dateStr.trim().length() == 10) {
              System.out.println(">>>>"+dateStr.trim().length());
            String yyyStr = String.valueOf(Integer.parseInt(dateStr.substring(0,4)) - 1911);
            if(yyyStr.length()<3){
                yyyStr = "0" + yyyStr;
            }
            String mmStr = dateStr.substring(5,7);
            if(mmStr.length()<2){
                mmStr = "0" + mmStr;
            }
            String ddStr = dateStr.substring(8,10);
            if(ddStr.length()<2){
                ddStr = "0" + ddStr;
            }
            tmpStr = yyyStr+"/"+mmStr+"/"+ddStr;
            
          }else {
              System.out.println(">>>>"+dateStr.trim().length());
              tmpStr = "";
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        finally {
          return tmpStr;
        }
      }

    /**
     * 99/03/21 13:21:10 -> 2010-03-21 13:21:10
     */
    public static String convertRoc2WesternCalendar(String rocCalendarString){

        rocCalendarString = rocCalendarString.replaceAll("/","-");
        int yearIndex = rocCalendarString.indexOf("-");
        rocCalendarString.substring(0,yearIndex);
        int westernYear = Integer.parseInt(rocCalendarString.substring(0,yearIndex))+1911;
        String westernCalendarString = westernYear+rocCalendarString.substring(yearIndex); 
        return westernCalendarString;

    }
        
    /**
     * 取得所選當月第一天
     * @param calendar
     * @return
     */
    public static Date getFirstMonthDay(Calendar calendar) {
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
        return calendar.getTime();
    }
    
    /**
     * 取得所選當月最後一天
     * @param calendar
     * @return
     */
    public static Date getLastMonthDay(Calendar calendar) {
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTime();
    }

    public static String getEnDate(Calendar calendar) {
    	String day = "";
    	int year = calendar.get(Calendar.YEAR);
    	int month = calendar.get(Calendar.MONTH) + 1;
    	int date = calendar.get(Calendar.DATE);
    	String strYear = toDefiniteLengthString(year, 4, '0').trim();
    	String strMonth = toDefiniteLengthString(month, 2, '0').trim();
    	String strDate = toDefiniteLengthString(date, 2, '0').trim();
    	day = strYear + "/" + strMonth + "/" + strDate;
    	return day;
    }
    
    /**
     * 數字轉字串補零
     * @param l
     * @param iLength
     * @param ch
     * @return
     */
    public   static   String   toDefiniteLengthString(long   l,   int   iLength,   char   ch)   { 
        StringBuffer   sb   =   new   StringBuffer( " "); 
        String   strValue   =   String.valueOf(l); 
        if   (iLength   <   strValue.length())   { 
                return   null; 
        } 
        for   (int   i   =   0;   i   <   iLength   -   strValue.length();   i++)   { 
                sb.append(ch); 
        } 
        sb.append(l); 
        return   new   String(sb); 
    }
	@Override
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		
	}

  
}
