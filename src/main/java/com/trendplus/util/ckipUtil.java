package com.trendplus.util;

import tw.cheyingwu.ckip.CKIP;
import tw.cheyingwu.ckip.Term;
import tw.cheyingwu.ckip.WordSegmentationService;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import com.hankcs.hanlp.HanLP;
import com.trendplus.semantic.bean.SaPhrase;
import com.trendplus.semantic.bean.SaRemovedKeywordMap;

public class ckipUtil {
	
	public static String ckipTransformer(List<SaPhrase> phraseList, String rawString) {
		StringBuffer sb = new StringBuffer();
		
		//phraseList:長詞片語合併
		for(int i=0; i<phraseList.size(); i++) {
			String phrase = phraseList.get(i).getPhrase();
			if(rawString.contains(phrase)) {
				String partOfSpeech = phraseList.get(i).getPartOfSpeech();
				//System.out.println("rawString_1=" + rawString);
				
				//appen加入保留片語中
				sb.append(phrase+","+partOfSpeech+";");
				
				//不需要再把保留片語作ckip處理。
				rawString = rawString.replace(phrase, "");
				//System.out.println("rawString_2=" + rawString);
			}
		}
		
		//ckip
		WordSegmentationService c; //宣告一個class變數c
        ArrayList<String> inputList = new ArrayList<String>(); //宣告動態陣列 存切詞的name
        ArrayList<String> TagList = new ArrayList<String>();   //宣告動態陣列 存切詞的詞性
        
        c = new CKIP( "140.109.19.104" , 1501, "trendplus_01", "gtbc2@16gogogo"); //輸入申請的IP、port、帳號、密碼
        
        c.setRawText(rawString);
        c.send(); //傳送至中研院斷詞系統服務使用
        
        //ckip蒐集字詞
        for (Term t : c.getTerm()) {
        	// length>1:若出現單一個字,不要存入。如：的,如,是,成..等。
        	// 數字也刪去
        	if( t.getTerm().toString().trim().length()>1 && !NumberUtils.isNumber(t.getTerm().toString())) {
        		inputList.add(t.getTerm()); // t.getTerm()會讀到斷詞的String，將其存到inputList陣列
                TagList.add(t.getTag());    // t.getTag() 會讀到斷詞的詞性，將其存到TagList陣列
                sb.append(t.getTerm()+","+t.getTag()+";");
        	}
        }
        
        
        
        String result = sb.toString();
        
        if(result.length()>3) {
            result = result.substring(0, result.length()-1);
        }
		return result;
	}
	
	public static String ckipFilter(String ckipTerm,  List<SaRemovedKeywordMap> removedKwList) {
		//保留的詞性
		String[] partOfSpeech = { "A", "ADV", "N", "DET","Vi", "Vt" };
		String[] ckipTermSplit = ckipTerm.split(";");
		StringBuffer sb = new StringBuffer();
		StringBuffer sbRK = new StringBuffer();
		String removedKeywords = "";
		
		if(removedKwList.size()>0) {
			for(int i=0; i<removedKwList.size(); i++) {
				sbRK.append(removedKwList.get(i).getRemoveItem());
			}
			removedKeywords = sbRK.toString();
			//System.out.println("removedKeywords=" + removedKeywords);
		}
		
		//System.out.println("ckipTerm="+ckipTerm);
		for(int i=0; i<ckipTermSplit.length; i++) {
			String[] splitStr = ckipTermSplit[i].split(",");
			String term=null, tag=null;
			
			if(splitStr.length>0) {
				term = splitStr[0];
				tag = splitStr[1];
				
//			for(int j=0; j<partOfSpeech.length; j++) {
//				
//				if(tag.equalsIgnoreCase(partOfSpeech[j])) {
//		        	//System.out.println("removedKeywords.length()="+ removedKeywords.length() +", Term=" + term +", tag="+ tag);
//		        	sb.append(term+";");
//		        	}
//			}
		        
				for(int j=0; j<partOfSpeech.length; j++) {
					if(removedKeywords.length()>0) {
						if(tag.equalsIgnoreCase(partOfSpeech[j]) && !removedKeywords.contains(term)) {
							//System.out.println("removedKeywords.length()="+ removedKeywords.length() +"Term=" + term +", tag="+ tag);
			        		sb.append(term+";");
			        		break;
			        	}
					} else {
						if(tag.equalsIgnoreCase(partOfSpeech[j])) {
			        		//System.out.println("removedKeywords.length()="+ removedKeywords.length() +"Term=" + term +", tag="+ tag);
			        		sb.append(term+";");
			        		break;
			        	}
					}
		        }
			}
		}
		
		String result = sb.toString();
		if(!result.trim().isEmpty())
			result = result.substring(0, result.length()-1);
		return result;
	}

	public static void main(String[] args) {
		
        WordSegmentationService c; //宣告一個class變數c
        ArrayList<String> inputList = new ArrayList<String>(); //宣告動態陣列 存切詞的name
        ArrayList<String> TagList = new ArrayList<String>();   //宣告動態陣列 存切詞的詞性
        String s = "早安，台新金控12月3日將召開股東臨時會進行董監改選";
        
        System.out.println("********** 使用中研院斷詞伺服器 *********");
        
        c = new CKIP( "140.109.19.104" , 1501, "trendplus", "gtbc2@16gogogo"); //輸入申請的IP、port、帳號、密碼
        
        c.setRawText(s);
        c.send(); //傳送至中研院斷詞系統服務使用
        
        
        for (Term t : c.getTerm()) {
          
            inputList.add(t.getTerm()); // t.getTerm()會讀到斷詞的String，將其存到inputList陣列
            TagList.add(t.getTag());    // t.getTag() 會讀到斷詞的詞性，將其存到TagList陣列
            System.out.println("Term:"+ t.getTerm() +", Tag:"+ t.getTag());
        }
        
        //將資料output成檔案
        try {
            FileWriter fr1 = new FileWriter("/Users/cjfan/Desktop/output.txt");
            BufferedWriter bw = new BufferedWriter(fr1);
            for(int i=0;i<inputList.size();i++)
            {
                bw.write(inputList.get(i));
                bw.write("\t"+TagList.get(i));
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
