<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta name="viewport" content="width=device-width">

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.ico" type="${pageContext.request.contextPath}/image/x-icon" />
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
        
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.metadata.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/hint-textbox.js"></script>
<style type="text/css">
	.errorMsg {
			background-color:#FFCCCC;
			border:1px solid #CC0000;
			width:500px;
			margin-bottom:8px;
	}
	.errorMsg li{ 
	  list-style: none; 
	}
	.successMsg {
			background-color:#DDFFDD;
			border:1px solid #009900;
			width:500px;
	}
	.successMsg li{ 
			list-style: none; 
	}
	.queryConditionMsg {
			background-color:#FFECF5;
			border:1px solid #cccc7d;
			width:350px;
	}
	.queryConditionMsg li{ 
			list-style: none; 
	}
</style>


