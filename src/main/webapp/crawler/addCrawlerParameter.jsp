<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>

<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Brand Analysis- Manage Crawler Parameters </title>
<style>
html,body{ height:100%; margin:0; padding:0} 
.mask{height:100%; width:100%; position:fixed; _position:absolute; top:0; z-index:1000; } 
.opacity{ opacity:0.3; filter: alpha(opacity=30); background-color:#000; }
.box {
        display: table-cell;
        vertical-align:middle;
        text-align:center;
        *display: block;
        *font-size: 175px;
        *font-family:Arial;
        
        width:500px;
        height:500px;
        border: 1px solid #eee;
}
.box label {
        vertical-align:middle;
        font-size:64px;
        color: royalblue;
}
</style>

<script type="text/javascript">
$(function(){
	//init();
	getCategory() ;
});

function getCategory() {
	var CategoryNameObj = document.getElementById("categoryName");
	CategoryNameObj.style.display = "none";
	var tarObj = document.getElementById("categoryName");
	var selectObj = document.createElement("SELECT");
	selectObj.id = "cat-Select";
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicCategoryAction.do",
			dataType: "json",
			success: function(data) {
				var sValue = CategoryNameObj.value;
				var index = 0;
				
				for(var i=0; i<data.topicCategory.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					if(sValue === data.topicCategory[i].topic) {
						index = i;
					}
					optionObj.innerHTML = data.topicCategory[i].name;
					optionObj.value = data.topicCategory[i].categorySerial +"@"+ data.topicCategory[i].secondName;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#categorySerial").val( arr[0] );
		$("#lblCategorySerial").html( arr[0] );
		
		$("#categoryName").val( this.options[this.selectedIndex].text);
		
		//call to getSubCategory();
		getSubCategory(arr[0]);
	};
}

function getSubCategory(categorySerial) {
	var selectObj = "";
	var SubCategoryNameObj = document.getElementById("subCategoryName");
	SubCategoryNameObj.style.display = "none";
	var tarObj = document.getElementById("subCategoryName");
	
	if(document.getElementById("subCat-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "subCat-Select";
	} else {
		//移除原本的options
		$('#subCat-Select').find('option').remove().end();
		$('#topic-Select').find('option').remove().end();
		document.getElementById("subCategorySerial").value = "";
		document.getElementById("topicSerial").value = "";
		selectObj = document.getElementById("subCat-Select");
	}
	
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetSubcategory2Action.do?categorySerial="+ categorySerial,
			dataType: "json",
			success: function(data) {
				var sValue = SubCategoryNameObj.value;
				var index = 0;
				var optionObj = "";
				
				for(var i=0; i<data.subCategory.length; i++) {
					optionObj = document.createElement("OPTION");
					
					if(sValue === data.subCategory[i].topic) {
						index = i;
					}
					optionObj.innerHTML = data.subCategory[i].subName;
					optionObj.value = data.subCategory[i].subCategorySerial +"@"+ data.subCategory[i].categorySerial;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#subCategorySerial").val( arr[0] );
		$("#lblSubCategorySerial").html( arr[0] );
		
		$("#subName").val( this.options[this.selectedIndex].text);
		getTopic() ;
	};
	
	insertAfter(selectObj, tarObj);
}


function getTopic() {
	var catSerial = document.getElementById("categorySerial").value;
	var subCatSerial = document.getElementById("subCategorySerial").value;
	var selectObj = "";
	
	var TopicObj = document.getElementById("topic");
	TopicObj.style.display = "none";
	var tarObj = document.getElementById("topic");
	selectObj = document.createElement("SELECT");
	
	if(document.getElementById("topic-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "topic-Select";
	} else {
		//移除原本的options
		$('#topic-Select').find('option').remove().end();
		document.getElementById("topicSerial").value = "";
		selectObj = document.getElementById("topic-Select");
	}
	
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicJSONAction.do?categorySerial="+catSerial+"&subCategorySerial="+subCatSerial,
			dataType: "json",
			success: function(data) {
				var sValue = TopicObj.value;
				var index = 0;
				
				for(var i=0; i<data.topicJSONStr.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					if(sValue === data.topicJSONStr[i].topic) {
						index = i;
					}
					optionObj.innerHTML = data.topicJSONStr[i].topic;
					optionObj.value = data.topicJSONStr[i].topicSerial +"@"+ data.topicJSONStr[i].updateDate;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#topicSerial").val( arr[0] );
		$("#lblTopicSerial").html( arr[0] );
		
		$("#topic").val( this.options[this.selectedIndex].text);
	};
}

function insertAfter(newEl, targetEl) {
    var parentEl = targetEl.parentNode;
    if(parentEl.lastChild == targetEl) {
        parentEl.appendChild(newEl);
    }else {
        parentEl.insertBefore(newEl,targetEl.nextSibling);
    }            
}

function submitForm() {
    if($("#form1").valid()){
    	$( "#dialog" ).dialog( "open" );
    }else {
    	$( "#warn" ).dialog( "open" );
    }
}

function submitFunc() {
	$('#form1').attr('action',
			'${pageContext.request.contextPath}/crawler/AddNewQueryConditionAction.do');
	$('#form1').submit();
}

$(function(){
	//須與form表單ID名稱相同
	$("#form1").validate({});
	$( "#warn" ).dialog({
		"autoOpen": false,
		buttons: [
		          {
		            text: "OK",
		            click: function() {
		            	$( this ).dialog( "close" );
		            }
		          }]
	});
	
	$( "#dialog" ).dialog({
		"autoOpen": false,
		width: 250,
		height:200,
		buttons: [
		          {
		            text: "OK",
		            click: function() {
		            	submitFunc();
		            }
		          },
		          {
			        text: "Cancel",
			        click: function() {
			           $( this ).dialog( "close" );
			        }
			      }
		        ]	
	
	});
	
});
</script>

</head>
<body>
<div class="mask opacity" id="allCover" style="display:none;">
	<div class="box" id="mDiv">
       <label>Please wait!!</label>
	</div>
</div>

<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/BrandAnalysis_logo.png" alt="BrandAnalysis" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： Crawler Management :
Add URL </div>
<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>


<form action="" method="post" name="form1" id="form1">

<table width="400" >
    <tr>
    	<th>Category：</th>
    	<td colspan="3">
			<s:hidden theme="simple" name="categoryName" id="categoryName" value="%{categoryName}" ReadOnly="true"/>
			<s:hidden theme="simple" name="categorySerial" id="categorySerial" value="%{categorySerial}" ReadOnly="true" cssClass="required"/>
		</td>
	</tr>
	<tr>	
		<th>SubCat.：</th>
    	<td colspan="3">
            
			<s:hidden theme="simple" name="subCategoryName" id="subCategoryName" value="%{subCategoryName}" ReadOnly="true"/>
			<s:hidden theme="simple" name="subCategorySerial" id="subCategorySerial" value="%{subCategorySerial}" ReadOnly="true" cssClass="required"/>
		</td>
	</tr>
	<tr>
		<th>Topic：</th>
		<td colspan="3">
			<s:hidden theme="simple" name="topic" id="topic" value="%{topic}" ReadOnly="true" />
			<s:hidden theme="simple" name="topicSerial" id="topicSerial" value="%{topicSerial}" ReadOnly="true" cssClass="required"/>
		</td>
	</tr>
	<tr>
		<th>URL Name：</th>
		<td colspan="3"><s:textfield theme="simple" name="urlName" id="urlName" type="text" size="60" maxlength="100" value="%{urlName}" cssClass="required form-control" /></td>
	</tr>
	<tr><th>URL：</th><td colspan="3"><s:textfield theme="simple" name="url" id="url" type="text" size="120" maxlength="255" value="%{url}" cssClass="required" /></td></tr>
	<tr>
		<th>Enable：</th>
		<td colspan="3">
		   <input type="radio" id="enable" name="enable" value="Y" checked> 啟用<p>
           <input type="radio" id="enable" name="enable" value="N"> 不啟用
		</td>
	</tr>
	<tr>
		<th>Keyword_0：</th>
		<td>
		   <s:textfield theme="simple" name="keyword_0" id="keyword_0" type="text" size="28" maxlength="50" value="%{keyword_0}" cssClass="required"/>
		</td>
		<th>Keyword_1：</th>
		<td>
		   <s:textfield theme="simple" name="keyword_1" id="keyword_1" type="text" size="28" maxlength="50" value="%{keyword_1}"/>
		</td>
	</tr>
	<tr>
		<th>Keyword_2：</th>
		<td>
		   <s:textfield theme="simple" name="keyword_2" id="keyword_2" type="text" size="28" maxlength="50" value="%{keyword_2}"/>
		</td>
		<th>Keyword_3：</th>
		<td>
		   <s:textfield theme="simple" name="keyword_3" id="keyword_3" type="text" size="28" maxlength="50" value="%{keyword_3}"/>
		</td>
	</tr>
	<tr>
		<th>Keyword_4：</th>
		<td>
		   <s:textfield theme="simple" name="keyword_4" id="keyword_4" type="text" size="28" maxlength="50" value="%{keyword_4}"/>
		</td>
		<th>Keyword_5：</th>
		<td>
		   <s:textfield theme="simple" name="keyword_5" id="keyword_5" type="text" size="28" maxlength="50" value="%{keyword_5}"/>
		</td>
	</tr>
	<tr>
	   <td colspan="4">
		   <s:submit onclick="submitFunc()" cssClass="comfirm" value="ADD" theme="simple"/>
	   </td>
	</tr>
</table>
</form>

<div id="dialog"title="Confirm">
	<DIV style="width:200px;height:70px;text-align:center;line-height:100px;">Save or not?</DIV>
</div>
<div id="warn" title="Warn">
	<DIV style="width:260px;height:70px;text-align:center;line-height:100px;">Please check every column !</DIV>
</div>

</div>
</div>
</div>
</body>
</html>
 