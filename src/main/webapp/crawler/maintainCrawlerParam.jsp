<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>

<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Brand Analysis- Manage Crawler Parameters </title>
<style>
html,body{ height:100%; margin:0; padding:0} 
.mask{height:100%; width:100%; position:fixed; _position:absolute; top:0; z-index:1000; } 
.opacity{ opacity:0.3; filter: alpha(opacity=30); background-color:#000; }
.box {
        display: table-cell;
        vertical-align:middle;
        text-align:center;
        *display: block;
        *font-size: 175px;
        *font-family:Arial;
        
        width:500px;
        height:500px;
        border: 1px solid #eee;
}
.box label {
        vertical-align:middle;
        font-size:64px;
        color: royalblue;
}
</style>

<script type="text/javascript">
$(function(){
	//init();
	getCategory() ;
});

function getCategory() {
	var CategoryNameObj = document.getElementById("categoryName");
	CategoryNameObj.style.display = "none";
	var tarObj = document.getElementById("categoryName");
	var selectObj = document.createElement("SELECT");
	selectObj.id = "cat-Select";
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicCategoryAction.do",
			dataType: "json",
			success: function(data) {
				var sValue = CategoryNameObj.value;
				var index = 0;
				
				for(var i=0; i<data.topicCategory.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					if(sValue === data.topicCategory[i].topic) {
						index = i;
					}
					optionObj.innerHTML = data.topicCategory[i].name;
					optionObj.value = data.topicCategory[i].categorySerial +"@"+ data.topicCategory[i].secondName;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#categorySerial").val( arr[0] );
		$("#lblCategorySerial").html( arr[0] );
		
		$("#categoryName").val( this.options[this.selectedIndex].text);
		
		//call to getSubCategory();
		getSubCategory(arr[0]);
	};
}

function getSubCategory(categorySerial) {
	var selectObj = "";
	var SubCategoryNameObj = document.getElementById("subCategoryName");
	SubCategoryNameObj.style.display = "none";
	var tarObj = document.getElementById("subCategoryName");
	
	if(document.getElementById("subCat-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "subCat-Select";
	} else {
		//移除原本的options
		$('#subCat-Select').find('option').remove().end();
		$('#topic-Select').find('option').remove().end();
		document.getElementById("subCategorySerial").value = "";
		document.getElementById("topicSerial").value = "";
		selectObj = document.getElementById("subCat-Select");
	}
	
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetSubcategory2Action.do?categorySerial="+ categorySerial,
			dataType: "json",
			success: function(data) {
				var sValue = SubCategoryNameObj.value;
				var index = 0;
				var optionObj = "";
				
				for(var i=0; i<data.subCategory.length; i++) {
					optionObj = document.createElement("OPTION");
					
					if(sValue === data.subCategory[i].topic) {
						index = i;
					}
					optionObj.innerHTML = data.subCategory[i].subName;
					optionObj.value = data.subCategory[i].subCategorySerial +"@"+ data.subCategory[i].categorySerial;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#subCategorySerial").val( arr[0] );
		$("#lblSubCategorySerial").html( arr[0] );
		
		$("#subName").val( this.options[this.selectedIndex].text);
		getTopic() ;
	};
	
	insertAfter(selectObj, tarObj);
}


function getTopic() {
	var catSerial = document.getElementById("categorySerial").value;
	var subCatSerial = document.getElementById("subCategorySerial").value;
	var selectObj = "";
	
	var TopicObj = document.getElementById("topic");
	TopicObj.style.display = "none";
	var tarObj = document.getElementById("topic");
	selectObj = document.createElement("SELECT");
	
	if(document.getElementById("topic-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "topic-Select";
	} else {
		//移除原本的options
		$('#topic-Select').find('option').remove().end();
		document.getElementById("topicSerial").value = "";
		selectObj = document.getElementById("topic-Select");
	}
	
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicJSONAction.do?categorySerial="+catSerial+"&subCategorySerial="+subCatSerial,
			dataType: "json",
			success: function(data) {
				var sValue = TopicObj.value;
				var index = 0;
				
				for(var i=0; i<data.topicJSONStr.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					if(sValue === data.topicJSONStr[i].topic) {
						index = i;
					}
					optionObj.innerHTML = data.topicJSONStr[i].topic;
					optionObj.value = data.topicJSONStr[i].topicSerial +"@"+ data.topicJSONStr[i].updateDate;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#topicSerial").val( arr[0] );
		$("#lblTopicSerial").html( arr[0] );
		
		$("#topic").val( this.options[this.selectedIndex].text);
	};
}

function insertAfter(newEl, targetEl) {
    var parentEl = targetEl.parentNode;
    if(parentEl.lastChild == targetEl) {
        parentEl.appendChild(newEl);
    }else {
        parentEl.insertBefore(newEl,targetEl.nextSibling);
    }            
}

//jqGrid Site
	$(document).ready(function() {
		
		$("#grid_site").jqGrid({
			
			url:encodeURI('${pageContext.request.contextPath}/crawler/GetSiteJSONAction.do?topicSerial=${topicSerial}'),
			
			datatype: "JSON",
			height: "150", 
			width:"900",
			colNames:['Topic', 'Name', 'URL', 'Enable', 'Editor', 'UpdateDate','Site Serial', 'Topic Serial'],
			         
	        colModel:[
					  {name:'topic', index:'topic', width:180, frozen: true,
						  editable: true,
						  edittype:"text", editrules:{required:true, readonly:true},
					      editoptions:{defaultValue:document.getElementById("topic").value, size:50, maxlength:50}
					  },
					  {name:'urlName', index:'urlName', width:200, frozen: true,
						  editable: true, editoptions:{size:50, maxlength:50},
	                	  edittype:"text", editrules:{required:true}
					  },
					  {name:'url', index:'url', width:500,
						  editable: true, editoptions:{size:50, maxlength: 200},
	                	  edittype:"text", editrules:{required:true}
					  },
					  {name:'enable', index:'enable', width:100, align:'center',
						  editable: true, editrules:{required: true},
	                	  edittype:'select', 
	                	  editoptions:{value:{Y:'Yes',N:'NO'}},
					  },
					  {name:'editor', index:'editor', width:100, align:'center'},
					  {name:'updateDate', index:'updateDate', width:150, align:'center'},
					  {name:'siteSerial', index:'siteSerial', width:100,
						  editable: true, 
	                	  edittype:"text", editrules:{required:true},
	                	  editoptions:{defaultValue:'新增時,不必填寫', size:10, maxlength:10}
					  },
					  {name:'topicSerial', index:'topicSerial', width:10,
						  editable: true, 
	                	  edittype:"text", 
	                	  editrules:{required:true, hidden:false},
	                	  editoptions:{defaultValue:document.getElementById("topicSerial").value ,size:10, maxlength: 10}
					  },
	        ],
	        shrinkToFit:false,
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: true,
	        rowNum: 5, 
	        rowList: [5], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager_site",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "Site's URL Management",
	        toppager: true,
	        loadComplete: function(data){
	        	$("#ulList1").css("zIndex", 1000);
	        	$("#ulList2").css("zIndex", 1000);
	        	$("#ulList3").css("zIndex", 1000);
	        	$("#ulList4").css("zIndex", 1000);
	        }
	    });
		
		jQuery("#grid_site").jqGrid('navGrid','#gridPager_site', 
			{edit:true, add:true, del:true, search:false, refresh:false, cloneToTop:true,
			     edittext: "Modify", edittitle: "Modify", 
			     addtext: "Add", addtitle: "Add", 
			     deltext: "Delete", deltitle: "Delete",
			     searchtext: "Search", searchtitle: "Search", 
			     refreshtext: "Refresh", refreshtitle: "Refresh"
			},
			
			// Modify
			{ url: '${pageContext.request.contextPath}/crawler/ModifyQuerySiteAction.do?act=modify',
			  height:'80%',
			  width:'100%',
			  //viewPagerButtons: false, //remove previous/next arrow
              //reloadAfterSubmit: true,
              onclickSubmit : function(params, posdata) { 
               	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
               	  $("#allCover").css("display","block");
              },
              afterSubmit : function(response, postdata) {
            	  $("#allCover").css("display","none");
              	  return true;
              },
              closeAfterEdit: true,
              recreateForm: true,
              beforeInitData: function () {
            	  $("#grid_site").jqGrid('setColProp','topic',{editoptions:{readonly:true}});
            	  $("#grid_site").jqGrid('setColProp','siteSerial',{editoptions:{readonly:true}});
            	  $("#grid_site").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true}});
              },
			}, 
			
			// add options
			{ url: '${pageContext.request.contextPath}/crawler/ModifyQuerySiteAction.do?act=add',
			  height:'80%',
			  width:'100%',
		      reloadAfterSubmit:true,
		      closeAfterAdd: true,
		      recreateForm: true,
              beforeInitData: function () {
            	  $("#grid_site").jqGrid('setColProp','topic',{editoptions:{readonly:true}});
            	  $("#grid_site").jqGrid('setColProp','siteSerial',{editoptions:{readonly:true}});
            	  $("#grid_site").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true}});
              }
			},  
			
			// del options
			{ url:'${pageContext.request.contextPath}/crawler/ModifyQuerySiteAction.do?act=delete',
			  reloadAfterSubmit: true,
			  closeAfterDel: true,
			  delData: {
				  siteSerial: function() {
				      var selectedId = $("#grid_site").jqGrid('getGridParam', 'selrow');
                      var value = $("#grid_site").jqGrid('getCell', selectedId, 'siteSerial');
                      //alert(value);
                      return value;
			      }
			  },
			  
			},
			
			// search options
			{ Find: "搜尋",  
			  closeAfterSearch: true  
			},
			//refresh optioins
			{ url: '${pageContext.request.contextPath}/user/JqGridFindAllStaff.do?UserType=STAFF'
			} 
		)
		jQuery("#grid_site").jqGrid('setFrozenColumns');
		
	});


//jqGrid Keyword
	$(document).ready(function() {
		
		$("#grid_keyword").jqGrid({
			
			url:encodeURI('${pageContext.request.contextPath}/crawler/GetKeywordJSONAction.do?topicSerial=${topicSerial}'),
			
			datatype: "JSON",
			height: "300", 
			width:"900",
			colNames:['Topic','Keyword', 'Enable', 'Editor', 'updateDate','KeywordSerial', 'Topic Serial'],
			         
	        colModel:[
					  {name:'topic', index:'topic', width:180, frozen: true,
						  editable: true,
						  edittype:"text", editrules:{required:true},
						  editoptions:{defaultValue:document.getElementById("topic").value, size:50, maxlength:50}
					  },
					  {name:'keyword', index:'keyword', width:350,frozen: true,
						  editable: true, editoptions:{size:50, maxlength:50},
	                	  edittype:"text", editrules:{required:true}
					  },
					  {name:'enable', index:'enable', width:100, align:'center',
						  editable: true,
	                	  edittype:"text", editrules:{required:true},
	                	  edittype:'select', 
	                	  editoptions:{value:{Y:'Yes',N:'NO'}},
	                	  
					  },
					  {name:'editor', index:'editor', width:100, align:'center'},
					  {name:'updateDate', index:'updateDate', width:150, align:'center'},
					  {name:'keywordSerial', index:'KeywordSerial', width:100,
						  editable: true, 
	                	  edittype:"text", editrules:{required:false},
	                	  editoptions:{defaultValue:'新增時,不必填寫',size:10, maxlength:10}
					  },
					  {name:'topicSerial', index:'topicSerial', width:100,
						  editable: true,
	                	  edittype:"text", editrules:{required:true}, hidden:false,
	                	  editoptions:{defaultValue:document.getElementById("topicSerial").value, size:10, maxlength: 10}
					  },
	        ],
	        shrinkToFit:false,
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: true,
	        rowNum: 10, 
	        rowList: [10], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager_keyword",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "Keyword Management",
	        toppager: true,
	        loadComplete: function(data){
	        	$("#ulList1").css("zIndex", 1000);
	        	$("#ulList2").css("zIndex", 1000);
	        	$("#ulList3").css("zIndex", 1000);
	        	$("#ulList4").css("zIndex", 1000);
	        }
	    });
		
		jQuery("#grid_keyword").jqGrid('navGrid','#gridPager_keyword', 
			{edit:true, add:true, del:true, search:false, refresh:false, cloneToTop:true,
			     edittext: "Modify", edittitle: "Modify", 
			     addtext: "Add", addtitle: "Add", 
			     deltext: "Delete", deltitle: "Delete",
			     searchtext: "Search", searchtitle: "Search", 
			     refreshtext: "Refresh", refreshtitle: "Refresh"
			},
			
			// Modify Or Approve
			{ url: '${pageContext.request.contextPath}/crawler/ModifyQueryKeywordAction.do?act=modify',
			  height:'80%',
			  width:'100%',
			  //viewPagerButtons: false, //remove previous/next arrow
              reloadAfterSubmit: true,
              onclickSubmit : function(params, posdata) { 
               	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
               	  $("#allCover").css("display","block");
              },
              afterSubmit : function(response, postdata) {
            	  $("#allCover").css("display","none");
              	  return true;
              },
              closeAfterEdit: true,
              recreateForm: true,
              beforeInitData: function () {
            	  $("#grid_keyword").jqGrid('setColProp','topic',{editoptions:{readonly:true}});
            	  $("#grid_keyword").jqGrid('setColProp','keywordSerial',{editoptions:{readonly:true}});
            	  $("#grid_keyword").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true}});
              },
			}, 
			
			// add options
			{ url: '${pageContext.request.contextPath}/crawler/ModifyQueryKeywordAction.do?act=add',
			  height:'80%',
			  width:'100%',	
		      reloadAfterSubmit:true,
		      closeAfterAdd: true,
		      recreateForm: true,
              beforeInitData: function () {
            	  $("#grid_keyword").jqGrid('setColProp','topic',{editoptions:{readonly:true}});
            	  $("#grid_keyword").jqGrid('setColProp','keywordSerial',{editoptions:{readonly:true}});
            	  $("#grid_keyword").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true}});
              }
			},  
			
			// del options
			{ url:'${pageContext.request.contextPath}/crawler/ModifyQueryKeywordAction.do?act=delete',
			  reloadAfterSubmit: true,
			  closeAfterDel: true,
			  delData: {
				  keywordSerial: function() {
				      var selectedId = $("#grid_keyword").jqGrid('getGridParam', 'selrow');
                      var value = $("#grid_keyword").jqGrid('getCell', selectedId, 'keywordSerial');
                      //alert(value);
                      return value;
			      }
			  },
			  reloadAfterSubmit: false
			},
			
			// search options
			{ Find: "搜尋",  
			  closeAfterSearch: true  
			},
			//refresh optioins
			{ url: '${pageContext.request.contextPath}/user/JqGridFindAllStaff.do?UserType=STAFF'
			} 
		)
		jQuery("#grid_keyword").jqGrid('setFrozenColumns');
		
	});
</script>

</head>
<body>
<div class="mask opacity" id="allCover" style="display:none;">
	<div class="box" id="mDiv">
       <label>Please wait!!</label>
	</div>
</div>

<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/BrandAnalysis_logo.png" alt="BrandAnalysis" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： Crawler Management :
Parameters List (Site's URL & Keyword)</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>


<form action="" method="post" name="form1" id="form1">

<table width="400" >
    <tr>
    	<th>Category：</th>
    	<td>
            
			<s:hidden theme="simple" name="categoryName" id="categoryName" value="%{categoryName}" ReadOnly="true"/>
			<s:hidden theme="simple" name="categorySerial" id="categorySerial" value="%{categorySerial}" ReadOnly="true"/>
		</td>
		
		<th>SubCat.：</th>
    	<td>
            
			<s:hidden theme="simple" name="subCategoryName" id="subCategoryName" value="%{subCategoryName}" ReadOnly="true"/>
			<s:hidden theme="simple" name="subCategorySerial" id="subCategorySerial" value="%{subCategorySerial}" ReadOnly="true"/>
		</td>
		
		<th>Topic：</th>
		<td>
		    <s:hidden theme="simple" name="topic" id="topic" value="%{topic}" ReadOnly="true" />
			<s:hidden theme="simple" name="topicSerial" id="topicSerial" value="%{topicSerial}" ReadOnly="true"/>
		</td>
		
		<td><s:submit onclick="searchForm()" cssClass="comfirm" value="Query" theme="simple"/></td>
	</tr>
</table>



<c:choose>
   <c:when test="${topicSerial == null || topicSerial.isEmpty()}">
     <div div style="background-color:#FFE6D9;padding:10px;margin-bottom:5px;">
  		<span style="color:#2894FF;">請先搜尋，再繼續 (系统需先知道您所要編輯的資料)。</span><br><br>
  		<span style="color:#2894FF;">请先搜寻，再继续动作 (系统必须先取得您所要编辑的资讯来源)。</span><br><br>
  		<span style="color:#2894FF;">Please Query first (We have to know what do you want to modify firstly).</span><br>
  		
  	 </div>
   </c:when>
   <c:otherwise>
  		<div id="all">
  		<div id="mainFunction">
			<left>
				<table id="grid_site"></table>
				<div id="gridPager_site"></div>
			</left>
	         <br>
			<left>
				<table id="grid_keyword"></table>
				<div id="gridPager_keyword"></div>
			</left>
		</div>
  </div>	  
   </c:otherwise>
</c:choose>

</form>

</div>
</div>
</div>
</body>
</html>
 