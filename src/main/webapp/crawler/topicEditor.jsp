<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>

<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Brand Analysis- Topic Editor </title>
<style>
html,body{ height:100%; margin:0; padding:0} 
.mask{height:100%; width:100%; position:fixed; _position:absolute; top:0; z-index:1000; } 
.opacity{ opacity:0.3; filter: alpha(opacity=30); background-color:#000; }
.box {
        display: table-cell;
        vertical-align:middle;
        text-align:center;
        *display: block;
        *font-size: 175px;
        *font-family:Arial;
        
        width:500px;
        height:500px;
        border: 1px solid #eee;
}
.box label {
        vertical-align:middle;
        font-size:64px;
        color: royalblue;
}
</style>

<script type="text/javascript">
//AJAX for Query Category
$(function(){
	init();
});

//Company jqGrid
	$(document).ready(function() {
		$("#company_grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/crawler/GetAllCompanyAction.do'),
			datatype: "JSON",
			height: "260", 
			width:"860",
			colNames:['Company','Note', 'editor', 'UpdateDate','CompanyId'],
			         
	        colModel:[
					  {name:'companyName', index:'companyName',width:35, align:'left',
						  editable: true, editoptions:{size:30, maxlength: 50},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  {name:'companyName2', index:'companyName2',width:20, align:'left',
						  editable: true, editoptions:{size:30, maxlength: 50},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  
					  {name:'editor', index:'editor', width:15, align:'center'},
					  {name:'updateDate', index:'updateDate', width:20, align:'center'},
					  
					  {name:'companyId', index:'companyId', width:15,
						  editable: true, editoptions:{size:10, maxlength:50},
	                	  edittype:"text", editrules:{required:false}, hidden:true
					  },
	        ],
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: true,
	        rowNum: 10, 
	        rowList: [20,40], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#company_gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "Company Maintenance",
	        toppager: true,
	        loadComplete: function(data){
	        	$("#ulList1").css("zIndex", 1000);
	        	$("#ulList2").css("zIndex", 1000);
	        	$("#ulList3").css("zIndex", 1000);
	        	$("#ulList4").css("zIndex", 1000);
	        }
	    });
		
		jQuery("#company_grid").jqGrid('navGrid','#company_gridPager', 
			{edit:true, add:true, del:true, search:false, refresh:false, cloneToTop:true,
			     edittext: "Modify", edittitle: "Modify", 
			     addtext: "Add", addtitle: "Add", 
			     deltext: "Delete", deltitle: "Delete",
			     searchtext: "Search", searchtitle: "Search", 
			     refreshtext: "Refresh", refreshtitle: "Refresh"
			},
			
			// Modify
			{ url: '${pageContext.request.contextPath}/crawler/ModifyCompanyAction.do?act=modify',
			  height:'78%',
			  width:'100%',
			  //viewPagerButtons: false, //remove previous/next arrow
              reloadAfterSubmit: true,
              onclickSubmit : function(params, posdata) { 
               	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
               	  $("#allCover").css("display","block");
              },
              afterSubmit : function(response, postdata) {
            	  $("#allCover").css("display","none");
              	  return true;
              },
              closeAfterEdit: true,
              recreateForm: true,
              beforeInitData: function () {
            	  $("#company_grid").jqGrid('setColProp','companyId',{editoptions:{readonly:true,hidden:true}});
              },
			}, 
			
			// add options
			{ url: '${pageContext.request.contextPath}/crawler/ModifyCompanyAction.do?act=add',
			  height:'78%',
			  width:'100%',
		      closeAfterAdd: true,
		      reloadAfterSubmit: true,
		      recreateForm: true,
              beforeInitData: function () {
            	  $("#company_grid").jqGrid('setColProp','companyId',{editoptions:{readonly:true}});
              }
			},  
			
			// del options
			{ url:'${pageContext.request.contextPath}/crawler/ModifyCompanyAction.do?act=delete',
			  delData: {
				  CompanyId: function() {
				      var selectedId = $("#company_grid").jqGrid('getGridParam', 'selrow');
                      var value = $("#company_grid").jqGrid('getCell', selectedId, 'companyId');
                      //alert(value);
                      return value;
			      }
			  },
			  reloadAfterSubmit: false
			},
			
			// search options
			{ Find: "搜尋",  
			  closeAfterSearch: true  
			},
			//refresh optioins
			{ url: '${pageContext.request.contextPath}/user/ModifyCompanyAction.do?UserType=STAFF'
			} 
		)
		jQuery("#company_grid").jqGrid('setFrozenColumns');
		
	});


//Topic jqGrid
	$(document).ready(function() {
		$("#grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/crawler/GetAllTopicJSONAction.do?topic=${topic}&categorySerial=${categorySerial}&subCategory=${subCategory}'),
			datatype: "JSON",
			height: "550", 
			width:"860",
			colNames:['Cat.','Sub Cat.', 'Topic', 'Enable', 'Company', 'Note','UpdateDate','Editor','TopicSerial'],
			         
	        colModel:[
					  {name:'topicCategory', index:'topicCategory',width:30, align:'left',
						  editable: true, editrules:{required: true}, editoptions:{ readonly: "readonly", hidden:false },
	                	  edittype:'select', hidden:false,
	                	  editoptions:{value:getTopicCategory()},
					  },
					  {name:'subCategory', index:'subCategory',width:50, align:'left',
						  editable: true,
						  editrules:{required: true},
	                	  edittype:'select',
	                	  editoptions:{value:getSubCategory()}
					  },
					  
					  {name:'topic', index:'topic', width:70,
						  editable: true, editoptions:{size:30, maxlength: 50},
	                	  edittype:"text", editrules:{required:true}, hidden:false
					  },
					  {name:'enable', index:'enable', width:10, align:'center',
						  editable: true, editrules:{required: true},
	                	  edittype:'select', 
	                	  editoptions:{value:{Y:'Yes',N:'NO'}},
					  },
					  {name:'companyName', index:'companyName', width:50, align:'left',
						  editable: true,
						  editrules:{required: true},
	                	  edittype:'select',
	                	  editoptions:{value:getCompany()},
					  },
					  {name:'companyName2', index:'companyName2', width:40, align:'center'},
					  {name:'updateDate', index:'updateDate', width:35, align:'center'},
					  {name:'editor', index:'updater', width:20, align:'center'},
					  {name:'topicSerial', index:'topicSerial', width:5,
						  editable: true, editoptions:{size:10, maxlength:50},
	                	  edittype:"text", editrules:{required:false}, hidden:true
					  },
	        ],
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: true,
	        rowNum: 20, 
	        rowList: [20,40], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "Topic Maintenance",
	        toppager: true,
	        loadComplete: function(data){
	        	$("#ulList1").css("zIndex", 1000);
	        	$("#ulList2").css("zIndex", 1000);
	        	$("#ulList3").css("zIndex", 1000);
	        	$("#ulList4").css("zIndex", 1000);
	        }
	    });
		
		jQuery("#grid").jqGrid('navGrid','#gridPager', 
			{edit:true, add:true, del:true, search:false, refresh:true, cloneToTop:true,
			     edittext: "Modify", edittitle: "Modify", 
			     addtext: "Add", addtitle: "Add", 
			     deltext: "Delete", deltitle: "Delete",
			     searchtext: "Search", searchtitle: "Search", 
			     refreshtext: "Refresh", refreshtitle: "Refresh"
			},
			
			// Modify
			{ url: '${pageContext.request.contextPath}/crawler/ModifyQueryTopicAction.do?act=modify',
			  height:'78%',
			  width:'100%',
			  //viewPagerButtons: false, //remove previous/next arrow
              reloadAfterSubmit: true,
              onclickSubmit : function(params, posdata) { 
               	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
               	  $("#allCover").css("display","block");
              },
              afterSubmit : function(response, postdata) {
            	  $("#allCover").css("display","none");
              	  return true;
              },
              closeAfterEdit: true,
              recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true,hidden:true}});
            	  $("#grid").jqGrid('setColProp','topicCategory',{editoptions:{readonly:true,hidden:true}});
            	  $("#grid").jqGrid('setColProp','companyName',{editoptions:{readonly:true,hidden:false}});
              },
			}, 
			
			// add options
			{ url: '${pageContext.request.contextPath}/crawler/ModifyQueryTopicAction.do?act=add',
			  height:'78%',
			  width:'100%',
		      closeAfterAdd: true,
		      reloadAfterSubmit: true,
		      recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true}});
              }
			},  
			
			// del options
			{ url:'${pageContext.request.contextPath}/crawler/ModifyQueryTopicAction.do?act=delete',
			  delData: {
				  TopicSerial: function() {
				      var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
                      var value = $("#grid").jqGrid('getCell', selectedId, 'topicSerial');
                      //alert(value);
                      return value;
			      }
			  },
			  reloadAfterSubmit: false
			},
			
			// search options
			{ Find: "搜尋",  
			  closeAfterSearch: true  
			},
			//refresh optioins
			{ url: '${pageContext.request.contextPath}/crawler/InitTopicSettingsAction.do'
			} 
		)
		jQuery("#grid").jqGrid('setFrozenColumns');
		
	});

//AJAX for jqGrid .
function getCompany() {
	var returnStr = "";
	$.ajax({
		async: false, 
		type: 'POST',
		url: "${pageContext.request.contextPath}/crawler/GetCompanyAction.do",
		dataType: "json",
		success: function(data) {
			for(var i=0; i<data.rows.length; i++) {
				returnStr += data.rows[i].companyId + ":" + data.rows[i].companyName+";";
			}
		}
    });
	//alert(returnStr);
	return returnStr; 
}

//AJAX for jqGrid .
function getTopicCategory() {
	var returnStr = "";
	$.ajax({
		async: false, 
		type: 'POST',
		url: "${pageContext.request.contextPath}/general/GetTopicCategoryAction.do",
		dataType: "json",
		success: function(data) {
			for(var i=0; i<data.topicCategory.length; i++) {
				returnStr += data.topicCategory[i].categorySerial + ":" + data.topicCategory[i].name+";";
			}
		}
    });
	return returnStr; 
}

//AJAX for jqGrid .
function getSubCategory() {
	var returnSubCatStr = "";
	$.ajax({
		async: false,  
		type: 'POST',
		url: "${pageContext.request.contextPath}/general/GetSubcategoryAction.do",
		dataType: "json",
		success: function(data) {
			for(var i=0; i<data.subCategory.length; i++) {
				returnSubCatStr += data.subCategory[i].subCategorySerial + ":" + data.subCategory[i].subName+";";
			}
		}
    });
	return returnSubCatStr; 
}
	


function init() {
	var NameObj = document.getElementById("categoryName");
	NameObj.style.display = "none";
	var tarObj = document.getElementById("categoryName");
	var selectObj = document.createElement("SELECT");
	
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicCategoryAction.do",
			dataType: "json",
			success: function(data) {
				var sValue = NameObj.value;
				var index = 0;
				
				for(var i=0; i<data.topicCategory.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					if(sValue === data.topicCategory[i].name) {
						index = i;
					}
					optionObj.innerHTML = data.topicCategory[i].name;
					optionObj.value = data.topicCategory[i].categorySerial +"@"+ data.topicCategory[i].secondName;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#categorySerial").val( arr[0] );
		$("#categoryName").val( this.options[this.selectedIndex].text);
	};
}

function insertAfter(newEl, targetEl) {
    var parentEl = targetEl.parentNode;
    if(parentEl.lastChild == targetEl) {
        parentEl.appendChild(newEl);
    }else {
        parentEl.insertBefore(newEl,targetEl.nextSibling);
    }            
}
</script>

</head>
<body>
<div class="mask opacity" id="allCover" style="display:none;">
	<div class="box" id="mDiv">
       <label>Please wait!!</label>
	</div>
</div>

<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/BrandAnalysis_logo.png" alt="BrandAnalysis" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH：Company and Topic Maintenance</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>


<form action="" method="post" name="form1" id="form1">

<table width="480" >
    <tr>
    	<th>Category：</th><td>
			<s:textfield theme="simple" name="categoryName" id="categoryName" value="" ReadOnly="true"/>
			<s:hidden theme="simple" name="categorySerial" id="categorySerial" value="" ReadOnly="true"/>
		</td>
		<th>Topic：</th><td>
			<s:textfield theme="simple" name="topic" id="topic" value=""/>
		</td>
		
		<td><s:submit onclick="searchForm()" cssClass="comfirm" value="Query" theme="simple"/></td>
	</tr>
</table>
  <div id="all">
  <div id="mainFunction">
    <left>
		<table id="company_grid"></table>
		<div id="company_gridPager"></div>
	</left>
	<br>
	<left>
		<table id="grid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>
</form>

</div>
</div>
</div>
</body>
</html>
 