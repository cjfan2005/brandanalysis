<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>

<%@ include file="/IncludePage.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Brand Analysis- Manage Parameters </title>
<style>
label.error{display:inline;margin-left: 10px; color: red;}
TD{padding-left:10px;} 
#wrap { display:table; }
#cell { display:table-cell; vertical-align:middle; }
</style>
<style>
html,body{ height:100%; margin:0; padding:0} 
.mask{height:100%; width:100%; position:fixed; _position:absolute; top:0; z-index:1000; } 
.opacity{ opacity:0.3; filter: alpha(opacity=30); background-color:#000000; }
.box {
        display: table-cell;
        vertical-align:middle;
        text-align:center;
        *display: block;
        *font-size: 190px;
        *font-family:Arial;
        
        width:500px;
        height:500px;
        border: 2px solid #eee;
}
.box label {
        vertical-align:middle;
        font-size:68px;
        color: white;
}
</style>


<script type="text/javascript">

$(function(){
	getCategory();
});

function getCategory() {
	var CategoryNameObj = document.getElementById("categoryName");
	CategoryNameObj.style.display = "none";
	var tarObj = document.getElementById("categoryName");
	var selectObj = document.createElement("SELECT");
	selectObj.id = "cat-Select";
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicCategoryAction.do",
			dataType: "json",
			success: function(data) {
				
				var index = 0;
				
				for(var i=0; i<data.topicCategory.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					optionObj.innerHTML = data.topicCategory[i].name;
					optionObj.value = data.topicCategory[i].categorySerial +"@"+ data.topicCategory[i].secondName;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#categorySerial").val( arr[0] );
		
		$("#categoryName").val( this.options[this.selectedIndex].text);
		
		//call to getSubCategory();
		getSubCategory(arr[0]);
	};
}

function getSubCategory(categorySerial) {
	var selectObj = "";
	var SubCategoryNameObj = document.getElementById("subCategoryName");
	SubCategoryNameObj.style.display = "none";
	var tarObj = document.getElementById("subCategoryName");
	
	if(document.getElementById("subCat-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "subCat-Select";
	} else {
		//移除原本的options
		$('#subCat-Select').find('option').remove().end();
		$('#topic-Select').find('option').remove().end();
		$('#period-Select').find('option').remove().end();
		document.getElementById("subCategorySerial").value = "";
		document.getElementById("topicSerial").value = "";
		document.getElementById("periodId").value = "";
		selectObj = document.getElementById("subCat-Select");
	}
	
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetSubcategory2Action.do?categorySerial="+ categorySerial,
			dataType: "json",
			success: function(data) {
				var index = 0;
				var optionObj = "";
				
				for(var i=0; i<data.subCategory.length; i++) {
					optionObj = document.createElement("OPTION");
					
					optionObj.innerHTML = data.subCategory[i].subName;
					optionObj.value = data.subCategory[i].subCategorySerial +"@"+ data.subCategory[i].categorySerial;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#subCategorySerial").val( arr[0] );
		
		$("#subName").val( this.options[this.selectedIndex].text);
		getTopic() ;
	};
	
	insertAfter(selectObj, tarObj);
}


function getTopic() {
	var catSerial = document.getElementById("categorySerial").value;
	var subCatSerial = document.getElementById("subCategorySerial").value;
	var selectObj = "";
	
	var TopicObj = document.getElementById("topic");
	TopicObj.style.display = "none";
	var tarObj = document.getElementById("topic");
	selectObj = document.createElement("SELECT");
	
	if(document.getElementById("topic-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "topic-Select";
	} else {
		//移除原本的options
		$('#topic-Select').find('option').remove().end();
		document.getElementById("topicSerial").value = "";
		selectObj = document.getElementById("topic-Select");
		
		$('#period-Select').find('option').remove().end();
		document.getElementById("periodId").value = "";
	}
	
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicJSONAction.do?categorySerial="+catSerial+"&subCategorySerial="+subCatSerial,
			dataType: "json",
			success: function(data) {
				var index = 0;
				
				for(var i=0; i<data.topicJSONStr.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					optionObj.innerHTML = data.topicJSONStr[i].topic;
					optionObj.value = data.topicJSONStr[i].topicSerial +"@"+ data.topicJSONStr[i].updateDate;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#topicSerial").val( arr[0] );
		
		$("#topic").val( this.options[this.selectedIndex].text);
		
		//清除下一個欄位裡的資料
		document.getElementById("periodId").value = "";
		getPeriod();
	};
	
}

function getPeriod() {
	var topicSerial = document.getElementById("topicSerial").value;
	var selectObj = "";
	
	var PeriodObj = document.getElementById("periodName");
	PeriodObj.style.display = "none";
	var tarObj = document.getElementById("periodName");
	selectObj = document.createElement("SELECT");
	
	if(document.getElementById("period-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "period-Select";
	} else {
		//移除原本的options
		$('#period-Select').find('option').remove().end();
		document.getElementById("periodId").value = "";
		selectObj = document.getElementById("period-Select");
	}
	
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetPeriodJSONAction.do?topicSerial="+topicSerial,
			dataType: "json",
			success: function(data) {
				var i=0, index = 0;
				var optionObj = null;
				
				for(i=0; i<data.periodJSONStr.length; i++) {
					
					optionObj = document.createElement("OPTION");
					if(i==0) {
						optionObj.innerHTML = data.periodJSONStr[i].periodName;
						optionObj.value = "" ;
					}else {
						optionObj.innerHTML = data.periodJSONStr[i].periodName +" - "+ data.periodJSONStr[i].duringTime;
						optionObj.value = data.periodJSONStr[i].periodId ;
					}
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		$("#periodId").val( this.value );
		
		$("#periodName").val(this.options[this.selectedIndex].text);
	};
}

function insertAfter(newEl, targetEl) {
    var parentEl = targetEl.parentNode;
    if(parentEl.lastChild == targetEl) {
        parentEl.appendChild(newEl);
    }else {
        parentEl.insertBefore(newEl,targetEl.nextSibling);
    }            
}


function submitForm() {
	
	
	$("#form1").validate({});//check all columns.
	
	$("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
 	$("#allCover").css("display","block");
 	  
	$('#form1').attr('action',
	'${pageContext.request.contextPath}/semantic/AddArticleAction.do');
    $('#form1').submit();
    //$("#allCover").css("display","none");
}


</script>

<!-- 內文長度限制 -->
<script language="javascript" type="text/javascript">

    $(document).ready( function () {

	maxLength = $("textarea#content").attr("maxlength");
        $("textarea#content").after("<div><font color='red'>Max Length=1200。 <span id='remainingLengthTempId'>"
                  + maxLength + "</span> remaining</font></div>");

        $("textarea#content").bind("keyup change", function(){checkMaxLength(this.id,  maxLength); } )

    });

    function checkMaxLength(textareaID, maxLength){

        currentLengthInTextarea = $("#"+textareaID).val().length;
        $(remainingLengthTempId).text(parseInt(maxLength) - parseInt(currentLengthInTextarea));

		if (currentLengthInTextarea > (maxLength)) {

			// Trim the field current length over the maxlength.
			$("textarea#content").val($("textarea#content").val().slice(0, maxLength));
			$(remainingLengthTempId).text(0);

		}
    }
</script>

</head>
<body>
<div class="mask opacity" id="allCover" style="display:none;">
	<div class="box" id="mDiv">
       <label>Please wait!!</label>
	</div>
</div>

<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/BrandAnalysis_logo.png" alt="BrandAnalysis" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： Article Management :
Add Article </div>
<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>

<form action="" method="post" name="form1" id="form1">

<table width="400" >
    <tr>
    	<th>Category：</th>
    	<td>
            
			<s:hidden theme="simple" name="categoryName" id="categoryName" value="%{categoryName}" ReadOnly="true"/>
			<s:hidden theme="simple" name="categorySerial" id="categorySerial" value="%{categorySerial}" ReadOnly="true" cssClass="required form-control"/>
		</td>
	</tr>
	<tr>	
		<th>SubCat.：</th>
    	<td>
            
			<s:hidden theme="simple" name="subCategoryName" id="subCategoryName" value="%{subCategoryName}" ReadOnly="true"/>
			<s:hidden theme="simple" name="subCategorySerial" id="subCategorySerial" value="%{subCategorySerial}" ReadOnly="true" cssClass="required form-control"/>
		</td>
	</tr>
	<tr>	
		<th>Topic：</th>
		<td>
		    <s:hidden theme="simple" name="topic" id="topic" value="%{topic}" ReadOnly="true" />
			<s:hidden theme="simple" name="topicSerial" id="topicSerial" value="%{topicSerial}" ReadOnly="true" cssClass="required form-control"/>
		</td>
	</tr>
	<tr>	
		<th>Period：</th>
		<td>
		    <s:hidden theme="simple" name="periodName" id="periodName" value="%{periodName}" ReadOnly="true" />
			<s:hidden theme="simple" name="periodId" id="periodId" value="%{periodId}" ReadOnly="true" cssClass="required form-control"/>
		</td>
	</tr>
	
	<tr>
		<th>Title：</th>
		<td colspan="3"><s:textfield theme="simple" name="title" id="title" type="text" size="60" maxlength="100" value="%{title}" cssClass="required form-control" /></td>
	</tr>
	<tr><th>Content：</th>
	   <td colspan="3">
	     <s:textarea theme="simple" label="content" name="content" id="content" value="%{content}" 
	        maxlength="1200" 
	        cssClass="required" 
            cssStyle="width:680px;height:500px ; resize: none;"></s:textarea>
	   </td>
	</tr>
	
	
	<tr>
	   <td colspan="4">
		   <s:submit onclick="submitForm()" cssClass="comfirm" value="ADD" theme="simple"/>
	   </td>
	</tr>
</table>
</form>



</div>
</div>
</div>
</body>
</html>
 