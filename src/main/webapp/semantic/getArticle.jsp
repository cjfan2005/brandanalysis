<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>

<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Brand Analysis- Article </title>
<style>
label.error{display:inline;margin-left: 10px; color: red;}
TD{padding-left:10px;} 
#wrap { display:table; }
#cell { display:table-cell; vertical-align:middle; }
</style>
<style>
html,body{ height:100%; margin:0; padding:0} 
.mask{height:100%; width:100%; position:fixed; _position:absolute; top:0; z-index:1000; } 
.opacity{ opacity:0.3; filter: alpha(opacity=30); background-color:#000000; }
.box {
        display: table-cell;
        vertical-align:middle;
        text-align:center;
        *display: block;
        *font-size: 190px;
        *font-family:Arial;
        
        width:500px;
        height:500px;
        border: 2px solid #eee;
}
.box label {
        vertical-align:middle;
        font-size:68px;
        color: white;
}
</style>

<script type="text/javascript">
//AJAX for Query Category
$(function(){
	getCategory();
});

function getCategory() {
	var CategoryNameObj = document.getElementById("categoryName");
	CategoryNameObj.style.display = "none";
	var tarObj = document.getElementById("categoryName");
	var selectObj = document.createElement("SELECT");
	selectObj.id = "cat-Select";
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicCategoryAction.do",
			dataType: "json",
			success: function(data) {
				
				var index = 0;
				
				for(var i=0; i<data.topicCategory.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					optionObj.innerHTML = data.topicCategory[i].name;
					optionObj.value = data.topicCategory[i].categorySerial +"@"+ data.topicCategory[i].secondName;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#categorySerial").val( arr[0] );
		
		$("#categoryName").val( this.options[this.selectedIndex].text);
		
		//call to getSubCategory();
		getSubCategory(arr[0]);
	};
}

function getSubCategory(categorySerial) {
	var selectObj = "";
	var SubCategoryNameObj = document.getElementById("subCategoryName");
	SubCategoryNameObj.style.display = "none";
	var tarObj = document.getElementById("subCategoryName");
	
	if(document.getElementById("subCat-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "subCat-Select";
	} else {
		//移除原本的options
		$('#subCat-Select').find('option').remove().end();
		$('#topic-Select').find('option').remove().end();
		$('#period-Select').find('option').remove().end();
		document.getElementById("subCategoryName").value = "";
		document.getElementById("subCategorySerial").value = "";
		document.getElementById("topic").value = "";
		document.getElementById("topicSerial").value = "";
		document.getElementById("periodId").value = "";
		document.getElementById("periodName").value = "";
		selectObj = document.getElementById("subCat-Select");
	}
	
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetSubcategory2Action.do?categorySerial="+ categorySerial,
			dataType: "json",
			success: function(data) {
				var index = 0;
				var optionObj = "";
				
				for(var i=0; i<data.subCategory.length; i++) {
					optionObj = document.createElement("OPTION");
					
					optionObj.innerHTML = data.subCategory[i].subName;
					optionObj.value = data.subCategory[i].subCategorySerial +"@"+ data.subCategory[i].categorySerial;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#subCategorySerial").val( arr[0] );
		
		$("#subName").val( this.options[this.selectedIndex].text);
		getTopic() ;
	};
	
	insertAfter(selectObj, tarObj);
}


function getTopic() {
	var catSerial = document.getElementById("categorySerial").value;
	var subCatSerial = document.getElementById("subCategorySerial").value;
	var selectObj = "";
	
	var TopicObj = document.getElementById("topic");
	TopicObj.style.display = "none";
	var tarObj = document.getElementById("topic");
	selectObj = document.createElement("SELECT");
	
	if(document.getElementById("topic-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "topic-Select";
	} else {
		//移除原本的options
		$('#topic-Select').find('option').remove().end();
		document.getElementById("topicSerial").value = "";
		selectObj = document.getElementById("topic-Select");
		
		$('#period-Select').find('option').remove().end();
		document.getElementById("periodId").value = "";
	}
	
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicJSONAction.do?categorySerial="+catSerial+"&subCategorySerial="+subCatSerial,
			dataType: "json",
			success: function(data) {
				var index = 0;
				
				for(var i=0; i<data.topicJSONStr.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					optionObj.innerHTML = data.topicJSONStr[i].topic;
					optionObj.value = data.topicJSONStr[i].topicSerial +"@"+ data.topicJSONStr[i].updateDate;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#topicSerial").val( arr[0] );
		
		$("#topic").val( this.options[this.selectedIndex].text);
		
		//清除下一個欄位裡的資料
		document.getElementById("periodId").value = "";
	    document.getElementById("periodName").value = "";
		getPeriod();
	};
	
}

function getPeriod() {
	var topicSerial = document.getElementById("topicSerial").value;
	var selectObj = "";
	
	var PeriodObj = document.getElementById("periodName");
	PeriodObj.style.display = "none";
	var tarObj = document.getElementById("periodName");
	selectObj = document.createElement("SELECT");
	
	if(document.getElementById("period-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "period-Select";
	} else {
		//移除原本的options
		$('#period-Select').find('option').remove().end();
		document.getElementById("periodId").value = "";
		selectObj = document.getElementById("period-Select");
	}
	
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetPeriodJSONAction.do?topicSerial="+topicSerial,
			dataType: "json",
			success: function(data) {
				var i=0, index = 0;
				var optionObj = null;
				
				for(i=0; i<data.periodJSONStr.length; i++) {
					
					optionObj = document.createElement("OPTION");
					if(i==0) {
						optionObj.innerHTML = data.periodJSONStr[i].periodName;
						optionObj.value = "" ;
					}else {
						optionObj.innerHTML = data.periodJSONStr[i].periodName +" - "+ data.periodJSONStr[i].duringTime;
						optionObj.value = data.periodJSONStr[i].periodId ;
					}
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		$("#periodId").val( this.value );
		
		$("#periodName").val(this.options[this.selectedIndex].text);
	};
}

function insertAfter(newEl, targetEl) {
    var parentEl = targetEl.parentNode;
    if(parentEl.lastChild == targetEl) {
        parentEl.appendChild(newEl);
    }else {
        parentEl.insertBefore(newEl,targetEl.nextSibling);
    }            
}

//jqGrid article start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	$(document).ready(function() {
		$("#grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/semantic/GetArticleAction.do?periodId=${periodId}&topicSerial=${topicSerial}'),
			datatype: "JSON",
			height: "330", 
			width:"860",
			colNames:['Topic','title', 'Content', 'Status', 'Period','Detail', 'InsertDate', 'UpdateDate', 'Editor', 'ArticleId','PeriodId'],
			         
	        colModel:[
					  {name:'topic', index:'topic',width:150, align:'center', frozen: false},
					  {name:'title', index:'title',width:180, align:'left'},
					  
					  {name:'content', index:'content', width:200},
					  {name:'status', index:'status', width:80, align:'center'},
					  {name:'period', index:'period', width:200, align:'center'},
					  
					  {   //Show Button
	                	  name:'action',index:'action',sortable:false, width:60, align:'center',sortable:false,
	                	  formatter:function (cellvalue, options, rowObject) {
	                		 //alert("aId:" + rowObject.articleId); //get row data
	                	     return '<input type="button" onClick="clickme(\'' + rowObject.articleId + '/' + rowObject.title+ '/' + rowObject.topic +  '\')" type="button" value="Detail"/>'
	                	  }
	                  },
	                  
	                  {name:'insertDate', index:'insertDate', width:120, align:'center', hiddne:true},
					  {name:'updateDate', index:'updateDate', width:120, align:'center', hidden:true},
					  {name:'editor', index:'editor', width:100, align:'center',sortable:false,},
	                  {name:'articleId', index:'articleId', width:60, align:'center',sortable:false},
	                  {name:'periodId', index:'periodId', width:60, align:'center',sortable:false},
	        ],
	        shrinkToFit:false,
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: true,
	        rowNum: 20, 
	        rowList: [20,40], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "Article List",
	        toppager: true,
	        loadComplete: function(data){
	        	$("#ulList1").css("zIndex", 1000);
	        	$("#ulList2").css("zIndex", 1000);
	        	$("#ulList3").css("zIndex", 1000);
	        	$("#ulList4").css("zIndex", 1000);
	        }
	    });
		
		jQuery("#grid").jqGrid('navGrid','#gridPager', 
			{edit:false, add:false, del:true, search:false, refresh:false, cloneToTop:true,
			     edittext: "Modify", edittitle: "Modify", 
			     addtext: "Add", addtitle: "Add", 
			     deltext: "Delete", deltitle: "Delete",
			     searchtext: "Search", searchtitle: "Search", 
			     refreshtext: "Refresh", refreshtitle: "Refresh"
			},
			
			// Modify
			{ url: '${pageContext.request.contextPath}/semantic/ModifyQueryTopicAction.do?act=modify',
			  height:'78%',
			  width:'100%',
			  //viewPagerButtons: false, //remove previous/next arrow
              reloadAfterSubmit: true,
              onclickSubmit : function(params, posdata) { 
               	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
               	  $("#allCover").css("display","block");
              },
              afterSubmit : function(response, postdata) {
            	  $("#allCover").css("display","none");
              	  return true;
              },
              closeAfterEdit: true,
              recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true,hidden:true}});
            	  $("#grid").jqGrid('setColProp','topicCategory',{editoptions:{readonly:true,hidden:true}});
              },
			}, 
			
			// add options
			{ url: '${pageContext.request.contextPath}/semantic/ModifyQueryTopicAction.do?act=add',
			  height:'78%',
			  width:'100%',
		      closeAfterAdd: true,
		      reloadAfterSubmit: true,
		      recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true}});
              }
			},  
			
			// del options
			{ url:'${pageContext.request.contextPath}/semantic/DeleteArticleIdByIdAction.do',
			  delData: {
				  ArticleId: function() {
				      var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
                      var value = $("#grid").jqGrid('getCell', selectedId, 'articleId');
                      //alert(value);
                      return value;
			      }
			  },
			  reloadAfterSubmit: false
			},
			
			// search options
			{ Find: "搜尋",  
			  closeAfterSearch: true  
			},
			//refresh optioins
			{ url: '${pageContext.request.contextPath}/semantic/JqGridFindAllStaff.do?UserType=STAFF'
			} 
		)
		jQuery("#grid").jqGrid('setFrozenColumns');
		
	});
//jqgrid article end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
function clickme(conditions) {
	$("#form1").validate({});//check all columns.
	$("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
 	$("#allCover").css("display","block");
 	
 	var splitStr = conditions.split("/");
 	//alert(splitStr[0] +"," +splitStr[1]+","+ splitStr[2]);
 	
	var url = '${pageContext.request.contextPath}/semantic/InitSentenceDetailAction.do?articleId='+splitStr[0]+'&title='+ splitStr[1] +'&topic='+splitStr[2];
	popup(url);
	//$('#form1').attr('action', '${pageContext.request.contextPath}/ft/SentenceDetailAction.do?detailIds=' + detailIds);
	//$('#form1').submit();
	if (window.focus) {newwin.focus()}
	$("#allCover").css("display","none");
}

function popup(url) {
	params  = 'width='+(screen.width/2);
	params += ', height='+(screen.height/2)+100;
	params += ', top=0, left=0'
	params += ', fullscreen=0';
	params += ', scrollbars=yes';
	params += ', location=no';
	
	newwin=window.open(url,'popUpWindow', params);
	if (window.focus) {newwin.focus()}
	$("#allCover").css("display","none");
	return false;
}

function showTFDF() {
	$("#form1").validate({});//check all columns.
	$("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
 	$("#allCover").css("display","block");
	
 	
 	//var ts = document.getElementById("topicSerial").value;
	//$('#form1').attr('action', '${pageContext.request.contextPath}/semantic/InitTfDfCalculatorAction.do?topicSerial='+ts[0]);
	
    $('#form1').attr('action', '${pageContext.request.contextPath}/semantic/InitGetTfDfAction.do');
	$('#form1').submit();
	
	//if (window.focus) {newwin.focus()}
	//$("#allCover").css("display","none");
}

function calTFDF() {
	$("#form1").validate({});//check all columns.
	$("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
 	$("#allCover").css("display","block");
	
 	
 	//var ts = document.getElementById("topicSerial").value;
	//$('#form1').attr('action', '${pageContext.request.contextPath}/semantic/InitTfDfCalculatorAction.do?topicSerial='+ts[0]);
	
    $('#form1').attr('action', '${pageContext.request.contextPath}/semantic/TfDfCalculatorAction.do');
	$('#form1').submit();
	
	//if (window.focus) {newwin.focus()}
	//$("#allCover").css("display","none");
}


//jqGrid domain know how start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
$(document).ready(function() {
	$("#domainKnowhowTopic_grid").jqGrid({
		url:encodeURI('${pageContext.request.contextPath}/semantic/GetDomainKnowhowByIdAction.do?topicSerial=${topicSerial}'),
		datatype: "JSON",
		height: "200", 
		width:"860",
		colNames:['Topic','議題詞(Domain knowhow)','Editor','UpdateDate', 'TopicSerial', 'DomainKnowhow Id',],
		         
        colModel:[
                  {name:'topic', index:'topic',width:160, align:'center',
                	  editable: true,frozen: true,
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{defaultValue:'新增時,不必填寫', size:10, maxlength:10}
                  },
                  
				  {name:'domainKnowhowName', index:'domainKnowhowName', width:200, align:'center',frozen: true,
                	  editable: true, editoptions:{size:50, maxlength:50},
                	  edittype:"text", editrules:{required:true} 
				  },
				  
				  {name:'editor', index:'editor',width:100, align:'center'},
				  {name:'updateDate', index:'updateDate', width:150, align:'center'},
                  
                  {name:'topicSerial', index:'topicSerial', width:150, align:'center',
                	  editable: false, sortable:false,
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{ size:10, maxlength:10}
                  },
                  {name:'domainKnowhowId', index:'domainKnowhowId', width:180, align:'center',
                	  editable: true, sortable:false,
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{defaultValue:'新增時,不必填寫', size:10, maxlength:10}
                  },
        ],
        shrinkToFit:false,
        viewrecords: true,
        gridview: true, 
        rownumbers: true,
        rowNum: 10, 
        rowList: [10,20,30], 
        pgbuttons: true,
        pginput: true,
        pager: "#domainKnowhowTopic_gridPager",
        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
        caption: "Domain know how topic List",
        toppager: true,
        loadComplete: function(data){
        	$("#ulList1").css("zIndex", 1000);
        	$("#ulList2").css("zIndex", 1000);
        	$("#ulList3").css("zIndex", 1000);
        	$("#ulList4").css("zIndex", 1000);
        }
    });
	
	jQuery("#domainKnowhowTopic_grid").jqGrid('navGrid','#domainKnowhowTopic_gridPager', 
		{edit:true, add:true, del:true, search:false, refresh:false, cloneToTop:true,
		     edittext: "Modify", edittitle: "Modify", 
		     addtext: "Add", addtitle: "Add", 
		     deltext: "Delete", deltitle: "Delete",
		     searchtext: "Search", searchtitle: "Search", 
		     refreshtext: "Refresh", refreshtitle: "Refresh"
		},
		
		// Modify
		{ url: '${pageContext.request.contextPath}/semantic/ModifyDomainKnowhowTopicAction.do?act=modify&topicSerial=${topicSerial}',
		  height:'78%',
		  width:'100%',
		  //viewPagerButtons: false, //remove previous/next arrow
          reloadAfterSubmit: true,
          onclickSubmit : function(params, posdata) { 
           	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
           	  $("#allCover").css("display","block");
          },
          afterSubmit : function(response, postdata) {
        	  $("#allCover").css("display","none");
          	  return true;
          },
          closeAfterEdit: true,
          recreateForm: true,
          beforeInitData: function () {
        	  $("#domainKnowhowTopic_grid").jqGrid('setColProp','topic',{editoptions:{readonly:true,hidden:false}});
        	  $("#domainKnowhowTopic_grid").jqGrid('setColProp','domainKnowhowId',{editoptions:{readonly:true,hidden:true}});//隱藏,java還是抓的到!
        	  $("#domainKnowhowTopic_grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true,hidden:true}});
          },
		}, 
		
		// add options
		{ url: '${pageContext.request.contextPath}/semantic/ModifyDomainKnowhowTopicAction.do?act=add&topicSerial=${topicSerial}',
		  height:'78%',
		  width:'100%',
	      closeAfterAdd: true,
	      reloadAfterSubmit: true,
	      recreateForm: true,
          beforeInitData: function () {
        	  $("#domainKnowhowTopic_grid").jqGrid('setColProp','topic',{editoptions:{readonly:true,hidden:true}});
        	  $("#domainKnowhowTopic_grid").jqGrid('setColProp','domainKnowhowId',{editoptions:{readonly:true}});
        	  $("#domainKnowhowTopic_grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true}});
          }
		},  
		
		// del options
		{ url:'${pageContext.request.contextPath}/semantic/ModifyDomainKnowhowTopicAction.do?act=delete',
		  delData: {
			  DomainKnowhowId: function() {
			      var selectedId = $("#domainKnowhowTopic_grid").jqGrid('getGridParam', 'selrow');
                  var value = $("#domainKnowhowTopic_grid").jqGrid('getCell', selectedId, 'domainKnowhowId');
                  //alert(value);
                  return value;
		      }
		  },
		  reloadAfterSubmit: false
		},
		
		// search options
		{ Find: "搜尋",  
		  closeAfterSearch: true  
		},
		//refresh optioins
		{ url: '${pageContext.request.contextPath}/semantic/JqGridFindAllStaff.do?UserType=STAFF'
		} 
	)
	jQuery("#domainKnowhowTopic_grid").jqGrid('setFrozenColumns');
	
});
//jqgrid domain know how end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//特定時間區間jqGrid Statistic on domain know how start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

$(document).ready(function() {
		$("#statistic_dkh_Grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/semantic/GetStatisticDomainKnowhowByIdAction.do?periodId=${periodId}&topicSerial=${topicSerial}&topic=${topic}&periodName=${periodName}'),
			datatype: "JSON",
			height: "100%+150", 
			width:"860",
			colNames:['議題詞','關鍵字個數','總分','正分總和','負分總和', 'topicSerial', '議題id','periodId'],
	        colModel:[
                 
                 {name:'domainKnowhowName', index:'domainKnowhowName', width:180, align:'center',hidden: false, sort:false},
                 {name:'counter', index:'counter', width:120, align:'right',hidden: false, sort:false},
                 {name:'totalScore', index:'totalScore', width:120, align:'right', hidden: false, sort:false},
                 {name:'positiveScore', index:'positiveScore', width:120, align:'right', hidden: false, sort:false},
                 {name:'negativeScore', index:'negativeScore', width:120, align:'right', hidden: false, sort:false},
                 {name:'topicSerial', index:'topicSerial', width:120, align:'center', hidden: true, sort:false},
                 {name:'domainKnowhowId', index:'domainKnowhowId', width:120, align:'center',hidden: true, sort:false},
                 {name:'periodId', index:'periodId', width:120, align:'center', hidden: true, sort:false},
                 

	        ],
	        shrinkToFit:false,
	        viewrecords: true,
	        gridview: true, 
	        
	        pgbuttons: false,
	        pginput: false,
	        pager: "#statistic_dkh_gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "議題詞vs情緒分數 (Statistic on domain know how)",
	        subGrid : true,
	        loadComplete: function(data) {
	        	var counter=[], domainKnowhow=[], totalScore=[];
	    	    
	        	//1.前處理
	        	for (var i=0 ; i<data.rows.length; i++)
	        	{
	        		counter[i] = data.rows[i].counter;//該議題詞有幾個關鍵字(keyword)
	        	    domainKnowhow[i] = data.rows[i].domainKnowhowName;//議題詞
	        	    totalScore[i] = data.rows[i].totalScore;//分數
	        	}
	        	drawHighChart(domainKnowhow, counter, totalScore);//forward to chart function
	        },
	        
	    	//subGrid start >>>
	    	subGridRowExpanded: function(subgrid_id, row_id) {
				var headData = jQuery("#statistic_dkh_Grid").jqGrid('getRowData',row_id);
				var conditions = "topicSerial="+headData.topicSerial+"&periodId="+headData.periodId+"&domainKnowhowId="+headData.domainKnowhowId;
				var subgrid_table_id;
				subgrid_table_id = subgrid_id+"_t";
			 
				$("#"+subgrid_id).html("<table id='"+ subgrid_table_id+"' class='scroll'></table>");
			  	$("#"+subgrid_table_id).jqGrid({
			  		url:"${pageContext.request.contextPath}/semantic/GetKeywordsFormStatisticAction.do?"+conditions,
			  		datatype: "json",
			  		width:"750",
			  		colNames: ['關鍵字','TF','DF','NTUSD正向個數','NTUSD負面個數','keywordId'],
			  		colModel: [
			      		{name:"keyword",index:"keyword",width:12, align:'center',sortable:false},
			      		{name:"termFrequency",index:"termFrequency",width:12, align:'center',sortable:false},
			      		{name:"docFrequency",index:"docFrequency",width:12, align:'center',sortable:false},
			      		{name:"positiveNTUSD",index:"positiveNTUSD",width:15, align:'center', sortable:false,hidden: true},
			      		{name:"negativeNTUSD",index:"negativeNTUSD",width:15, align:'center', sortable:false,hidden: true},
			      		{name:"keywordId",index:"keywordId",width:20, align:'center', sortable:false,hidden: true}
			  		],
			  		height: "100%"+20,
			  		rowNum:999,
			  		prmNames:{rows:"pageSize"},
			  	});
			},
	    	//subgrid end <<<
	    });
});
//特定時間區間jqgrid Statistic on domain know how end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//全時間jqGrid Statistic on domain know how start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

$(document).ready(function() {
		$("#statistic_all_dkh_Grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/semantic/GetAllStatisticDomainKnowhowByIdAction.do?topicSerial=${topicSerial}&topic=${topic}&periodName=All'),
			datatype: "JSON",
			height: "100%+150", 
			width:"860",
			colNames:['議題詞','關鍵字個數','總分','正分總和','負分總和', 'topicSerial', '議題id','periodId'],
	        colModel:[
                 
                 {name:'domainKnowhowName', index:'domainKnowhowName', width:180, align:'center',hidden: false, sort:false},
                 {name:'counter', index:'counter', width:120, align:'right',hidden: false, sort:false},
                 {name:'totalScore', index:'totalScore', width:120, align:'right', hidden: false, sort:false},
                 {name:'positiveScore', index:'positiveScore', width:120, align:'right', hidden: false, sort:false},
                 {name:'negativeScore', index:'negativeScore', width:120, align:'right', hidden: false, sort:false},
                 {name:'topicSerial', index:'topicSerial', width:120, align:'center', hidden: true, sort:false},
                 {name:'domainKnowhowId', index:'domainKnowhowId', width:120, align:'center',hidden: true, sort:false},
                 {name:'periodId', index:'periodId', width:120, align:'center', hidden: true, sort:false},
                 

	        ],
	        shrinkToFit:false,
	        viewrecords: true,
	        gridview: true, 
	        
	        pgbuttons: false,
	        pginput: false,
	        pager: "#statistic_all_dkh_gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "議題詞vs情緒分數 (Statistic on domain know how)",
	        subGrid : true,
	        loadComplete: function(data) {
	        	var counter=[], domainKnowhow=[], totalScore=[];
	    	    
	        	//1.前處理
	        	for (var i=0 ; i<data.rows.length; i++)
	        	{
	        		counter[i] = data.rows[i].counter;//該議題詞有幾個關鍵字(keyword)
	        	    domainKnowhow[i] = data.rows[i].domainKnowhowName;//議題詞
	        	    totalScore[i] = data.rows[i].totalScore;//分數
	        	}
	        	drawHighChart(domainKnowhow, counter, totalScore);//forward to chart function
	        },
	        
	    	//subGrid start >>>
	    	subGridRowExpanded: function(subgrid_id, row_id) {
				var headData = jQuery("#statistic_all_dkh_Grid").jqGrid('getRowData',row_id);
				var conditions = "topicSerial="+headData.topicSerial+"&domainKnowhowId="+headData.domainKnowhowId;
				var subgrid_table_id;
				subgrid_table_id = subgrid_id+"_t";
			 
				$("#"+subgrid_id).html("<table id='"+ subgrid_table_id+"' class='scroll'></table>");
			  	$("#"+subgrid_table_id).jqGrid({
			  		url:"${pageContext.request.contextPath}/semantic/GetKeywordsFormAllStatisticAction.do?"+conditions,
			  		datatype: "json",
			  		width:"750",
			  		colNames: ['關鍵字','TF','DF','NTUSD正向個數','NTUSD負面個數','keywordId'],
			  		colModel: [
			      		{name:"keyword",index:"keyword",width:12, align:'center',sortable:false},
			      		{name:"termFrequency",index:"termFrequency",width:12, align:'center',sortable:false},
			      		{name:"docFrequency",index:"docFrequency",width:12, align:'center',sortable:false},
			      		{name:"positiveNTUSD",index:"positiveNTUSD",width:15, align:'center', sortable:false,hidden: true},
			      		{name:"negativeNTUSD",index:"negativeNTUSD",width:15, align:'center', sortable:false,hidden: true},
			      		{name:"keywordId",index:"keywordId",width:20, align:'center', sortable:false,hidden: true}
			  		],
			  		height: "100%"+20,
			  		rowNum:999,
			  		prmNames:{rows:"pageSize"},
			  	});
			},
	    	//subgrid end <<<
	    });
});
//時間區間jqgrid Statistic on domain know how end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//CHART JS.
function drawHighChart(domainKnowhow, counter, totalScore) {
	
	$('#container').highcharts({
		
		
        chart: {
            zoomType: 'x'
        },
        credits: {
            enabled: false //移除highchats右下方logo.
        },
        title: {
            text: '議題詞vs關鍵字 ${topic}'
        },
        subtitle: {
            text:  '${periodName}' //show 
        },
        xAxis: [{
            categories: domainKnowhow, //議題詞
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '${value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            title: {
                text: '關鍵字個數 (個)',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: '情緒總分',
                style: {
                    color: "red"
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: "red"
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 0,
            verticalAlign: 'top',
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        plotOptions: {
        	column: {
                dataLabels: {
                    enabled: true
                }
            },
            spline: {
                dataLabels: {
                    enabled: true,
                    color: "red"
                }
            }
        },
        
        series: [{
            name: '關鍵字個數',
            type: 'column',
            yAxis: 0,
            data: counter, //個數
            color: Highcharts.getOptions().colors[0],
            tooltip: {
                valueSuffix: ''
            }

        }, {
            name: '情緒總分',
            type: 'spline',
            color: "red",
            yAxis: 1,
            data: totalScore,//close
            tooltip: {
                valueSuffix: ''
            }
        }]
    });	
    
}

</script>

</head>
<body>
<div class="mask opacity" id="allCover" style="display:none;">
	<div class="box" id="mDiv">
       <label>Please wait!!</label>
	</div>
</div>

<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/BrandAnalysis_logo.png" alt="BrandAnalysis" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： Article List</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:if test="hasActionMessages()">

		<div class="successMsg" align="center" style="margin:0 auto;">
		   <s:actionmessage/>
		</div>
</s:if>
<s:if test="hasActionErrors()">
		<div class="errorMsg" align="center" style="margin:0 auto;">
		   <s:actionerror/>
		</div>
</s:if>


<form action="" method="post" name="form1" id="form1">

<table width="480" >
	<tr>
    	<th>Category：</th>
    	<td>
            
			<s:hidden theme="simple" name="categoryName" id="categoryName" value="%{categoryName}" ReadOnly="true"/>
			<s:hidden theme="simple" name="categorySerial" id="categorySerial" value="%{categorySerial}" ReadOnly="true" cssClass="required form-control"/>
		</td>
	
		<th>SubCat.：</th>
    	<td>
            
			<s:hidden theme="simple" name="subCategoryName" id="subCategoryName" value="%{subCategoryName}" ReadOnly="true"/>
			<s:hidden theme="simple" name="subCategorySerial" id="subCategorySerial" value="%{subCategorySerial}" ReadOnly="true" cssClass="required form-control"/>
		</td>
	
		<th>Topic：</th>
		<td>
		    <s:textfield theme="simple" name="topic" id="topic" value="%{topic}" ReadOnly="true" />
			<s:hidden theme="simple" name="topicSerial" id="topicSerial" value="%{topicSerial}" ReadOnly="true" cssClass="required form-control"/>
		</td>
	</tr>
	<tr>
	    <th>Period：</th>
		<td>
		    <s:textfield theme="simple" name="periodName" id="periodName" value="%{periodName}" ReadOnly="true" />
			<s:hidden theme="simple" name="periodId" id="periodId" value="%{periodId}" ReadOnly="true" />
		</td>	
		<td>
		    <s:submit onclick="searchForm()" cssClass="comfirm" value="Query" theme="simple"/>
		</td>
		<c:choose>
	      <c:when test="${(topicSerial != null && not empty topicSerial) && ( empty periodId) }">
	 		  <!-- 計算所有的TF/DF
	 		  <td>
				  <s:submit onclick="showTFDF()" cssClass="comfirm" value="TF/DF結果[全部]" theme="simple"/>
			  </td>
			  <td>
				  <s:submit onclick="calTFDF()" cssClass="comfirm" value="計算[全部]TF/DF" theme="simple"/>
			  </td>
			  -->
	      </c:when>
	      <c:when test="${(topicSerial != null && not empty topicSerial) && (periodId != null && not empty periodId) }">
	 		  <!-- 只計算時間區間內(periodId)TF/DF-->
	 		  <td>
				  <s:submit onclick="showTFDF()" cssClass="comfirm" value="TF/DF結果[區間]" theme="simple"/>
			  </td>
			  <td>
				  <s:submit onclick="calTFDF()" cssClass="comfirm" value="計算[區間]TF/DF" theme="simple"/>
			  </td>
	      </c:when>
	      <c:otherwise>
	  		  <!-- 不顯示,留空白 -->
	      </c:otherwise>
	    </c:choose>
		
	</tr>
</table>
<c:choose>
   <c:when test="${empty topicSerial && empty periodId}">
     <div style="background-color:#FFE6D9;padding:10px;margin-bottom:5px;">
  		<span style="color:#2894FF;">請先搜尋，再繼續 (系统需先知道您所要編輯的資料)。</span><br><br>
  		<span style="color:#2894FF;">请先搜寻，再继续动作 (系统必须先取得您所要编辑的资讯来源)。</span><br><br>
  		<span style="color:#2894FF;">Please Query first (We have to know what do you want to modify firstly).</span><br>
  		
  	 </div>
   </c:when>
   <c:when test="${not empty topicSerial && not empty periodId}">
     <div id="all">
  		<div id="mainFunction">
			<left>
				<table id="grid"></table>
				<div id="gridPager"></div>
			</left>
	         <br>
	        <left>
				<table id="domainKnowhowTopic_grid"></table>
				<div id="domainKnowhowTopic_gridPager"></div>
			</left>
			 <br>
	        <left>
				<table id="statistic_dkh_Grid"></table>
				<div id="statistic_dkh_gridPager"></div>
			</left><br>
			<!-- high chart -->
	    	<script src="https://code.highcharts.com/highcharts.js"></script>
	        <script src="https://code.highcharts.com/modules/exporting.js"></script>
	        <div id="container" style="min-width: 510px; height: 360px; margin: 0 auto"></div><br>
	         
		</div>
     </div>	
   </c:when>
   <c:otherwise>
  		<div id="all">
  		<div id="mainFunction">
			<left>
				<table id="grid"></table>
				<div id="gridPager"></div>
			</left>
	         <br>
			<left>
				<table id="domainKnowhowTopic_grid"></table>
				<div id="domainKnowhowTopic_gridPager"></div>
			</left>
			 <br>
	        <left>
				<table id="statistic_all_dkh_Grid"></table>
				<div id="statistic_all_dkh_gridPager"></div>
			</left><br>
			<!-- high chart -->
	    	<script src="https://code.highcharts.com/highcharts.js"></script>
	        <script src="https://code.highcharts.com/modules/exporting.js"></script>
	        <div id="container" style="min-width: 510px; height: 360px; margin: 0 auto"></div><br>
		</div>
        </div>	  
   </c:otherwise>
</c:choose>

</form>

</div>
</div>
</div>
</body>
</html>
 