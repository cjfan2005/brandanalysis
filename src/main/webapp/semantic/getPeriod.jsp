<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>

<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Brand Analysis- Time Period, Removed Keywords, Phrases, In Out Record </title>
<style>
label.error{display:inline;margin-left: 10px; color: red;}
TD{padding-left:10px;} 
#wrap { display:table; }
#cell { display:table-cell; vertical-align:middle; }
</style>
<style>
html,body{ height:100%; margin:0; padding:0} 
.mask{height:100%; width:100%; position:fixed; _position:absolute; top:0; z-index:1000; } 
.opacity{ opacity:0.3; filter: alpha(opacity=30); background-color:#000000; }
.box {
        display: table-cell;
        vertical-align:middle;
        text-align:center;
        *display: block;
        *font-size: 190px;
        *font-family:Arial;
        
        width:500px;
        height:500px;
        border: 2px solid #eee;
}
.box label {
        vertical-align:middle;
        font-size:68px;
        color: white;
}
</style>

<script type="text/javascript">
//AJAX for Query Category
$(function(){
	getCategory();
});

function getCategory() {
	var CategoryNameObj = document.getElementById("categoryName");
	CategoryNameObj.style.display = "none";
	var tarObj = document.getElementById("categoryName");
	var selectObj = document.createElement("SELECT");
	selectObj.id = "cat-Select";
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicCategoryAction.do",
			dataType: "json",
			success: function(data) {
				var index = 0;
				
				for(var i=0; i<data.topicCategory.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					
					optionObj.innerHTML = data.topicCategory[i].name;
					optionObj.value = data.topicCategory[i].categorySerial +"@"+ data.topicCategory[i].secondName;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#categorySerial").val( arr[0] );
		
		$("#categoryName").val( this.options[this.selectedIndex].text);
		
		//call to getSubCategory();
		getSubCategory(arr[0]);
	};
}

function getSubCategory(categorySerial) {
	var selectObj = "";
	var SubCategoryNameObj = document.getElementById("subCategoryName");
	SubCategoryNameObj.style.display = "none";
	var tarObj = document.getElementById("subCategoryName");
	
	if(document.getElementById("subCat-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "subCat-Select";
	} else {
		//移除原本的options
		$('#subCat-Select').find('option').remove().end();
		$('#topic-Select').find('option').remove().end();
		document.getElementById("subCategorySerial").value = "";
		document.getElementById("topicSerial").value = "";
		selectObj = document.getElementById("subCat-Select");
	}
	
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetSubcategory2Action.do?categorySerial="+ categorySerial,
			dataType: "json",
			success: function(data) {
				var index = 0;
				var optionObj = "";
				
				for(var i=0; i<data.subCategory.length; i++) {
					optionObj = document.createElement("OPTION");
					
					optionObj.innerHTML = data.subCategory[i].subName;
					optionObj.value = data.subCategory[i].subCategorySerial +"@"+ data.subCategory[i].categorySerial;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#subCategorySerial").val( arr[0] );
		
		$("#subName").val( this.options[this.selectedIndex].text);
		getTopic() ;
	};
	
	insertAfter(selectObj, tarObj);
}


function getTopic() {
	var catSerial = document.getElementById("categorySerial").value;
	var subCatSerial = document.getElementById("subCategorySerial").value;
	var selectObj = "";
	
	var TopicObj = document.getElementById("topic");
	TopicObj.style.display = "none";
	var tarObj = document.getElementById("topic");
	selectObj = document.createElement("SELECT");
	
	if(document.getElementById("topic-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "topic-Select";
	} else {
		//移除原本的options
		$('#topic-Select').find('option').remove().end();
		document.getElementById("topicSerial").value = "";
		selectObj = document.getElementById("topic-Select");
	}
	
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicJSONAction.do?categorySerial="+catSerial+"&subCategorySerial="+subCatSerial,
			dataType: "json",
			success: function(data) {
				var index = 0;
				
				for(var i=0; i<data.topicJSONStr.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					optionObj.innerHTML = data.topicJSONStr[i].topic;
					optionObj.value = data.topicJSONStr[i].topicSerial +"@"+ data.topicJSONStr[i].updateDate;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#topicSerial").val( arr[0] );
		
		$("#topic").val( this.options[this.selectedIndex].text);
	};
}

function insertAfter(newEl, targetEl) {
    var parentEl = targetEl.parentNode;
    if(parentEl.lastChild == targetEl) {
        parentEl.appendChild(newEl);
    }else {
        parentEl.insertBefore(newEl,targetEl.nextSibling);
    }            
}

function clickme(conditions) {
	$("#form1").validate({});//check all columns.
	$("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
 	$("#allCover").css("display","block");
 	
 	var splitStr = conditions.split("/");
 	//alert(splitStr[0] +"," +splitStr[1]+","+ splitStr[2]);
 	
	var url = '${pageContext.request.contextPath}/semantic/InitSentenceDetailAction.do?articleId='+splitStr[0]+'&title='+ splitStr[1] +'&topic='+splitStr[2];
	popup(url);
	//$('#form1').attr('action', '${pageContext.request.contextPath}/ft/SentenceDetailAction.do?detailIds=' + detailIds);
	//$('#form1').submit();
	if (window.focus) {newwin.focus()}
	$("#allCover").css("display","none");
}

function popup(url) {
	params  = 'width='+(screen.width/2);
	params += ', height='+(screen.height/2)+100;
	params += ', top=0, left=0'
	params += ', fullscreen=0';
	params += ', scrollbars=yes';
	params += ', location=no';
	
	newwin=window.open(url,'popUpWindow', params);
	if (window.focus) {newwin.focus()}
	$("#allCover").css("display","none");
	return false;
}

function showTFDF() {
	$("#form1").validate({});//check all columns.
	$("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
 	$("#allCover").css("display","block");
	
 	
 	//var ts = document.getElementById("topicSerial").value;
	//$('#form1').attr('action', '${pageContext.request.contextPath}/semantic/InitTfDfCalculatorAction.do?topicSerial='+ts[0]);
	
    $('#form1').attr('action', '${pageContext.request.contextPath}/semantic/InitGetTfDfAction.do');
	$('#form1').submit();
	
	//if (window.focus) {newwin.focus()}
	//$("#allCover").css("display","none");
}

function calTFDF() {
	$("#form1").validate({});//check all columns.
	$("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
 	$("#allCover").css("display","block");
	
 	
 	//var ts = document.getElementById("topicSerial").value;
	//$('#form1').attr('action', '${pageContext.request.contextPath}/semantic/InitTfDfCalculatorAction.do?topicSerial='+ts[0]);
	
    $('#form1').attr('action', '${pageContext.request.contextPath}/semantic/TfDfCalculatorAction.do');
	$('#form1').submit();
	
	//if (window.focus) {newwin.focus()}
	//$("#allCover").css("display","none");
}

//jqGrid Period start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
$(document).ready(function() {
	$("#period_grid").jqGrid({
		url:encodeURI('${pageContext.request.contextPath}/semantic/GetPeriodAction.do?topicSerial=${topicSerial}'),
		datatype: "JSON",
		height: "500", 
		width:"860",
		colNames:['Topic','區間名稱','起始','結束', 'Editor','TopicSerial', 'periodId',],
		         
        colModel:[
                  {name:'topic', index:'topic',width:150, align:'center',
                	  editable: true,frozen: false,
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{defaultValue:'新增時,不必填寫', size:10, maxlength:10}
                  },
                  
				  {name:'periodName', index:'periodName', width:150, align:'center',frozen: false,
                	  editable: true, editoptions:{size:50, maxlength:50},
                	  edittype:"text", editrules:{required:true} 
				  },
				   
				  {name:'startDate',index:'startDate', align:'center',
					  editable:true,edittype:'text',editable:true,
					  editoptions:{size:10,maxlength:10,dataInit:getDate},
					  editrules:{date:true,required:true},
					  formatter:'date',
					  formatoptions:{srcformat:'Y-m-d',newformat:'Y-m-d'}
				  },
				  
				  {name:'endDate',index:'endDate', align:'center',
					  editable:true,edittype:'text',editable:true, 
					  editoptions:{size:10,maxlength:10,dataInit:getDate},
					  editrules:{date:true,required:true},
					  formatter:'date',
					  formatoptions:{srcformat:'Y-m-d',newformat:'Y-m-d'}
				  },
				  
				  {name:'editor', index:'editor', width:80, align:'center'},
                  {name:'topicSerial', index:'topicSerial', width:150, align:'center',
                	  editable: false, 
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{ defaultValue:'新增時,不必填寫', size:10, maxlength:10}
                  },
                  {name:'periodId', index:'periodId', width:180, align:'center',
                	  editable: true, 
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{defaultValue:'新增時,不必填寫', size:15, maxlength:15}
                  },
        ],
        shrinkToFit:false,
        viewrecords: true,
        gridview: true, 
        rownumbers: true,
        rowNum: 20, 
        rowList: [20,30,50], 
        pgbuttons: true,
        pginput: true,
        pager: "#period_gridPager",
        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
        caption: "時間區間列表 Period List ",
        toppager: true,
        loadComplete: function(data){
        	$("#ulList1").css("zIndex", 1000);
        	$("#ulList2").css("zIndex", 1000);
        	$("#ulList3").css("zIndex", 1000);
        	$("#ulList4").css("zIndex", 1000);
        }
    });
	
	jQuery("#period_grid").jqGrid('navGrid','#period_gridPager', 
		{edit:true, add:true, del:true, search:false, refresh:false, cloneToTop:true,
		     edittext: "Modify", edittitle: "Modify", 
		     addtext: "Add", addtitle: "Add", 
		     deltext: "Delete", deltitle: "Delete",
		     searchtext: "Search", searchtitle: "Search", 
		     refreshtext: "Refresh", refreshtitle: "Refresh"
		},
		
		// Modify
		{ url: '${pageContext.request.contextPath}/semantic/ModifyPeriodAction.do?act=modify&topicSerial=${topicSerial}',
		  height:'78%',
		  width:'100%',
		  //viewPagerButtons: false, //remove previous/next arrow
          reloadAfterSubmit: true,
          onclickSubmit : function(params, posdata) { 
           	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
           	  $("#allCover").css("display","block");
          },
          afterSubmit : function(response, postdata) {
        	  $("#allCover").css("display","none");
          	  return true;
          },
          closeAfterEdit: true,
          recreateForm: true,
          beforeInitData: function () {
        	  $("#period_grid").jqGrid('setColProp','topic',{editoptions:{readonly:true,hidden:false}});
        	  $("#period_grid").jqGrid('setColProp','periodId',{editoptions:{readonly:true,hidden:true}});//隱藏,java還是抓的到!
        	  $("#period_grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true,hidden:true}});
          },
		}, 
		
		// add options
		{ url: '${pageContext.request.contextPath}/semantic/ModifyPeriodAction.do?act=add&topicSerial=${topicSerial}',
		  height:'78%',
		  width:'100%',
	      closeAfterAdd: true,
	      reloadAfterSubmit: true,
	      recreateForm: true,
          beforeInitData: function () {
        	  $("#period_grid").jqGrid('setColProp','topic',{editoptions:{readonly:true,hidden:false}});
        	  $("#period_grid").jqGrid('setColProp','periodId',{editoptions:{readonly:true}});
        	  $("#period_grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true}});
          }
		},  
		
		// del options
		{ url:'${pageContext.request.contextPath}/semantic/ModifyPeriodAction.do?act=delete',
		  delData: {
			  PeriodId: function() {
			      var selectedId = $("#period_grid").jqGrid('getGridParam', 'selrow');
                  var value = $("#period_grid").jqGrid('getCell', selectedId, 'periodId');
                  //alert(value);
                  return value;
		      }
		  },
		  reloadAfterSubmit: false
		},
		
		// search options
		{ Find: "搜尋",  
		  closeAfterSearch: true  
		},
		//refresh optioins
		{ url: '${pageContext.request.contextPath}/semantic/JqGridFindAllStaff.do?UserType=STAFF'
		} 
	)
	jQuery("#period_grid").jqGrid('setFrozenColumns');
	function getDate(el) {
		 $(el).datepicker({dateFormat:"yy-mm-dd"});
		}
});
//jqgrid Period end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


//jqGrid Phrase start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
$(document).ready(function() {
	$("#phrase_grid").jqGrid({
		url:encodeURI('${pageContext.request.contextPath}/semantic/GetPhraseAction.do?topicSerial=${topicSerial}'),
		datatype: "JSON",
		height: "500", 
		width:"860",
		colNames:['Topic','Phrase','詞性', 'Update', 'Editor','TopicSerial', 'phraseId',],
		         
        colModel:[
                  {name:'topic', index:'topic',width:150, align:'center',
                	  editable: true,frozen: false,
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{defaultValue:'新增時,不必填寫', size:10, maxlength:10}
                  },
                  
				  {name:'phrase', index:'phrase', width:150, align:'center',frozen: false,
                	  editable: true, editoptions:{size:50, maxlength:50},
                	  edittype:"text", editrules:{required:true} 
				  },
				  
				  {name:'partOfSpeech', index:'partOfSpeech', width:150, align:'center',
                	  editable: true, editoptions:{size:50, maxlength:50},
                	  edittype:'select', editrules:{required:true},
                	  editoptions:{value:{YA:'A', ADV:'ADV', N:'N', DET:'DET', Vi:'Vi', Vt:'Vt'}}
				  },
				  
				  {name:'updateDate',index:'updateDate', align:'center'},
				  
				  {name:'editor', index:'editor', width:80, align:'center'},
                  {name:'topicSerial', index:'topicSerial', width:150, align:'center',
                	  editable: false, 
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{ defaultValue:'新增時,不必填寫', size:10, maxlength:10}
                  },
                  {name:'phraseId', index:'phraseId', width:180, align:'center',
                	  editable: true, 
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{defaultValue:'新增時,不必填寫', size:15, maxlength:15}
                  },
        ],
        shrinkToFit:false,
        viewrecords: true,
        gridview: true, 
        rownumbers: true,
        rowNum: 20, 
        rowList: [20,30,50], 
        pgbuttons: true,
        pginput: true,
        pager: "#phrase_gridPager",
        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
        caption: "長詞片語列表 (欲保留的特殊字詞) Phrase List ",
        toppager: true,
        loadComplete: function(data){
        }
    });
	
	jQuery("#phrase_grid").jqGrid('navGrid','#phrase_gridPager', 
		{edit:true, add:true, del:true, search:false, refresh:false, cloneToTop:true,
		     edittext: "Modify", edittitle: "Modify", 
		     addtext: "Add", addtitle: "Add", 
		     deltext: "Delete", deltitle: "Delete",
		     searchtext: "Search", searchtitle: "Search", 
		     refreshtext: "Refresh", refreshtitle: "Refresh"
		},
		
		// Modify
		{ url: '${pageContext.request.contextPath}/semantic/ModifyPhraseAction.do?act=modify&topicSerial=${topicSerial}',
		  height:'78%',
		  width:'100%',
		  //viewPagerButtons: false, //remove previous/next arrow
          reloadAfterSubmit: true,
          onclickSubmit : function(params, posdata) { 
           	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
           	  $("#allCover").css("display","block");
          },
          afterSubmit : function(response, postdata) {
        	  $("#allCover").css("display","none");
          	  return true;
          },
          closeAfterEdit: true,
          recreateForm: true,
          beforeInitData: function () {
        	  $("#period_grid").jqGrid('setColProp','topic',{editoptions:{readonly:true,hidden:false}});
        	  $("#period_grid").jqGrid('setColProp','phraseId',{editoptions:{readonly:true,hidden:true}});//隱藏,java還是抓的到!
        	  $("#period_grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true,hidden:true}});
          },
		}, 
		
		// add options
		{ url: '${pageContext.request.contextPath}/semantic/ModifyPhraseAction.do?act=add&topicSerial=${topicSerial}',
		  height:'78%',
		  width:'100%',
	      closeAfterAdd: true,
	      reloadAfterSubmit: true,
	      recreateForm: true,
          beforeInitData: function () {
        	  $("#period_grid").jqGrid('setColProp','topic',{editoptions:{readonly:true,hidden:false}});
        	  $("#period_grid").jqGrid('setColProp','phraseId',{editoptions:{readonly:true}});
        	  $("#period_grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true}});
          }
		},  
		
		// del options
		{ url:'${pageContext.request.contextPath}/semantic/ModifyPhraseAction.do?act=delete',
		  delData: {
			  PhraseId: function() {
			      var selectedId = $("#phrase_grid").jqGrid('getGridParam', 'selrow');
                  var value = $("#phrase_grid").jqGrid('getCell', selectedId, 'phraseId');
                  //alert(value);
                  return value;
		      }
		  },
		  reloadAfterSubmit: false
		},
		
		// search options
		{ Find: "搜尋",  
		  closeAfterSearch: true  
		},
		//refresh optioins
		{ url: '${pageContext.request.contextPath}/semantic/JqGridFindAllStaff.do?UserType=STAFF'
		} 
	)
	
});
//jqgrid Phrase end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//jqGrid sa_removed_keyword_map start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
$(document).ready(function() {
	$("#rmKeyword_grid").jqGrid({
		url:encodeURI('${pageContext.request.contextPath}/semantic/GetRmKeywordAction.do?topicSerial=${topicSerial}'),
		datatype: "JSON",
		height: "500", 
		width:"860",
		colNames:['Topic','關鍵字','Editor', 'Update','TopicSerial', 'rmId'],
		         
        colModel:[
                  {name:'topic', index:'topic',width:150, align:'center',
                	  editable: true,frozen: false,
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{defaultValue:'新增時,不必填寫', size:10, maxlength:10}
                  },
                  
				  {name:'removeItem', index:'removeItem', width:150, align:'center',frozen: false,
                	  editable: true, editoptions:{size:50, maxlength:50},
                	  edittype:"text", editrules:{required:true} 
				  },
				  
				  {name:'editor', index:'editor', width:120, align:'center'},
				  
				  {name:'updateDate',index:'updateDate', align:'center'},
				  
                  {name:'topicSerial', index:'topicSerial', width:150, align:'center',
                	  editable: false, 
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{ defaultValue:'新增時,不必填寫', size:10, maxlength:10}
                  },
                  {name:'removedId', index:'removedId', width:150, align:'center',
                	  editable: true, 
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{defaultValue:'新增時,不必填寫', size:15, maxlength:15}
                  },
        ],
        shrinkToFit:false,
        viewrecords: true,
        gridview: true, 
        rownumbers: true,
        rowNum: 20, 
        rowList: [20,30,50], 
        pgbuttons: true,
        pginput: true,
        pager: "#rmKeyword_gridPager",
        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
        caption: "已移除字詞列表 (不在TF/DF字詞出現) Removed Keywords List ",
        toppager: true,
        loadComplete: function(data){
        }
    });
	
	jQuery("#rmKeyword_grid").jqGrid('navGrid','#rmKeyword_gridPager', 
		{edit:true, add:true, del:true, search:false, refresh:false, cloneToTop:true,
		     edittext: "Modify", edittitle: "Modify", 
		     addtext: "Add", addtitle: "Add", 
		     deltext: "Delete", deltitle: "Delete",
		     searchtext: "Search", searchtitle: "Search", 
		     refreshtext: "Refresh", refreshtitle: "Refresh"
		},
		
		// Modify
		{ url: '${pageContext.request.contextPath}/semantic/ModifyRmKeywordAction.do?act=modify&topicSerial=${topicSerial}',
		  height:'100%',
		  width:'100%',
		  //viewPagerButtons: false, //remove previous/next arrow
          reloadAfterSubmit: true,
          onclickSubmit : function(params, posdata) { 
           	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
           	  $("#allCover").css("display","block");
          },
          afterSubmit : function(response, postdata) {
        	  $("#allCover").css("display","none");
          	  return true;
          },
          closeAfterEdit: true,
          recreateForm: true,
          beforeInitData: function () {
        	  $("#rmKeyword_grid").jqGrid('setColProp','topic',{editoptions:{readonly:true,hidden:false}});
        	  $("#rmKeyword_grid").jqGrid('setColProp','removedId',{editoptions:{readonly:true,hidden:true}});//隱藏,java還是抓的到!
        	  $("#rmKeyword_grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true,hidden:true}});
          },
		}, 
		
		// add options
		{ url: '${pageContext.request.contextPath}/semantic/ModifyRmKeywordAction.do?act=add&topicSerial=${topicSerial}',
		  height:'78%',
		  width:'100%',
	      closeAfterAdd: true,
	      reloadAfterSubmit: true,
	      recreateForm: true,
          beforeInitData: function () {
        	  $("#rmKeyword_grid").jqGrid('setColProp','topic',{editoptions:{readonly:true,hidden:false}});
        	  $("#rmKeyword_grid").jqGrid('setColProp','removedId',{editoptions:{readonly:true}});
        	  $("#rmKeyword_grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true}});
          }
		},  
		
		// del options
		{ url:'${pageContext.request.contextPath}/semantic/ModifyRmKeywordAction.do?act=delete',
		  delData: {
			  RemovedId: function() {
			      var selectedId = $("#rmKeyword_grid").jqGrid('getGridParam', 'selrow');
                  var value = $("#rmKeyword_grid").jqGrid('getCell', selectedId, 'removedId');
                  //alert(value);
                  return value;
		      }
		  },
		  reloadAfterSubmit: false
		},
		
		// search options
		{ Find: "搜尋",  
		  closeAfterSearch: true  
		},
		//refresh optioins
		{ url: '${pageContext.request.contextPath}/semantic/ModifyRmKeywordAction.do?'
		} 
	)
	
});
//jqgrid sa_removed_keyword_map end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//jqGrid InOutRecord start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
$(document).ready(function() {
	$("#inOut_grid").jqGrid({
		url:encodeURI('${pageContext.request.contextPath}/semantic/GetInOutRecordAction.do?topicSerial=${topicSerial}'),
		datatype: "JSON",
		height: "500", 
		width:"860",
		colNames:['Topic','PeriodName','OrderQuantity', 'ShipQuantity', 'StartDate', 'EndDate', 'Editor','TopicSerial', 'inOutId', 'PeriodId'],
		         
        colModel:[
                  {name:'topic', index:'topic',width:150, align:'center',
                	  editable: true,frozen: false,
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{defaultValue:'新增時,不必填寫', size:10, maxlength:10}
                  },
                  
				  {name:'periodName', index:'periodName', width:150, align:'center',
                	  editable: true, editrules:{required: true}, editoptions:{ readonly: "readonly", hidden:false },
                	  edittype:'select', hidden:false,
                	  editoptions:{value:getPeriodAjax()},
				  },
				  
				  {name:'inQuantity', index:'inQuantity', width:150, align:'center',
					  editable: true, 
                	  edittype:"integer", editrules:{required:true, integer:true}, 
                	  editoptions:{size:15, maxlength:15}
				  },
				  
				  {name:'outQuantity',index:'outQuantity', align:'center',
					  editable: true, 
                	  edittype:"integer", editrules:{required:true, integer:true},
                	  editoptions:{size:15, maxlength:15}
				  },
				  
				  
				  {name:'startDate', index:'startDate', width:120, align:'center'},
				  {name:'endDate', index:'endDate', width:120, align:'center'},
				  {name:'editor', index:'editor', width:80, align:'center'},
				  
                  {name:'topicSerial', index:'topicSerial', width:150, align:'center',
                	  editable: false, 
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{ defaultValue:'新增時,不必填寫', size:15, maxlength:10}
                  },
                  {name:'inOutId', index:'inOutId', width:180, align:'center',
                	  editable: true, 
                	  edittype:"text", editrules:{required:true},
                	  editoptions:{defaultValue:'新增時,不必填寫', size:15, maxlength:15}
                  },
                  {name:'periodId', index:'periodId', width:180, align:'center',
            	     editable: true, 
            	     edittype:"text", editrules:{required:true},
            	     editoptions:{defaultValue:'新增時,不必填寫', size:15, maxlength:15}
                  },
                  
        ],
        shrinkToFit:false,
        viewrecords: true,
        gridview: true, 
        rownumbers: true,
        rowNum: 20, 
        rowList: [20,30,50], 
        pgbuttons: true,
        pginput: true,
        pager: "#inOut_gridPager",
        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
        caption: "訂單量 與 出貨量 ( IN ＆ OUT List )",
        toppager: true,
        loadComplete: function(data){
        	
        }
    });
	
	jQuery("#inOut_grid").jqGrid('navGrid','#inOut_gridPager', 
		{edit:true, add:true, del:true, search:false, refresh:false, cloneToTop:true,
		     edittext: "Modify", edittitle: "Modify", 
		     addtext: "Add", addtitle: "Add", 
		     deltext: "Delete", deltitle: "Delete",
		     searchtext: "Search", searchtitle: "Search", 
		     refreshtext: "Refresh", refreshtitle: "Refresh"
		},
		
		// Modify
		{ url: '${pageContext.request.contextPath}/semantic/ModifyInOutRecordAction.do?act=modify&topicSerial=${topicSerial}',
		  height:'100%',
		  width:'120%',
		  //viewPagerButtons: false, //remove previous/next arrow
          reloadAfterSubmit: true,
          onclickSubmit : function(params, posdata) { 
           	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
           	  $("#allCover").css("display","block");
          },
          afterSubmit : function(response, postdata) {
        	  $("#allCover").css("display","none");
          	  return true;
          },
          closeAfterEdit: true,
          recreateForm: true,
          beforeInitData: function () {
        	  $("#inOut_grid").jqGrid('setColProp','periodId',{editoptions:{readonly:true,hidden:false}});
        	  $("#inOut_grid").jqGrid('setColProp','inOutId',{editoptions:{readonly:true,hidden:false}});//隱藏,java還是抓的到!
        	  $("#inOut_grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true,hidden:false}});
          },
		}, 
		
		// add options
		{ url: '${pageContext.request.contextPath}/semantic/ModifyInOutRecordAction.do?act=add&topicSerial=${topicSerial}',
		  height:'100%',
		  width:'120%',
	      closeAfterAdd: true,
	      reloadAfterSubmit: true,
	      recreateForm: true,
          beforeInitData: function () {
        	  $("#inOut_grid").jqGrid('setColProp','periodId',{editoptions:{readonly:true,hidden:false}});
        	  $("#inOut_grid").jqGrid('setColProp','inOutId',{editoptions:{readonly:true,hidden:false}});//隱藏,java還是抓的到!
        	  $("#inOut_grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true,hidden:false}});
          }
		},  
		
		// del options
		{ url:'${pageContext.request.contextPath}/semantic/ModifyInOutRecordAction.do?act=delete',
		  delData: {
			  inOutId: function() {
			      var selectedId = $("#inOut_grid").jqGrid('getGridParam', 'selrow');
                  var value = $("#inOut_grid").jqGrid('getCell', selectedId, 'inOutId');
                  //alert(value);
                  return value;
		      }
		  },
		  reloadAfterSubmit: false
		},
		
		// search options
		{ Find: "搜尋",  
		  closeAfterSearch: true  
		},
		//refresh optioins
		{ url: '${pageContext.request.contextPath}/semantic/ModifyInOutRecordAction.do?UserType=STAFF'
		} 
	)
	
});
//jqgrid InOutRecord end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//AJAX for jqGrid .
function getPeriodAjax() {
	var returnStr = "";
	$.ajax({
		async: false, 
		type: 'POST',
		url: "${pageContext.request.contextPath}/general/GetPeriodJSONAction.do?topicSerial=${topicSerial}",
		dataType: "json",
		success: function(data) {
			for(var i=0; i<data.periodJSONStr.length; i++) {
				  returnStr += data.periodJSONStr[i].periodId + ":" + data.periodJSONStr[i].periodName+";";
			}
		}
    });
	//alert(returnStr);
	return returnStr; 
}

</script>
  
</head>
<body>
<div class="mask opacity" id="allCover" style="display:none;">
	<div class="box" id="mDiv">
       <label>Please wait!!</label>
	</div>
</div>

<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/BrandAnalysis_logo.png" alt="BrandAnalysis" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： Time Period, Removed Keywords, Phrases, In Out Record Maintain</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:if test="hasActionMessages()">

		<div class="successMsg" align="center" style="margin:0 auto;">
		   <s:actionmessage/>
		</div>
</s:if>
<s:if test="hasActionErrors()">
		<div class="errorMsg" align="center" style="margin:0 auto;">
		   <s:actionerror/>
		</div>
</s:if>


<form action="" method="post" name="form1" id="form1">

<table width="480" >
	<tr>
    	<th>Category：</th>
    	<td>
            
			<s:hidden theme="simple" name="categoryName" id="categoryName" value="%{categoryName}" ReadOnly="true"/>
			<s:hidden theme="simple" name="categorySerial" id="categorySerial" value="%{categorySerial}" ReadOnly="true" cssClass="required form-control"/>
		</td>
	
		<th>SubCat.：</th>
    	<td>
            
			<s:hidden theme="simple" name="subCategoryName" id="subCategoryName" value="%{subCategoryName}" ReadOnly="true"/>
			<s:hidden theme="simple" name="subCategorySerial" id="subCategorySerial" value="%{subCategorySerial}" ReadOnly="true" cssClass="required form-control"/>
		</td>
	
		<th>Topic：</th>
		<td>
		    <s:textfield theme="simple" name="topic" id="topic" value="%{topic}" ReadOnly="true" />
			<s:hidden theme="simple" name="topicSerial" id="topicSerial" value="%{topicSerial}" ReadOnly="true" cssClass="required form-control"/>
		</td>
		
		<td>
		    <s:submit onclick="searchForm()" cssClass="comfirm" value="Query" theme="simple"/>
		</td>
		
	</tr>
	
</table>
<c:choose>
   <c:when test="${empty topicSerial}">
     <div style="background-color:#FFE6D9;padding:10px;margin-bottom:5px;">
  		<span style="color:#2894FF;">請先搜尋，再繼續 (系统需先知道您所要編輯的資料)。</span><br><br>
  		<span style="color:#2894FF;">请先搜寻，再继续动作 (系统必须先取得您所要编辑的资讯来源)。</span><br><br>
  		<span style="color:#2894FF;">Please Query first (We have to know what do you want to modify firstly).</span><br>
  		
  	 </div>
   </c:when>
   <c:otherwise>
        
  		<div id="all">
  		<div id="mainFunction">
			<left>
				<table id="period_grid"></table>
				<div id="period_gridPager"></div>
			</left>
			<br>
			<left>
				<table id="phrase_grid"></table>
				<div id="phrase_gridPager"></div>
			</left>
			<br>
			<left>
				<table id="rmKeyword_grid"></table>
				<div id="rmKeyword_gridPager"></div>
			</left>
			<br>
			<left>
				<table id="inOut_grid"></table>
				<div id="inOut_gridPager"></div>
			</left>
			<br>
		</div>
  </div>	  
   </c:otherwise>
</c:choose>

</form>

</div>
</div>
</div>
</body>
</html>
 