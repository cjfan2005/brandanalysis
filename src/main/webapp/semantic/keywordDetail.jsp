<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>

<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Brand Analysis- Keyword </title>
<style>
html,body{ height:100%; margin:0; padding:0} 
.mask{height:100%; width:100%; position:fixed; _position:absolute; top:0; z-index:1000; } 
.opacity{ opacity:0.3; filter: alpha(opacity=30); background-color:#000000; }
.box {
        display: table-cell;
        vertical-align:middle;
        text-align:center;
        *display: block;
        *font-size: 190px;
        *font-family:Arial;
        
        width:500px;
        height:500px;
        border: 2px solid #eee;
}
.box label {
        vertical-align:middle;
        font-size:68px;
        color: white;
}
</style>

<script type="text/javascript">
//AJAX for Query Category
$(function(){

});

//jqGrid
	$(document).ready(function() {
		$("#grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/semantic/CkipKeywordDetailAction.do?articleIdGroup=${articleIdGroup}&periodId=${periodId}&keyword=${keyword}'),
			datatype: "JSON",
			height: "100%", 
			width:"800",
			colNames:[ 'Content','NTUSD', 'HowNet', 'SentId', 'KW_id','ArticleId'],
			         
	        colModel:[
					  {name:'content', index:'content',width:350, align:'left'},
					  {name:'intentionNTUSD', index:'intentionNTUSD', width:80, align:'center'},
					  {name:'howNetScore', index:'howNetScore',width:80, align:'center'},
					  {name:'sentenceId', index:'sentenceId', width:60, align:'center'},
					  {name:'sentenceId', index:'sentenceId', width:60, align:'center'},
	                  {name:'articleId', index:'articleId', width:60, align:'center'},
	        ],
	        shrinkToFit:false,
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: true,
	        rowNum: 1000, 
	        //rowList: [200,400], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "與keyword關連的句子列表",
	        toppager: true,
	        loadComplete: function(data){
	        	$("#ulList1").css("zIndex", 1000);
	        	$("#ulList2").css("zIndex", 1000);
	        	$("#ulList3").css("zIndex", 1000);
	        	$("#ulList4").css("zIndex", 1000);
	        }
	    });
		
		jQuery("#grid").jqGrid('navGrid','#gridPager', 
			{edit:false, add:false, del:false, search:false, refresh:false, cloneToTop:true,
			     edittext: "Modify", edittitle: "Modify", 
			     addtext: "Add", addtitle: "Add", 
			     deltext: "Delete", deltitle: "Delete",
			     searchtext: "Search", searchtitle: "Search", 
			     refreshtext: "Refresh", refreshtitle: "Refresh"
			},
			
			// Modify
			{ url: '${pageContext.request.contextPath}/semantic/ModifyQueryTopicAction.do?act=modify',
			  height:'78%',
			  width:'100%',
			  //viewPagerButtons: false, //remove previous/next arrow
              reloadAfterSubmit: true,
              onclickSubmit : function(params, posdata) { 
               	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
               	  $("#allCover").css("display","block");
              },
              afterSubmit : function(response, postdata) {
            	  $("#allCover").css("display","none");
              	  return true;
              },
              closeAfterEdit: true,
              recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true,hidden:true}});
            	  $("#grid").jqGrid('setColProp','topicCategory',{editoptions:{readonly:true,hidden:true}});
              },
			}, 
			
			// add options
			{ url: '${pageContext.request.contextPath}/semantic/ModifyQueryTopicAction.do?act=add',
			  height:'78%',
			  width:'100%',
		      closeAfterAdd: true,
		      reloadAfterSubmit: true,
		      recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true}});
              }
			},  
			
			// del options
			{ url:'${pageContext.request.contextPath}/semantic/.do',
			  delData: {
				  keywi: function() {
				      var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
                      var value = $("#grid").jqGrid('getCell', selectedId, 'topicSerial');
                      //alert(value);
                      return value;
			      }
			  },
			  reloadAfterSubmit: false
			},
			
			// search options
			{ Find: "搜尋",  
			  closeAfterSearch: true  
			},
			//refresh optioins
			{ url: '${pageContext.request.contextPath}/semantic/GetDomainKnowhowByIdJsonAction.do'
			} 
		)
		jQuery("#grid").jqGrid('setFrozenColumns');
		
	});

</script>

</head>
<body>
<div class="mask opacity" id="allCover" style="display:none;">
	<div class="box" id="mDiv">
       <label>Please wait!!</label>
	</div>
</div>

<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/BrandAnalysis_logo.png" alt="BrandAnalysis" border="0" /></a></div>

<br><br>
<div class="path">
PATH：Article List > TF/DF >Keyword Detail</div>
<div class="body">
<div class="body">

<div class="dialog">


<form action="" method="post" name="form1" id="form1">

<table width="800" >
    <tr>
        <s:hidden theme="simple" name="articleIdGroup" id="articleIdGroup" value="%{articleIdGroup}" ReadOnly="true"/>
        <th>Topic</th>
        <td><s:textfield theme="simple" name="topic" id="topic" value="%{topic}" ReadOnly="true"/></td>
        <th>Keyword</th>
        <td>
            <s:textfield theme="simple" name="keyword" id="keyword" value="%{keyword}" ReadOnly="true"/>
            <s:hidden theme="simple" name="keywordId" id="keywordId" value="%{keywordId}" ReadOnly="true" cssClass="required form-control"/>
        </td>
        
        <th>所屬議題</th>
        <td>
            <s:textfield theme="simple" name="domainKnowhowName" id="domainKnowhowName" value="%{domainKnowhowName}" ReadOnly="true"/>
        </td>
	     
	</tr>
</table>
  <div id="all">
  <div id="mainFunction">
	<left>
		<table id="grid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>
</form>

</div>
</div>
</div>
</body>
</html>
 