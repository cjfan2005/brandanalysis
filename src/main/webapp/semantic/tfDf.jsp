<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>

<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Brand Analysis- TF/DF </title>
<style>
html,body{ height:100%; margin:0; padding:0} 
.mask{height:100%; width:100%; position:fixed; _position:absolute; top:0; z-index:1000; } 
.opacity{ opacity:0.3; filter: alpha(opacity=30); background-color:#000000; }
.box {
        display: table-cell;
        vertical-align:middle;
        text-align:center;
        *display: block;
        *font-size: 190px;
        *font-family:Arial;
        
        width:500px;
        height:500px;
        border: 2px solid #eee;
}
.box label {
        vertical-align:middle;
        font-size:68px;
        color: white;
}
</style>

<script type="text/javascript">
//AJAX for Query Category
$(function(){
	
});

//jqGrid
	$(document).ready(function() {
		$("#grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/semantic/GetTfDfAction.do?topicSerial=${topicSerial}&periodId=${periodId}'),
			datatype: "JSON",
			height: "100%", 
			width:"800",
			colNames:[ 'Period','KeyWord', 'TF','DF','議題詞(domain knowhow)', '情緒分數','列出句子','計算','Update','editor','KW_id','ArticleIds', 'TopicSerial','periodId'],
			         
	        colModel:[
                      {name:'period', index:'period', width:150, align:'center'},
					  {name:'keyword', index:'keyword',width:80, align:'left',
						  editable: true, 
	                	  edittype:"text", editrules:{required:true},
	                	  editoptions:{defaultValue:'新增時,不必填寫', size:10, maxlength:10}
					  },
					  
					  
					  {name:'termFrequency', index:'termFrequency', width:40, align:'center'},
					  {name:'docFrequency', index:'docFrequency', width:40, align:'center'},
					  {name:'domainKnowhowName', index:'domainKnowhowName',width:120, align:'center',sortable:false,
						  editable: true, editrules:{required: true}, editoptions:{ readonly: "readonly", hidden:false },
	                	  edittype:'select', hidden:false,
	                	  editoptions:{value:getDomainKnowhowJson()},
					  },//DKT:domain knowhow topic.
					  
					  {name:'sentimentScore', index:'sentimentScore',width:50, align:'center'},//DKT:domain knowhow topic.
					  {   //Show Button
	                	  name:'action',index:'action',sortable:false, width:70, align:'center',sortable:false,
	                	  formatter:function (cellvalue, options, rowObject) {
	                		 //alert("aId:" + rowObject.articleId); //get row data
	                	     return '<input class="buttonBlue" type="button" onClick="clickme(\'' + rowObject.keyword + '/' + rowObject.articleIdGroup+ '/' + rowObject.keywordId + '/' + rowObject.domainKnowhowName + '/' + rowObject.periodId +  '\')" type="button" value="列出句子"/>'
	                	  }
	                  },
	                  {   //Show Button
	                	  name:'action',index:'action',sortable:false, width:100, align:'center',sortable:false,
	                	  formatter:function (cellvalue, options, rowObject) {
	                		 //alert("aId:" + rowObject.articleId); //get row data
	                	     return '<input class="buttonBlue" type="button" onClick="calHowNetNTUSD(\'' + rowObject.keyword + '/' + rowObject.articleIdGroup+ '/' + rowObject.keywordId + '/' + rowObject.periodId +   '\')" type="button" value="計算情緒分數"/>'
	                	  }
	                  },
	                  {name:'updateDate', index:'updateDate', width:120, align:'center'},
	                  {name:'editor', index:'editor', width:80, align:'center',sortable:false},
					  {name:'keywordId', index:'keywordId', width:50, align:'center',sortable:false,
	                	  editable: true, 
	                	  edittype:"text", editrules:{required:true},
	                	  editoptions:{defaultValue:'新增時,不必填寫', size:10, maxlength:10}
					  },
					  {name:'articleIdGroup', index:'articleIdGroup', width:50, align:'center',sortable:false},
	                  {name:'topicSerial', index:'topicSerial', width:50, align:'center',sortable:false},
	                  {name:'periodId', index:'periodId', width:50, align:'center',sortable:false}
	                  
	        ],
	        shrinkToFit:false,
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: true,
	        rowNum: 100, 
	        rowList: [100,200,500,800,1000], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "TF/DF List",
	        toppager: true,
	        loadComplete: function(data){
	        	$("#ulList1").css("zIndex", 1000);
	        	$("#ulList2").css("zIndex", 1000);
	        	$("#ulList3").css("zIndex", 1000);
	        	$("#ulList4").css("zIndex", 1000);
	        }
	    });
		
		jQuery("#grid").jqGrid('navGrid','#gridPager', 
			{edit:true, add:false, del:true, search:false, refresh:false, cloneToTop:true,
			     edittext: "議題詞修改 Modify Domain know How", edittitle: "議題詞修改/ Modify Domain know How", 
			     addtext: "Add", addtitle: "Add", 
			     deltext: "Delete", deltitle: "Delete",
			     searchtext: "Search", searchtitle: "Search", 
			     refreshtext: "Refresh", refreshtitle: "Refresh"
			},
			
			// Modify
			{ url: '${pageContext.request.contextPath}/semantic/UpdateKeywordFrequencyDomainKnowhowAction.do?',
			  height:'78%',
			  width:'100%',
			  //viewPagerButtons: false, //remove previous/next arrow
              reloadAfterSubmit: true,
              onclickSubmit : function(params, posdata) { 
               	  $("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
               	  $("#allCover").css("display","block");
              },
              afterSubmit : function(response, postdata) {
            	  $("#allCover").css("display","none");
              	  return true;
              },
              closeAfterEdit: true,
              recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','keyword',{editoptions:{readonly:true,hidden:false}});
            	  $("#grid").jqGrid('setColProp','keywordId',{editoptions:{readonly:true,hidden:true}});//隱藏,java還是抓的到!
            	  $("#grid").jqGrid('setColProp','name',{editoptions:{readonly:true,hidden:false}});
              },
			}, 
			
			// add options
			{ url: '${pageContext.request.contextPath}/semantic/ModifyQueryTopicAction.do?act=add',
			  height:'78%',
			  width:'100%',
		      closeAfterAdd: true,
		      reloadAfterSubmit: true,
		      recreateForm: true,
              beforeInitData: function () {
            	  $("#grid").jqGrid('setColProp','topicSerial',{editoptions:{readonly:true}});
              }
			},  
			
			// del options
			{ url:'${pageContext.request.contextPath}/semantic/DeleteKeywordByIdAction.do',
			  delData: {
				  delKwCondition: function() {
				      var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
                      var kId = $("#grid").jqGrid('getCell', selectedId, 'keywordId');
                      var kw = $("#grid").jqGrid('getCell', selectedId, 'keyword');
                      var ts = $("#grid").jqGrid('getCell', selectedId, 'topicSerial');
                      //alert(kId + "/"+ kw + "/" + ts);
                      var delKwCond = kId + "/"+ kw + "/" + ts;
                      return delKwCond;
			      }
			  },
			  reloadAfterSubmit: false
			},
			
			// search options
			{ Find: "搜尋",  
			  closeAfterSearch: true  
			},
			//refresh optioins
			{ url: '${pageContext.request.contextPath}/semantic/InitGetTfDfAction.do'
			} 
		)
		//jQuery("#grid").jqGrid('setFrozenColumns');
		
	});
	
	
	function clickme(conditions) {
		$("#form1").validate({});//check all columns.
		$("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
	 	$("#allCover").css("display","block");
	 	
	 	var splitStr = conditions.split("/");
	 	//alert(splitStr[4] +"/" +splitStr[1]+"/"+splitStr[2]);
	 	var topic = document.getElementById("topic").value;
	 	var ts = document.getElementById("topicSerial").value;
	 	
		var url = '${pageContext.request.contextPath}/semantic/InitCkipKeywordDetailAction.do?keyword='+splitStr[0]+'&articleIdGroup='+ splitStr[1]+'&periodId='+splitStr[4]+'&topic='+ topic+'&topicSerial='+ts + '&domainKnowhowName='+splitStr[3];
		popup(url);
		//$('#form1').attr('action', '${pageContext.request.contextPath}/ft/SentenceDetailAction.do?detailIds=' + detailIds);
		//$('#form1').submit();
		if (window.focus) {newwin.focus()}
		$("#allCover").css("display","none");
	}

	function popup(url) {
		params  = 'width='+(screen.width/2);
		params += ', height='+(screen.height/2);
		params += ', top=0, left=0'
		params += ', fullscreen=0';
		params += ', scrollbars=yes';
		params += ', location=no';
		
		newwin=window.open(url,'popUpWindow', params);
		if (window.focus) {newwin.focus()}
		$("#allCover").css("display","none");
		return false;
	}
	
	function calHowNetNTUSD(conditions) {
		$("#form1").validate({});//check all columns.
		$("#mDiv").css("width",document.body.clientWidth).css("height",document.body.clientHeight);
	 	$("#allCover").css("display","block");
	 	
	 	var splitStr = conditions.split("/");
	 	//alert(splitStr[0] +"/" +splitStr[1]+"/"+splitStr[2]);
	 	var topic = document.getElementById("topic").value;
	 	var ts = document.getElementById("topicSerial").value;
	 	
		var url = '${pageContext.request.contextPath}/semantic/CalHowNetNTUSDAction.do?keyword='+splitStr[0]+'&articleIdGroup='+ splitStr[1]+'&periodId='+splitStr[3]+'&topic='+ topic+'&topicSerial='+ts + '&keywordId='+splitStr[2];
		
		$('#form1').attr('action', url);
		$('#form1').submit();
		if (window.focus) {newwin.focus()}
		$("#allCover").css("display","none");
	}
	
	//AJAX for jqGrid .
	function getDomainKnowhowJson() {
		
		var returnStr = "-1:Clear/清空/清除;";
		$.ajax({
			async: false,  
			type: 'POST',
			url: "${pageContext.request.contextPath}/semantic/GetDomainKnowhowByIdJsonAction.do?topicSerial=${topicSerial}",
			dataType: "json",
			success: function(data) {
				for(var i=0; i<data.domainKnowhowJson.length; i++) {
					returnStr += data.domainKnowhowJson[i].domainKnowhowId + ":" + data.domainKnowhowJson[i].name+";";
				}
			}
	    });
		//alert(returnStr)
		return returnStr; 
	}
</script>

</head>
<body>
<div class="mask opacity" id="allCover" style="display:none;">
	<div class="box" id="mDiv">
       <label>Please wait!!</label>
	</div>
</div>

<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/BrandAnalysis_logo.png" alt="BrandAnalysis" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH：Article List > TF/DF</div>
<div class="body">
<div class="body">

<div class="dialog">


<form action="" method="post" name="form1" id="form1">

<table width="480" >
    <tr>
        <th>Topic</th>
        <td><s:textfield theme="simple" name="topic" id="topic" value="%{topic}" ReadOnly="true"/></td>
        <th>Topic Serial</th>
        <td><s:textfield theme="simple" name="topicSerial" id="topicSerial" value="%{topicSerial}" ReadOnly="true" /></td>
        
	</tr>
</table>
  <div id="all">
  <div id="mainFunction">
	<left>
		<table id="grid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>
</form>

</div>
</div>
</div>
</body>
</html>
 