<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>
        <!-- D3.js WordCloud -->
        <!-- <script src="http://d3js.org/d3.v3.min.js"></script> -->
        <script src="${pageContext.request.contextPath}/js/Graph/d3.v3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/Graph/d3.layout.cloud.js"></script>
        
<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Brand Analysis- Domain Know How & Period </title>
<style>
label.error{display:inline;margin-left: 10px; color: red;}
TD{padding-left:10px;} 
#wrap { display:table; }
#cell { display:table-cell; vertical-align:middle; }
</style>
<style>
html,body{ height:100%; margin:0; padding:0} 
.mask{height:100%; width:100%; position:fixed; _position:absolute; top:0; z-index:1000; } 
.opacity{ opacity:0.3; filter: alpha(opacity=30); background-color:#000000; }
.box {
        display: table-cell;
        vertical-align:middle;
        text-align:center;
        *display: block;
        *font-size: 190px;
        *font-family:Arial;
        
        width:500px;
        height:500px;
        border: 2px solid #eee;
}
.box label {
        vertical-align:middle;
        font-size:68px;
        color: white;
}
</style>

<script type="text/javascript">

$(function(){
	//to get data for word cloud.
	$.ajax({
		type: 'POST',
		url: "${pageContext.request.contextPath}/semanticGraph/WordCloudAction.do?topicSerial=${topicSerial}",
		dataType: "JSON",
		success: function(data) {
            var wordCloudData=[];
    	    
            //alert(data.wordCloud.length);
        	for (var i=0 ; i<data.wordCloud.length; i++)
        	{
        		wordCloudData.push({text : data.wordCloud[i].keyword, size:data.wordCloud[i].weight});
        	}
			
			//alert(JSON.stringify(wordCloudData));
        	var fill = d3.scale.category20();   
        	d3.layout.cloud().size([600, 400])   
    	     .words(wordCloudData)   
    	     .rotate(function() { return ~~(Math.random() *2) * 90; })   
    	     .font("Impact")   
    	     .fontSize(function(d) { return (d.size); })   //function(d) { return d.size; }
    	     .on("end", draw)   
    	     .start(); 

	    	function draw(words) {   
	    	  d3.select("#tag") //要插入標籤雲的tag id
    	      .append("svg")   
    	      .attr("width", 800)   
    	      .attr("height", 400)   
    	      .append("g")   
    	      .attr("transform", "translate(400,200)") //這裡的值要對應到繪圖區域的寬
    	      .selectAll("text")   
    	      .data(words)   
    	      .enter().append("text")   
    	      .style("font-size", function(d) { return d.size + "px"; })   
    	      .style("font-family", "Impact")   
    	      .style("fill", function(d, i) { return fill(i); })   
    	      .attr("text-anchor", "middle")   
    	      .attr("transform", function(d) {
    	        return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";   
    	       })   
    	      .text(function(d) { return d.text; });   
	    	}
		}
    });	
	  
	getCategory();
});

function getCategory() {
	var CategoryNameObj = document.getElementById("categoryName");
	CategoryNameObj.style.display = "none";
	var tarObj = document.getElementById("categoryName");
	var selectObj = document.createElement("SELECT");
	selectObj.id = "cat-Select";
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicCategoryAction.do",
			dataType: "json",
			success: function(data) {
				var index = 0;
				
				for(var i=0; i<data.topicCategory.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					
					optionObj.innerHTML = data.topicCategory[i].name;
					optionObj.value = data.topicCategory[i].categorySerial +"@"+ data.topicCategory[i].secondName;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#categorySerial").val( arr[0] );
		
		$("#categoryName").val( this.options[this.selectedIndex].text);
		
		//call to getSubCategory();
		getSubCategory(arr[0]);
	};
}

function getSubCategory(categorySerial) {
	var selectObj = "";
	var SubCategoryNameObj = document.getElementById("subCategoryName");
	SubCategoryNameObj.style.display = "none";
	var tarObj = document.getElementById("subCategoryName");
	
	if(document.getElementById("subCat-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "subCat-Select";
	} else {
		//移除原本的options
		$('#subCat-Select').find('option').remove().end();
		$('#topic-Select').find('option').remove().end();
		document.getElementById("subCategorySerial").value = "";
		document.getElementById("topicSerial").value = "";
		document.getElementById("topicChart").value = "";
		selectObj = document.getElementById("subCat-Select");
	}
	
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetSubcategory2Action.do?categorySerial="+ categorySerial,
			dataType: "json",
			success: function(data) {
				var index = 0;
				var optionObj = "";
				
				for(var i=0; i<data.subCategory.length; i++) {
					optionObj = document.createElement("OPTION");
					
					optionObj.innerHTML = data.subCategory[i].subName;
					optionObj.value = data.subCategory[i].subCategorySerial +"@"+ data.subCategory[i].categorySerial;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#subCategorySerial").val( arr[0] );
		
		$("#subName").val( this.options[this.selectedIndex].text);
		getTopic() ;
	};
	
	insertAfter(selectObj, tarObj);
}


function getTopic() {
	var catSerial = document.getElementById("categorySerial").value;
	var subCatSerial = document.getElementById("subCategorySerial").value;
	var selectObj = "";
	
	var TopicObj = document.getElementById("topic");
	TopicObj.style.display = "none";
	var tarObj = document.getElementById("topic");
	selectObj = document.createElement("SELECT");
	
	if(document.getElementById("topic-Select")==null) {
		selectObj = document.createElement("SELECT");
		selectObj.id = "topic-Select";
	} else {
		//移除原本的options
		$('#topic-Select').find('option').remove().end();
		document.getElementById("topicSerial").value = "";
		document.getElementById("topicChart").value = "";
		selectObj = document.getElementById("topic-Select");
	}
	
	
	insertAfter(selectObj, tarObj)
	$.ajax({
			type: 'POST',
			url: "${pageContext.request.contextPath}/general/GetTopicJSONAction.do?categorySerial="+catSerial+"&subCategorySerial="+subCatSerial,
			dataType: "json",
			success: function(data) {
				var index = 0;
				
				for(var i=0; i<data.topicJSONStr.length; i++) {
					var optionObj = document.createElement("OPTION");
					
					optionObj.innerHTML = data.topicJSONStr[i].topic;
					optionObj.value = data.topicJSONStr[i].topicSerial +"@"+ data.topicJSONStr[i].topic;
					
					selectObj.appendChild(optionObj);
				}
				selectObj.selectedIndex = index;
			}
	});
	selectObj.onchange = function() {
		var both = this.value;
		var arr = this.value.split("@");
		$("#topicSerial").val( arr[0] );
		$("#topicChart").val( arr[1]);
		$("#topic").val( this.options[this.selectedIndex].text);
	};
}

function insertAfter(newEl, targetEl) {
    var parentEl = targetEl.parentNode;
    if(parentEl.lastChild == targetEl) {
        parentEl.appendChild(newEl);
    }else {
        parentEl.insertBefore(newEl,targetEl.nextSibling);
    }            
}


//jqGrid pieChart data start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
$(document).ready(function() {
	$("#pieChart_grid").jqGrid({
		url:encodeURI('${pageContext.request.contextPath}/semanticGraph/PieChartAction.do?topicSerial=${topicSerial}'),
		datatype: "JSON",
		height: "100%", 
		width:"860",
		colNames:['keyword','domainKnowhowId','domainKnowhowName','domainKnowhowScore','keywordScore','總分','正分總和','負分總和','關鍵字分數'],
		         
        colModel:[
                  {name:'keyword', index:'keyword',width:150, align:'center'},
                  
				  {name:'domainKnowhowId', index:'domainKnowhowId', width:150, align:'center'},
				   
				  {name:'domainKnowhowName',index:'domainKnowhowName', align:'center',width:150},
				  
				  {name:'domainKnowhowScore',index:'domainKnowhowScore', align:'center',width:150},
				  
				  {name:'keywordScore',index:'keywordScore', align:'center',width:150},
				  {name:'totalScore', index:'totalScore', width:120, align:'right', hidden: false, sort:false},
                  {name:'positiveScore', index:'positiveScore', width:120, align:'right', hidden: false, sort:false},
                  {name:'negativeScore', index:'negativeScore', width:120, align:'right', hidden: false, sort:false},
                  {name:'sentimentScore', index:'sentimentScore', width:120, align:'right', hidden: false, sort:false},
                  
        ],
        shrinkToFit:false,
        viewrecords: true,
        gridview: true, 
        rownumbers: true,
        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
        caption: "Pie Chart data List",
        toppager: true,
        loadComplete: function(data){
        	//alert(JSON.stringify(data));
            var dkhIdAndName=[], dkhAll=[], dkhId=[], dkhName=[];
    	    
        	//1.前處理
        	for (var i=0 ; i<data.rows.length; i++)
        	{
        		dkhAll[i] = data.rows[i].domainKnowhowId+","+data.rows[i].domainKnowhowName+","+data.rows[i].keyword+","+data.rows[i].keywordScore+","+data.rows[i].domainKnowhowScore+","+data.rows[i].sentimentScore;
        		dkhIdAndName[i] = data.rows[i].domainKnowhowId+","+data.rows[i].domainKnowhowName+","+data.rows[i].totalScore+","+data.rows[i].positiveScore+","+data.rows[i].negativeScore;
        		dkhName[i] = data.rows[i].domainKnowhowName;
        	}
        	
        	//2.去除array中重複元素.
        	dkhIdAndName = dkhIdAndName.filter(function (el, i, arr) {
        		return arr.indexOf(el) === i;
        	});
        	
        	drawPieChart(dkhAll, dkhIdAndName);
        }
    });
});
//jqgrid pieChart data end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//PIE CHARTS
function drawPieChart(dkhAll, dkhIdAndName) {
	
	//1.categories arrange and colors array
	var colors = Highcharts.getOptions().colors,
	i=0, categories=[], dataArr=[];
	
	for(i=0; i<dkhIdAndName.length; i++) {
		var dkhIdAndNameSplit = dkhIdAndName[i].split(",");
		var dkhId = dkhIdAndNameSplit[0];//domainKnowHowId
		categories.push(dkhIdAndNameSplit[1]+'<br>情緒總分：'+dkhIdAndNameSplit[2]+'<br>'+'<br>正向分數：'+dkhIdAndNameSplit[3]+'<br>'+'<br>負向分數：'+dkhIdAndNameSplit[4]);
		var drilldowns=[], drilldowns2=[], keywords=[], keywordScore=[];
		var dkhIdHasSet=null;
		//2.arange data
		for(var j=0; j<dkhAll.length; j++) {
			
			//dkhAll_Array: 0:domainKnowhowId, 1:domainKnowhowName, 2:keyword, 3:keywordScore, 4:domainKnowhowScore, 5. totalScore
			var dkhAllSplit = dkhAll[j].split(",");
			
			if(dkhId===dkhAllSplit[0]) {
				var combineData = dkhAllSplit[2] +'[情緒:' + dkhAllSplit[5]+'] ';
				//drilldowns.push({categories:dkhAllSplit[2], data:Number(dkhAllSplit[3])});
				drilldowns.push({categories:combineData, data:Number(dkhAllSplit[3])});
				
				if(dkhIdHasSet==null) {
					dataArr.push({y:Number(dkhAllSplit[4]), color:colors[i], drilldown:drilldowns2});
					dkhIdHasSet = dkhId;
				}
			}
		}
		
		
		
		//整理drillData
		for(var d=0; d<drilldowns.length; d++) {
			keywords.push(drilldowns[d].categories);
			keywordScore.push(drilldowns[d].data);
		}
		drilldowns2.push({categories:keywords, data:keywordScore});
		
		//console.log("b==> " +drilldowns2);
	}
		
    
    var data = dataArr,
    browserData = [],
    versionsData = [],
    i,
    j,
    dataLen = data.length,
    drillDataLen,
    brightness;


	// Build the data arrays
	for (i = 0; i < dataLen; i += 1) {
	
	    // add browser data
	    browserData.push({
	        name: categories[i],
	        y: data[i].y,
	        color: data[i].color
	    });
	
	    // add version data
	    drillDataLen = data[i].drilldown[0].data.length;
	    for (j = 0; j < drillDataLen; j += 1) {
	        brightness = 0.2 - (j / drillDataLen) / 5;
	        versionsData.push({
	            name: data[i].drilldown[0].categories[j],
	            y: data[i].drilldown[0].data[j],
	            color: Highcharts.Color(data[i].color).brighten(brightness).get()
	        });
	    }
	}
	
	// Create the chart
	Highcharts.chart('container_pieChart', {
	    chart: {
	        type: 'pie'
	    },
	    credits: {
            enabled: false //移除highchats右下方logo.
        },
	    title: {
	        text: '議題與關鍵字之關連性'
	    },
	    subtitle: {
	        text: '主題：${topicChart}'
	    },
	    yAxis: {
	        title: {
	            text: 'Total percent market share'
	        }
	    },
	    plotOptions: {
	        pie: {
	            shadow: false,
	            center: ['50%', '50%']
	        }
	    },
	    tooltip: {
	        valueSuffix: '%'
	    },
	    series: [{
	        name: '佔比',
	        data: browserData,
	        size: '60%',
	        dataLabels: {
	            formatter: function () {
	                return this.y > 5 ? this.point.name : null;
	            },
	            color: '#ffff',
	            distance: -30,
	            style: {
                    fontSize: '12px',
                    fontFamily: 'Arial, sans-serif'
                }
	        }
	    }, {
	        name: '關鍵字比率',
	        data: versionsData,
	        size: '80%',
	        innerSize: '60%',
	        dataLabels: {
	            formatter: function () {
	                // display only if larger than 1
	                return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y + '%' : null;
	            },
	            style: {
                    fontSize: '12px',
                    fontFamily: 'Arial, sans-serif'
                }
	        }
	    }]
	});
}

//jqGrid domain know how & period statistic start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
$(document).ready(function() {
	$("#grid").jqGrid({
		url:encodeURI('${pageContext.request.contextPath}/semanticGraph/DomainKnowhowAndPeriodStatisticAction.do?topicSerial=${topicSerial}&topicChart=${topicChart}'),
		datatype: "JSON",
		height: "100%", 
		width:"860",
		colNames:['Period','DomainknowHow','TF','DF'],
		         
        colModel:[
                  {name:'periodName', index:'periodName',width:150, align:'center'},
                  
				  {name:'domainKnowhowName', index:'domainKnowhowName', width:150, align:'center'},
				   
				  {name:'TF',index:'TF', align:'center',width:150},
				  
				  {name:'DF',index:'DF', align:'center',width:150},
				  
        ],
        shrinkToFit:false,
        viewrecords: true,
        gridview: true, 
        rownumbers: true,
        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
        caption: "Domain know how and Period Statistic List",
        toppager: true,
        loadComplete: function(data){
        	//$("#jqgrid_div").hide();//隱藏jqgrid表單
        	
        	var period=[], domainKnowhow=[], TF=[], DF=[], seriesData=[], seriesLineData=[];
    	    
        	//1.前處理
        	for (var i=0 ; i<data.rows.length; i++)
        	{
        		period[i] = data.rows[i].periodName;
        	    domainKnowhow[i] = data.rows[i].domainKnowhowName;
        	}
        	
        	//2.去除array中重複元素.
        	var dedupePeriod = period.filter(function (el, i, arr) {
        		return arr.indexOf(el) === i;
        	});
        	
        	var dedupeDomainKnowhow = domainKnowhow.filter(function (el, i, arr) {
        		return arr.indexOf(el) === i;
        	});
        	
        	//3.組新json給動態series for Bar chart.
        	for(var i=0; i<dedupeDomainKnowhow.length; i++) {
        		for(var j=0; j<dedupePeriod.length; j++) {
        			var tfData=[];
        			for(var k=0; k<data.rows.length; k++) {
        				
        				if(dedupeDomainKnowhow[i]==data.rows[k].domainKnowhowName){
        					//alert(dedupePeriod[i]+', '+ dedupeDomainKnowhow[j] + ', ' + Number(data.rows[k].TF));
        					tfData.push(Number(data.rows[k].TF));
                		}
        			}
        		}
        		seriesData.push({name : dedupeDomainKnowhow[i], data:tfData});
        	}
        	/**
        	 [{"name":"販售價格","data":[9,7,10]},
        	  {"name":"行銷策略","data":[16,13,16]},
        	  {"name":"系統穩定","data":[15,9,10]},
        	  {"name":"硬體規格","data":[15,8,13]}
        	  ]
        	**/

        	//4.組新json給動態series for Line chart.
        	for(var i=0; i<dedupePeriod.length; i++) {
        		for(var j=0; j<dedupeDomainKnowhow.length; j++) {
        			var tfData=[];
        			for(var k=0; k<data.rows.length; k++) {
        				
        				if(dedupePeriod[i]==data.rows[k].periodName){
        					//alert(dedupePeriod[i]+', '+ dedupeDomainKnowhow[j] + ', ' + Number(data.rows[k].TF));
        					tfData.push(Number(data.rows[k].TF));
                		}
        			}
        		}
        		seriesLineData.push({name : dedupePeriod[i], data:tfData});
        	}
        	//alert(JSON.stringify(seriesLineData));
        	/**
        	[{"name":"2012Q4","data":[21,0,7,14,15,57]},
        	 {"name":"2013Q1","data":[96,0,62,22,91,29]},
        	 {"name":"2014Q4","data":[24,0,13,5,39,0]}]
        	**/
        	
        	//5.整理後資料給highcharts
        	barChart(dedupePeriod, seriesData);
        	lineChart(dedupeDomainKnowhow, seriesLineData);
        	
        }
    });
});
//jqgrid domain know how & period statistic end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


//CHART JS 長條圖barChart.
	 var bChart = null;
	 var bOptions = {
    //$('#container').highcharts({
        chart: {
        	renderTo: 'container_barChart', //设置显示的页面块
            type: 'column',
            zoomType: 'x'
        },
        credits: {
            enabled: false //移除highchats右下方logo.
        },
        title: {
            text: '議題與銷售量的週期關係圖'
        },
        subtitle: {
            text: '主題：${topicChart}',
        },
        xAxis: {
            //categories: ['Q1','Q2','Q3'], //時間區間, 改用動態處理!
            crosshair: true,
        },
        yAxis: [{
        	     min: 0,
                 title: {
                   text: 'TF-Term Frequency (個)'
                 }
                },{
        	     //min: 0,
                 title: {
                   text: 'QUANTITY'
                 },
                 opposite: true
                }
                ],
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} 個</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
        	column: {
                dataLabels: {
                    enabled: true,
                    rotation: -80,
                    align: 'right',
                    format: '{point.y:.0f}', // one decimal
                    y: -18, // 10 pixels down from the top
                    style: {
                        fontSize: '9px',
                        fontFamily: 'Arial, sans-serif'
                    }
                }
            }
        }
        
        /* 已改用動態處理!! 
        series: [
        {
            name: 'Tokyo',
            data: [49.9, 71.5, 106.4]

        }, {
            name: 'New York',
            data: [83.6, 78.8, 98.5]

        ] 
        */
    //});
	 }; 
    
    //get dynamic series.
    function barChart(period, series){
          bChart = new Highcharts.Chart(bOptions);
          //异步添加第数据列
          
          LoadSerie_barChart(period, series);
          
    }; 
    //長條圖：异步读取数据并加载到图表
    function LoadSerie_barChart(period, series) {
        bChart.showLoading();
        bChart.xAxis[0].setCategories(period);//添加X轴
        
        $.each(series, function(i, n) {
            var bSeries = {
            		yAxis: 0,
            
                    name: series[i].name,
                    data: series[i].data,
                    type: 'column',
            };
            bChart.addSeries(bSeries);//添加数据列
        });
        
        
        //進出貨量
        $.ajax({
  			type: 'POST',
  			url: "${pageContext.request.contextPath}/semanticGraph/InOutRecordAction.do?topicSerial=${topicSerial}",
  			dataType: "json",
  			success: function(data) {
  				
  			   if(data.inOutRecord==null)
  				   return;
  			   
  			   var inQuantity = new Array(period.length), 
  			       outQuantity = new Array(period.length);
  			   
  			   for(var i=0; i<period.length; i++) {
  				   for(var j=0; j<data.inOutRecord.length; j++) {
  				   	   if(period[i]==data.inOutRecord[j].periodName) {
  				   		  inQuantity[i] = data.inOutRecord[j].inQuantity;
  				   		  outQuantity[i] = data.inOutRecord[j].outQuantity;
  				   		  //inQuantity.push(data.inOutRecord[j].inQuantity);
  				   		  //outQuantity.push(data.inOutRecord[j].outQuantity);
  				   	   }
    			   }
  			   }
  			   
  			   for(var i=0; i<inQuantity.length; i++) {
  				   if(inQuantity[i] == null)
  					   inQuantity[i] = 0;  
  			   }
  			   for(var i=0; i<outQuantity.length; i++) {
				   if(outQuantity[i] == null)
					   outQuantity[i] = 0;  
			   }
  			   

  		       bChart.xAxis[0].setCategories(period);//添加X轴
	           var inQuantitySeries = {
	        		   yAxis: 1,
	                   name: "Predict",
	                   data: inQuantity,
	                   type: 'spline',
	           };
  		       
	           var outQuantitySeries = {
	        		   yAxis: 1,
	                   name: "Actual",
	                   data: outQuantity,
	                   type: 'spline',
	           };
	           
	           bChart.addSeries(inQuantitySeries);//添加数据列
	           bChart.addSeries(outQuantitySeries);//添加数据列
  			}
        });
        
        bChart.hideLoading();
    };

    
//CHART JS 折線圖lineChart.
var lChart = null;
var lOptions = {
 //$('#container').highcharts({
     chart: {
     	renderTo: 'container_lineChart', //设置显示的页面块
        type: 'column',
        //zoomType: 'xy'
     },
     credits: {
         enabled: false //移除highchats右下方logo.
     },
     title: {
         text: '議題詞與週期關係圖'
     },
     subtitle: {
         text: '主題：${topicChart}',
     },
     xAxis: {
         //categories: ['Q1','Q2','Q3'], //時間區間, 改用動態處理!
         crosshair: true,
     },
     yAxis: {
     	min: 0,
         title: {
             text: 'TF-Term Frequency (個)'
         }
     },
     tooltip: {
         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
         pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
             '<td style="padding:0"><b>{point.y:.0f} 個</b></td></tr>',
         footerFormat: '</table>',
         shared: true,
         useHTML: true
     },
     plotOptions: {
    	 spline: {
             dataLabels: {
                 enabled: true,
                 style: {
                     fontSize: '9px',
                     fontFamily: 'Arial, sans-serif'
                 }
             }
         }
     }
     /* 已改用動態處理!! 
     series: [
     {
         name: 'Tokyo',
         data: [49.9, 71.5, 106.4]

     }, {
         name: 'New York',
         data: [83.6, 78.8, 98.5]

     ] 
     */
 //});
 };
 
 //get dynamic series.
 function lineChart(domainKnowhow, series){
	 lChart = new Highcharts.Chart(lOptions);
     //异步添加第数据列
     LoadSerie_lineChart(domainKnowhow, series);
 }; 
 //异步读取数据并加载到图表
 function LoadSerie_lineChart(domainKnowhow, series) {
     lChart.showLoading();
     lChart.xAxis[0].setCategories(domainKnowhow);//添加X轴
     
     $.each(series, function(i, n) {
         var lSeries = {
                 name: series[i].name,
                 data: series[i].data,
                 type: 'spline',
         };
         lChart.addSeries(lSeries);//添加数据列
     });
     
 	/**
 	[{"name":"2012Q4","data":[21,0,7,14,15,57]},
 	 {"name":"2013Q1","data":[96,0,62,22,91,29]},
 	 {"name":"2014Q4","data":[24,0,13,5,39,0]}]
 	**/
     
     lChart.hideLoading();
 }
 
 
//全時間jqGrid Statistic on domain know how start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

 $(document).ready(function() {
 		$("#statistic_all_dkh_Grid").jqGrid({
 			url:encodeURI('${pageContext.request.contextPath}/semantic/GetAllStatisticDomainKnowhowByIdAction.do?topicSerial=${topicSerial}&topic=${topic}'),
 			datatype: "JSON",
 			height: "100%+150", 
 			width:"860",
 			colNames:['議題詞','關鍵字個數','總分','正分總和','負分總和', 'topicSerial', '議題id','periodId'],
 	        colModel:[
                  
                  {name:'domainKnowhowName', index:'domainKnowhowName', width:180, align:'center',hidden: false, sort:false},
                  {name:'counter', index:'counter', width:120, align:'right',hidden: false, sort:false},
                  {name:'totalScore', index:'totalScore', width:120, align:'right', hidden: false, sort:false},
                  {name:'positiveScore', index:'positiveScore', width:120, align:'right', hidden: false, sort:false},
                  {name:'negativeScore', index:'negativeScore', width:120, align:'right', hidden: false, sort:false},
                  {name:'topicSerial', index:'topicSerial', width:120, align:'center', hidden: true, sort:false},
                  {name:'domainKnowhowId', index:'domainKnowhowId', width:120, align:'center',hidden: true, sort:false},
                  {name:'periodId', index:'periodId', width:120, align:'center', hidden: true, sort:false},
                  

 	        ],
 	        shrinkToFit:false,
 	        viewrecords: true,
 	        gridview: true, 
 	        
 	        pgbuttons: false,
 	        pginput: false,
 	        pager: "#statistic_all_dkh_gridPager",
 	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
 	        caption: "議題詞vs情緒分數 (Statistic on domain know how)",
 	        subGrid : true,
 	        loadComplete: function(data) {
 	        	var counter=[], domainKnowhow=[], totalScore=[];
 	    	    
 	        	//1.前處理
 	        	for (var i=0 ; i<data.rows.length; i++)
 	        	{
 	        		counter[i] = data.rows[i].counter;//該議題詞有幾個關鍵字(keyword)
 	        	    domainKnowhow[i] = data.rows[i].domainKnowhowName;//議題詞
 	        	    totalScore[i] = data.rows[i].totalScore;//分數
 	        	}
 	        	drawHighChart(domainKnowhow, counter, totalScore);//forward to chart function
 	        },
 	        
 	    	//subGrid start >>>
 	    	subGridRowExpanded: function(subgrid_id, row_id) {
 				var headData = jQuery("#statistic_all_dkh_Grid").jqGrid('getRowData',row_id);
 				var conditions = "topicSerial="+headData.topicSerial+"&domainKnowhowId="+headData.domainKnowhowId;
 				var subgrid_table_id;
 				subgrid_table_id = subgrid_id+"_t";
 			 
 				$("#"+subgrid_id).html("<table id='"+ subgrid_table_id+"' class='scroll'></table>");
 			  	$("#"+subgrid_table_id).jqGrid({
 			  		url:"${pageContext.request.contextPath}/semantic/GetKeywordsFormAllStatisticAction.do?"+conditions,
 			  		datatype: "json",
 			  		width:"750",
 			  		colNames: ['關鍵字','TF','DF','NTUSD正向個數','NTUSD負面個數','keywordId'],
 			  		colModel: [
 			      		{name:"keyword",index:"keyword",width:12, align:'center',sortable:false},
 			      		{name:"termFrequency",index:"termFrequency",width:12, align:'center',sortable:false},
 			      		{name:"docFrequency",index:"docFrequency",width:12, align:'center',sortable:false},
 			      		{name:"positiveNTUSD",index:"positiveNTUSD",width:15, align:'center', sortable:false,hidden: true},
 			      		{name:"negativeNTUSD",index:"negativeNTUSD",width:15, align:'center', sortable:false,hidden: true},
 			      		{name:"keywordId",index:"keywordId",width:20, align:'center', sortable:false,hidden: true}
 			  		],
 			  		height: "100%"+20,
 			  		rowNum:999,
 			  		prmNames:{rows:"pageSize"},
 			  	});
 			},
 	    	//subgrid end <<<
 	    });
 });
 //時間區間jqgrid Statistic on domain know how end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

 //CHART JS.
 function drawHighChart(domainKnowhow, counter, totalScore) {
 	
 	$('#container_semantic').highcharts({
 		
         chart: {
             zoomType: 'x'
         },
         credits: {
             enabled: false //移除highchats右下方logo.
         },
         title: {
             text: '口碑品牌情緒分析'
         },
         subtitle: {
             text:  '主題： ${topicChart}' //show 
         },
         xAxis: [{
             categories: domainKnowhow, //議題詞
             crosshair: true
         }],
         yAxis: [{ // Primary yAxis
             labels: {
                 format: '${value}',
                 style: {
                     color: Highcharts.getOptions().colors[0]
                 }
             },
             title: {
                 text: '關鍵字個數 (個)',
                 style: {
                     color: Highcharts.getOptions().colors[0]
                 }
             }
         }, { // Secondary yAxis
             title: {
                 text: '情緒總分',
                 style: {
                     color: "red"
                 }
             },
             labels: {
                 format: '{value}',
                 style: {
                     color: "red"
                 }
             },
             opposite: true
         }],
         tooltip: {
             shared: true
         },
         
         legend: {
             layout: 'vertical',
             align: 'left',
             x: 0,
             verticalAlign: 'top',
             y: 0,
             floating: true,
             backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
         },
         plotOptions: {
         	column: {
                 dataLabels: {
                     enabled: true
                 }
             },
             spline: {
                 dataLabels: {
                     enabled: true,
                     color: "red"
                 }
             }
         },
         
         series: [{
             name: '關鍵字個數',
             type: 'column',
             yAxis: 0,
             data: counter, //個數
             color: Highcharts.getOptions().colors[0],
             tooltip: {
                 valueSuffix: ''
             }

         }, {
             name: '情緒總分',
             type: 'spline',
             color: "red",
             yAxis: 1,
             data: totalScore,//close
             tooltip: {
                 valueSuffix: ''
             }
         }]
     });	
     
 }
</script>
  
</head>
<body>
<div class="mask opacity" id="allCover" style="display:none;">
	<div class="box" id="mDiv">
       <label>Please wait!!</label>
	</div>
</div>

<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/BrandAnalysis_logo.png" alt="BrandAnalysis" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： Domain Know How and Period</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:if test="hasActionMessages()">

		<div class="successMsg" align="center" style="margin:0 auto;">
		   <s:actionmessage/>
		</div>
</s:if>
<s:if test="hasActionErrors()">
		<div class="errorMsg" align="center" style="margin:0 auto;">
		   <s:actionerror/>
		</div>
</s:if>


<form action="" method="post" name="form1" id="form1">

<table width="480" >
	<tr>
    	<th>Category：</th>
    	<td>
            
			<s:hidden theme="simple" name="categoryName" id="categoryName" value="%{categoryName}" ReadOnly="true"/>
			<s:hidden theme="simple" name="categorySerial" id="categorySerial" value="%{categorySerial}" ReadOnly="true" cssClass="required form-control"/>
		</td>
	
		<th>SubCat.：</th>
    	<td>
            
			<s:hidden theme="simple" name="subCategoryName" id="subCategoryName" value="%{subCategoryName}" ReadOnly="true"/>
			<s:hidden theme="simple" name="subCategorySerial" id="subCategorySerial" value="%{subCategorySerial}" ReadOnly="true" cssClass="required form-control"/>
		</td>
	
		<th>Topic：</th>
		<td>
		    <s:hidden theme="simple" name="topic" id="topic" value="%{topic}" ReadOnly="true" />
			<s:hidden theme="simple" name="topicSerial" id="topicSerial" value="%{topicSerial}" ReadOnly="true" cssClass="required form-control"/>
			<!-- 給Highcharts抓取 -->
			<s:textfield theme="simple" name="topicChart" id="topicChart" value="%{topicChart}" ReadOnly="true" />
		</td>
		
		<td>
		    <s:submit onclick="searchForm()" cssClass="comfirm" value="Query" theme="simple"/>
		</td>
		
	</tr>
	
</table>
<c:choose>
   <c:when test="${empty topicSerial}">
     <div style="background-color:#FFE6D9;padding:10px;margin-bottom:5px;">
  		<span style="color:#2894FF;">請先搜尋，再繼續 (系统需先知道您所要編輯的資料)。</span><br><br>
  		<span style="color:#2894FF;">请先搜寻，再继续动作 (系统必须先取得您所要编辑的资讯来源)。</span><br><br>
  		<span style="color:#2894FF;">Please Query first (We have to know what do you want to modify firstly).</span><br>
  		
  	 </div>
   </c:when>
   <c:otherwise>
        <div id="tag" style="border: 2px solid #87CEFA; "></div><br>
	    <!-- high chart -->
	    <script src="https://code.highcharts.com/highcharts.js"></script>
	    <script src="https://code.highcharts.com/modules/exporting.js"></script>
	    <div id="container_pieChart" style="width: 960px; height: 800px; margin: 0 auto; alight:left"></div><br>
	    <center>
	    <table id="statistic_all_dkh_Grid"></table>
		<div id="statistic_all_dkh_gridPager"></div>
		</center><br>
			
	    <div id="container_semantic" style="min-width: 310px; height:500px; margin: 0 auto"></div><br>
	    <div id="container_barChart" style="min-width: 310px; height: 500px; margin: 0 auto"></div><br>
	    <div id="container_lineChart" style="min-width: 310px; height: 500px; margin: 0 auto"></div><br>
        
        <!-- GRID -->
  		<div id="jqgrid_div">
  		<div id="mainFunction">
			
			<left>
				<table id="grid"></table>
				<div id="gridPager"></div>
			</left>
			<br>
			<left>
				<table id="pieChart_grid"></table>
				<div id="pieChart_gridPager"></div>
			</left>
			<br>
			
		</div>
        </div>	  
   </c:otherwise>
</c:choose>

</form>

</div>
</div>
</div>
</body>
</html>
 